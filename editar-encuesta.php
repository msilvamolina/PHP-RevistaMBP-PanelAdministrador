<?php 

include('../paginas_include/variables-generales.php');
$nivel_pagina = 5;
include('php/verificar-permisos.php');

$id_encuesta = trim($_GET['encuesta']);

//verificamos la direccion ingresada
$link_db = "http://".$_SERVER['HTTP_HOST'].'/admin/editar-encuesta.php?encuesta='.$id_encuesta;
$pagina_url ="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

if($link_db != $pagina_url) {
	$redireccion = $link_db;
	header('Location: '.$redireccion);
	exit;
}

conectar('encuestas');

$query_rs_elegir_noticia = "SELECT id_encuesta, encuesta_pregunta, fecha_carga FROM encuestas ORDER BY id_encuesta DESC";
$rs_elegir_noticia = mysql_query($query_rs_elegir_noticia)or die(mysql_error());
$row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia);
$totalrow_rs_elegir_noticia = mysql_num_rows($rs_elegir_noticia);

if($id_encuesta) {
$query_rs_noticia = "SELECT id_encuesta, encuesta_pregunta, encuesta_descripcion, fecha_carga FROM encuestas WHERE id_encuesta = '$id_encuesta'";
$rs_noticia = mysql_query($query_rs_noticia)or die(mysql_error());
$row_rs_noticia = mysql_fetch_assoc($rs_noticia);
$totalrow_rs_noticia = mysql_num_rows($rs_noticia);

$query_rs_opciones = "SELECT id_opcion, opcion_nombre FROM encuestas_opciones WHERE id_encuesta = '$id_encuesta'";
$rs_opciones = mysql_query($query_rs_opciones)or die(mysql_error());
$row_rs_opciones = mysql_fetch_assoc($rs_opciones);
$totalrow_rs_opciones = mysql_num_rows($rs_opciones);


$encuesta_pregunta = trim($row_rs_noticia['encuesta_pregunta']);
$encuesta_descripcion = trim($row_rs_noticia['encuesta_descripcion']);
$encuesta_fecha_carga = trim($row_rs_noticia['fecha_carga']);
desconectar();
do {
	$opcion_encuesta[$row_rs_opciones['id_opcion']] = $row_rs_opciones['opcion_nombre']; 
}while($row_rs_opciones = mysql_fetch_assoc($rs_opciones));

}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link rel="shortcut icon" href="<?php echo $Servidor_url; ?>favicon.ico">
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<style type="text/css">

	#publicacion_fotos{
		width:620px;
		padding-right:20px;
		padding-top:10px;
		overflow-y:auto;
		border:1px solid #F60;
		-webkit-border-radius:6px;
		-moz-border-radius:6px;
		background:#F93;
	}
	 a {
		 color:#000;
	 }
	#upload{  
		padding:7px;
		margin:0 auto;
        text-align:center;  
		background-color:#F60;
		font-weight:bold;
        color:#000;  
        border:1px solid #000;  
		display:block;  
		text-decoration:none;
		width:80px;
		-webkit-border-radius:4px;
		-moz-border-radius:4px;
    }
		#upload a{
			color:#000;
    }
	
	#gallery{
		list-style:none;
		text-align:center;
		padding:0;
		margin-top:10px;
	}
	#gallery li{	
		display:block;
		float:left;
		width:100px;
		height:100px;
		background:#fff;
		border:1px solid #999;
		text-align:center;
		position:relative;
		padding:10px;
		margin-left:8px;
		margin-bottom:14px;
		-webkit-border-radius:4px;
		-moz-border-radius:4px;
	}
	#gallery li img{
		width:100px;
		height:100px
	}
	#gallery li a{
		position:absolute;
		right:10px;
		top:10px
	}
	#gallery li a img{ width:auto; height:auto}
	
	#crear_publicacion_descripcion {
		width:650px;
		height:400px;
	}
	.noticia_titulo {
		font-size:36px;
		padding:10px;
		width:96%;
		min-height:100px;
	}
	.noticia_bajada {
		font-size:18px;
		min-height:100px;
		padding:10px;
		width:96%;
	}
	.noticia_destacado {
		font-size:18px;
		min-height:100px;
		color:#d3222c;
		padding:10px;
		width:96%;
	}	
	#contenido_principal input[type='text'] {
		margin-top:0;
		margin-left:0;
		width:96%;
	}

	.elegir_noticia {
		margin-top:10px;
		padding:15px;
		background:#6CF;
		border:1px solid #66F;
	}
	.elegir_noticia h3{
		margin-top:0;
	}
	.elegir_noticia select{
		width:100%;
	}
	.opciones_encuesta p {
		margin-bottom:10px;
		font-size:18px;
		padding-left:10px;		
	}
	.opciones_encuesta a {
		color:#06F;
		text-decoration:none;
	}
	.opciones_encuesta a:hover {
		text-decoration:underline;
	}	
</style>
  	  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $Servidor_url;?>00-Javascripts/ajax.js"></script> 

	<script type="text/javascript" src="00-appendo/jquery.pack.js"></script>
	<script type="text/javascript" src="00-appendo/jquery.appendo.js"></script>
<script type="text/javascript">
jQuery(function() { 
		jQuery('#noticia_encuestas').appendo({ maxRows: 1000, labelAdd: 'Opción(+)', labelDel: 'Opción(-)'});

});

function confirmar()
{
	if(confirm('¿Estas seguro de eliminar esta opción?'))
		return true;
	else
		return false;
}
</script>
</head>

<body>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
  <h1>Editar Encuesta</h1>
  <div class="elegir_noticia">
  <h3>Encuesta:</h3>
  <form action="<?php echo $_SERVER['PHP_SELF']; ?>" name="form_elegir">
  <select name="encuesta" onchange="document.forms.form_elegir.submit()">
  <option value="0">Elegir una Encuesta:</option>
  <?php do { 
	if($id_encuesta == $row_rs_elegir_noticia['id_encuesta']) {
		$seleccionar = 'selected="selected"';
	}else{
		$seleccionar = '';
	}
		$cuantohace = 'cargada '.cuantoHace($row_rs_elegir_noticia['fecha_carga']);
  ?>

    <option <?php echo $seleccionar; ?> value="<?php echo $row_rs_elegir_noticia['id_encuesta']; ?>"><?php echo $row_rs_elegir_noticia['encuesta_pregunta']; ?> - <?php echo $cuantohace;?></option>
  <?php } while($row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia)) ?>

  </select>
  </form>  
  </div>
  <?php if($totalrow_rs_noticia) { ?>
  <form action="php/editar-encuesta-db.php" id="formulario_mbp" name="formulario_mbp" onsubmit="return controlar_formulario()" method="post">
	<input type="hidden" value="<?php echo $id_encuesta;?>" name="form_id_encuesta" /> 
  <h3>Pregunta:</h3>
  <p><input type="text" value="<?php echo $encuesta_pregunta;?>"placeholder="Pregunta" id="form_pregunta" name="form_pregunta" required="required"/></p>
  <h3>Descripción:</h3>
  <p><textarea placeholder="Descripción de la Encuesta" class="formulario_bajada"  id="form_bajada" name="form_descripcion" required="required"><?php echo $encuesta_descripcion;?></textarea></p>

<h3>Opciones:</h3>
<?php if($totalrow_rs_opciones) { ?>
<div class="opciones_encuesta">
<?php
foreach ($opcion_encuesta as $clave => $valor) {
	echo '<p>'.$valor.' <a href="php/eliminar-opcion.php?encuesta='.$id_encuesta.'&opcion='.$clave.'" onclick="return confirmar()">[Eliminar]</a></p>';
}?>
</div>
<?php } ?>
  	<TABLE id="noticia_encuestas" class="tabla_noticia">
	<TBODY>
  	<TR>
		<TD> 
     <p><input type="text" placeholder="Opción de encuenta" name="noticia_encuestas[]" id="noticia_encuestas" /></p>
  		</TD> 
	</TR>
	</TBODY></TABLE>   
  <p><center><input type="submit" value="Editar Encuesta" id="btn_cargar_noticia" name="btn_cargar_noticia" /></center></p>
  </form>
</div>
  <?php } ?>
    <div class="eliminar_flotante"></div>
  </div>

  <!-- end .content --></div>
<p>&nbsp;</p>
  <!-- end .container --></div>
</body>
</html>
