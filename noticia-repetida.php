<?php 

include('../paginas_include/variables-generales.php');
$nivel_pagina = 0;
include('php/verificar-permisos.php');

conectar('sitioweb');

$id_noticia = trim($_GET['noticia']);

if($id_noticia) {
$query_rs_noticia = "SELECT id_noticia, noticia_titulo FROM noticias WHERE id_noticia = $id_noticia";
$rs_noticia = mysql_query($query_rs_noticia)or die(mysql_error());
$row_rs_noticia = mysql_fetch_assoc($rs_noticia);
$totalrow_rs_noticia = mysql_num_rows($rs_noticia);

desconectar();

$noticia_titulo = $row_rs_noticia['noticia_titulo'];

$noticia_cuerpo = str_replace('.', '.<br>', $noticia_cuerpo);

}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link rel="shortcut icon" href="<?php echo $Servidor_url; ?>favicon.ico">
<script src="<?php echo $Servidor_url; ?>00-Javascripts/autocompletar/jquery-1.3.2.min.js" type="text/javascript"></script>

<link href="css/admin.css" rel="stylesheet" type="text/css" />
<style type="text/css">

	#publicacion_fotos{
		width:620px;
		padding-right:20px;
		padding-top:10px;
		overflow-y:auto;
		border:1px solid #F60;
		-webkit-border-radius:6px;
		-moz-border-radius:6px;
		background:#F93;
	}
	 a {
		 color:#000;
	 }
	#upload{  
		padding:7px;
		margin:0 auto;
        text-align:center;  
		background-color:#F60;
		font-weight:bold;
        color:#000;  
        border:1px solid #000;  
		display:block;  
		text-decoration:none;
		width:80px;
		-webkit-border-radius:4px;
		-moz-border-radius:4px;
    }
		#upload a{
			color:#000;
    }
	
	#gallery{
		list-style:none;
		text-align:center;
		padding:0;
		margin-top:10px;
	}
	#gallery li{	
		display:block;
		float:left;
		width:100px;
		height:100px;
		background:#fff;
		border:1px solid #999;
		text-align:center;
		position:relative;
		padding:10px;
		margin-left:8px;
		margin-bottom:14px;
		-webkit-border-radius:4px;
		-moz-border-radius:4px;
	}
	#gallery li img{
		width:100px;
		height:100px
	}
	#gallery li a{
		position:absolute;
		right:10px;
		top:10px
	}
	#gallery li a img{ width:auto; height:auto}
	
	#crear_publicacion_descripcion {
		width:650px;
		height:400px;
	}
	.noticia_titulo {
		font-size:36px;
		padding:10px;
		width:96%;
		min-height:100px;
	}
	.noticia_bajada {
		font-size:18px;
		min-height:100px;
		padding:10px;
		width:96%;
	}
	.noticia_destacado {
		font-size:18px;
		min-height:100px;
		color:#d3222c;
		padding:10px;
		width:96%;
	}	
	#contenido_principal input[type='text'] {
		margin-top:0;
		margin-left:0;
		width:96%;
	}

	.elegir_noticia {
		margin-top:10px;
		padding:15px;
		background:#6CF;
		border:1px solid #66F;
	}
	.elegir_noticia h3{
		margin-top:0;
	}
	.elegir_noticia select{
		width:100%;

	}
	
.vista_previa a{
	background:#60C;
	color:#fff;
	padding:10px;
	border:1px solid #600;
	text-decoration:none;
	margin-top:20px;
}

.vista_previa a:hover{
	background:#66F;
}
</style>
<script type="text/javascript" src="<?php echo $Servidor_url; ?>00-Javascripts/ajaxupload.js"></script>
	<script type="text/javascript" src="00-appendo/jquery.pack.js"></script>
	<script type="text/javascript" src="00-appendo/jquery.appendo.js"></script>
</head>

<body>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
  <h1>Cargar Noticia</h1>

  <?php if($totalrow_rs_noticia) { ?>
  <h3>Advertencia:</h3>
  <p>Ya hay una noticia cargada con información muy parecida a la que vos estás cargando ahora</p>
  <br />
  <p>La noticia cargada, lleva el título: <strong><?php echo $noticia_titulo;?></strong></p>
  <br />
    <center><div class="vista_previa"><a target="_blank" href="<?php echo $Servidor_url;?>admin/vista-previa.php?noticia=<?php echo $id_noticia; ?>">Vista Previa</a></div></center>
     <center style="margin-top:30px;"><div class="vista_previa"><a target="_blank" href="<?php echo $Servidor_url;?>admin/editar-noticia.php?noticia=<?php echo $id_noticia; ?>">Editar Noticia</a></div></center>


  <?php } ?>
    <div class="eliminar_flotante"></div>
  </div>

  <!-- end .content --></div>
<p>&nbsp;</p>
  <!-- end .container --></div>
</body>
</html>
