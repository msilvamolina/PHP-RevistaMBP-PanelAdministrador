<?php include('../paginas_include/variables-generales.php');?>
<?php
$nivel_pagina = 5;
include('php/verificar-permisos.php');
?>
<?php 

conectar('sitioweb');
$query_rs_autores = "SELECT autores.id_autor, autores.nombre_autor FROM autores ORDER BY autores.nombre_autor ASC";
$rs_autores = mysql_query($query_rs_autores)or die(mysql_error());
$row_rs_autores = mysql_fetch_assoc($rs_autores);
$totalrow_rs_autores = mysql_num_rows($rs_autores);
desconectar();

conectar('magazine');
$query_rs_revistas = "SELECT description, id FROM mag_numbers ORDER BY id ASC";
$rs_revistas = mysql_query($query_rs_revistas)or die(mysql_error());
$row_rs_revistas = mysql_fetch_assoc($rs_revistas);
$totalrow_rs_revistas = mysql_num_rows($rs_revistas);
desconectar();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<style>
.form_destacado {
	color:#d3222c;
}

table td {
	width:100%;
}
</style>
  	  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $Servidor_url;?>00-Javascripts/ajax.js"></script> 

	<script type="text/javascript" src="00-appendo/jquery.pack.js"></script>
	<script type="text/javascript" src="00-appendo/jquery.appendo.js"></script>
<script type="text/javascript">
function controlar_formulario() {
	chequear_direccion=document.formulario_mbp.chequear_direccion.value;

error=null;
	
	if(chequear_direccion !='<div class="registro_bien">OK!</div>') {
		error='pepe';
	}	
	
	if(error==null) {
		return true;
	} else {
		return false;
	}
}

function quitaacentos(t){
á="a";é="e";í="i";ó="o";ú="u";ñ="n";ä="a";ë="e";ï= "i";ö="o";ü="u";
Á="A";É="E";Í="I";Ó="O";Ú="U";Ñ="N";Ä="A";Ë="E";Ï= "I";Ö="O";ü="Ü";
acentos=/[áéíóúñäëïöüÁÉÍÓÚÑÄËÏÖÜ]/g;
return t.replace(acentos,
function($1){
return eval($1)
}
)
}


function armar_amigable() {
	titulo=document.formulario_mbp.form_titulo.value;
	document.formulario_mbp.form_tituloamigable.value = quitaacentos(titulo);
}

jQuery(function() { 
		jQuery('#noticia_autores').appendo({ maxRows: 1000, labelAdd: 'Autor(+)', labelDel: 'Autor(-)'});

});

function verificar_direccion(direccion_ingresada){
	//donde se mostrará el resultado de la eliminacion
	chequear_direccion=document.formulario_mbp.chequear_direccion;	
	divResultado = document.getElementById('verificar_diminuta');
		//instanciamos el objetoAjax
		ajax=nuevoAjax();
		//uso del medotod GET
		//indicamos el archivo que realizará el proceso de eliminación
		//junto con un valor que representa el id del empleado
		divResultado.innerHTML = '<div class="registro_cargando">Comprobando...</div>';
		ajax.open("GET", "<?php echo $Servidor_url;?>paginas_include/3-ajax/ajax-direccion-diminuta.php?direccion="+direccion_ingresada);
		ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText;
				chequear_direccion.value = ajax.responseText;				
			}
		}
		//como hacemos uso del metodo GET
		//colocamos null
		ajax.send(null)
}
</script>
</head>

<body>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
  <h1>Cargar Video</h1>
  <form action="php/cargar-video-db.php" id="formulario_mbp" name="formulario_mbp" onsubmit="return controlar_formulario()" method="post">
  <p><input type="text" placeholder="Título" id="form_titulo"  onchange="armar_amigable()" name="form_titulo" required="required"/></p>
	<input type="hidden"  id="form_tituloamigable" name="form_tituloamigable"/>
 <p><textarea placeholder="Descripción" required="required" class="formulario_bajada"  id="form_bajada" name="form_descripcion"></textarea></p>
   <p><input type="text" placeholder="Link de Youtube" id="form_youtube" name="form_youtube" required="required"/></p>
  <h3>Dirección Diminuta:</h3>  
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>mbp.pe/<input type="text" name="direccion_diminuta" onkeyup="verificar_direccion(this.value)" placeholder="Dirección" style="color:#999;width:250px" maxlength="20" class="crear_direccion" value="<?php echo $noticia_titulo_corto;?>" required="required"/><input type="hidden" name="chequear_direccion" id="chequear_direccion"  /></td>
    <td><div id="verificar_diminuta"></div></td>
  </tr>
</table>
 <br />
  <p><center><input type="submit" value="Cargar Video" id="btn_cargar_noticia" name="btn_cargar_noticia" /></center></p>
  </form>
</div>
  
  </div>

  <!-- end .content --></div>
<p>&nbsp;</p>
  <!-- end .container --></div>
</body>
</html>
