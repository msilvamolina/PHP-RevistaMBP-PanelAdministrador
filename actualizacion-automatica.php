<?php include('../paginas_include/variables-generales.php');?>
<?php
$nivel_pagina = 99;
include('php/verificar-permisos.php');
?>
<?php 

conectar('sitioweb');
$query_rs_autores = "SELECT autores.id_autor, autores.nombre_autor FROM autores ORDER BY autores.nombre_autor ASC";
$rs_autores = mysql_query($query_rs_autores)or die(mysql_error());
$row_rs_autores = mysql_fetch_assoc($rs_autores);
$totalrow_rs_autores = mysql_num_rows($rs_autores);
desconectar();

conectar('magazine');
$query_rs_revistas = "SELECT description, id FROM mag_numbers ORDER BY id ASC";
$rs_revistas = mysql_query($query_rs_revistas)or die(mysql_error());
$row_rs_revistas = mysql_fetch_assoc($rs_revistas);
$totalrow_rs_revistas = mysql_num_rows($rs_revistas);
desconectar();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<style>
.form_destacado {
	color:#d3222c;
}

table td {
	width:100%;
}
</style>
  	  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $Servidor_url;?>00-Javascripts/ajax.js"></script> 

	<script type="text/javascript" src="00-appendo/jquery.pack.js"></script>
	<script type="text/javascript" src="00-appendo/jquery.appendo.js"></script>
<script type="text/javascript">
function controlar_formulario() {
	antetitulo=document.formulario_mbp.form_antetitulo.value;
	titulo=document.formulario_mbp.form_titulo.value;
	bajada=document.formulario_mbp.form_bajada.value;
	noticia=document.formulario_mbp.form_noticia.value;
	chequear_direccion=document.formulario_mbp.chequear_direccion.value;

error=null;
	
	if(chequear_direccion !='<div class="registro_bien">OK!</div>') {
		error='pepe';
	}	
	if(!antetitulo) {
		error='pepe';
	}
	if(!titulo) {
		error='pepe';
	}
	if(!bajada) {
		error='pepe';
	}
	if(!noticia) {
		error='pepe';
	}
	
	if(error==null) {
		return true;
	} else {
		return false;
	}
}

function quitaacentos(t){
á="a";é="e";í="i";ó="o";ú="u";ñ="n";ä="a";ë="e";ï= "i";ö="o";ü="u";
Á="A";É="E";Í="I";Ó="O";Ú="U";Ñ="N";Ä="A";Ë="E";Ï= "I";Ö="O";ü="Ü";
acentos=/[áéíóúñäëïöüÁÉÍÓÚÑÄËÏÖÜ]/g;
return t.replace(acentos,
function($1){
return eval($1)
}
)
}


function armar_amigable() {
	titulo=document.formulario_mbp.form_titulo.value;
	document.formulario_mbp.form_tituloamigable.value = quitaacentos(titulo);
}

jQuery(function() { 
		jQuery('#horario').appendo({ maxRows: 1000, labelAdd: 'Hora(+)', labelDel: 'Hora(-)'});

});

function verificar_direccion(direccion_ingresada){
	//donde se mostrará el resultado de la eliminacion
	chequear_direccion=document.formulario_mbp.chequear_direccion;	
	divResultado = document.getElementById('verificar_diminuta');
		//instanciamos el objetoAjax
		ajax=nuevoAjax();
		//uso del medotod GET
		//indicamos el archivo que realizará el proceso de eliminación
		//junto con un valor que representa el id del empleado
		divResultado.innerHTML = '<div class="registro_cargando">Comprobando...</div>';
		ajax.open("GET", "<?php echo $Servidor_url;?>paginas_include/3-ajax/ajax-direccion-diminuta.php?direccion="+direccion_ingresada);
		ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText;
				chequear_direccion.value = ajax.responseText;				
			}
		}
		//como hacemos uso del metodo GET
		//colocamos null
		ajax.send(null)
}
</script>
</head>

<body>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
  <h1>Actualización Automática</h1>
<form method="post" action="php/actualizacion-automatica-db.php">
<br />
<h3><input type="radio" name="activar_actualizacion" value="1" required="required" /> Activar Actualización Automática</h3>
<h3><input type="radio" name="activar_actualizacion" value="0" required="required" /> Desactivar Actualización Automática</h3>
<hr />

<h3>Horarios:</h3>  
  	<TABLE id="horario" class="tabla_noticia">
	<TBODY>   
  	<TR>
		<TD> 
    <p><div class="selecionar_autor">      

<select name="horario[]" id="horario[]" required="required">
	<option value="7:00">7:00</option>
	<option value="7:15">7:15</option>
	<option value="7:30">7:30</option>
	<option value="7:45">7:45</option>
 
	<option value="8:00">8:00</option>
	<option value="8:15">8:15</option>
	<option value="8:30">8:30</option>
	<option value="8:45">8:45</option>
 
	<option value="9:00">9:00</option>
	<option value="9:15">9:15</option>
	<option value="9:30">9:30</option>
	<option value="9:45">9:45</option>
 
	<option value="10:00">10:00</option>
	<option value="10:15">10:15</option>
	<option value="10:30">10:30</option>
	<option value="10:45">10:45</option>
 
	<option value="11:00">11:00</option>
	<option value="11:15">11:15</option>
	<option value="11:30">11:30</option>
	<option value="11:45">11:45</option>
 
	<option value="12:00">12:00</option>
	<option value="12:15">12:15</option>
	<option value="12:30">12:30</option>
	<option value="12:45">12:45</option>
 
	<option value="13:00">13:00</option>
	<option value="13:15">13:15</option>
	<option value="13:30">13:30</option>
	<option value="13:45">13:45</option>
 
	<option value="14:00">14:00</option>
	<option value="14:15">14:15</option>
	<option value="14:30">14:30</option>
	<option value="14:45">14:45</option>
 
	<option value="15:00">15:00</option>
	<option value="15:15">15:15</option>
	<option value="15:30">15:30</option>
	<option value="15:45">15:45</option>
 
	<option value="16:00">16:00</option>
	<option value="16:15">16:15</option>
	<option value="16:30">16:30</option>
	<option value="16:45">16:45</option>
 
	<option value="17:00">17:00</option>
	<option value="17:15">17:15</option>
	<option value="17:30">17:30</option>
	<option value="17:45">17:45</option>
 
	<option value="18:00">18:00</option>
	<option value="18:15">18:15</option>
	<option value="18:30">18:30</option>
	<option value="18:45">18:45</option>
 
	<option value="19:00">19:00</option>
	<option value="19:15">19:15</option>
	<option value="19:30">19:30</option>
	<option value="19:45">19:45</option>
 
	<option value="20:00">20:00</option>
	<option value="20:15">20:15</option>
	<option value="20:30">20:30</option>
	<option value="20:45">20:45</option>
 
	<option value="21:00">21:00</option>
	<option value="21:15">21:15</option>
	<option value="21:30">21:30</option>
	<option value="21:45">21:45</option>
 
	<option value="22:00">22:00</option>
	<option value="22:15">22:15</option>
	<option value="22:30">22:30</option>
	<option value="22:45">22:45</option>
 
	<option value="23:00">23:00</option>
	<option value="23:15">23:15</option>
	<option value="23:30 PM">23:30</option>
	<option value="23:45 PM">23:45</option>
</select>
  </div></p>
  		</TD> 
	</TR>   
	</TBODY></TABLE>   
<h3>Cantidad de Noticias a Publicar:</h3>  
   <input type="text" name="cantidad_noticias" value="1" style="color:#999;width:250px" maxlength="20" class="crear_direccion" required="required"/>

<h3>Noticias Principales:</h3>  
   <input type="text" name="noticias_principales" value="5" style="color:#999;width:250px" maxlength="20" class="crear_direccion" required="required"/>
<h3>Noticias Mosaicos:</h3>  
   <input type="text" name="noticias_mosaicos" value="24" style="color:#999;width:250px" maxlength="20" class="crear_direccion" required="required"/>
   
  <p><center><input type="submit" value="Crear Actualización" id="btn_cargar_noticia" name="btn_cargar_noticia" /></center></p>
  </form>
    <div class="eliminar_flotante"></div>

  


  <!-- end .content --></div>
<p>&nbsp;</p>
  <!-- end .container --></div>
</body>
</html>
