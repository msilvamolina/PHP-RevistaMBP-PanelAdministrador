<?php include('../paginas_include/variables-generales.php');?>
<?php
$nivel_pagina = 99;
include('php/verificar-permisos.php');

conectar('estadisticas');

$query_rs_registro = "SELECT * FROM panel_administrador ORDER BY id_estadistica DESC LIMIT 100";
$rs_registro = mysql_query($query_rs_registro)or die(mysql_error());
$row_rs_registro = mysql_fetch_assoc($rs_registro);
$totalrow_rs_registro = mysql_num_rows($rs_registro);

desconectar();
conectar('sitioweb');
$query_rs_administradores = "SELECT id_administrador, administrador_nombre FROM paneladministrador";
$rs_administradores = mysql_query($query_rs_administradores)or die(mysql_error());
$row_rs_administradores = mysql_fetch_assoc($rs_administradores);
$totalrow_rs_administradores = mysql_num_rows($rs_administradores);

$query_rs_noticias = "SELECT id_noticia, noticia_titulo, noticia_url FROM noticias";
$rs_noticias = mysql_query($query_rs_noticias)or die(mysql_error());
$row_rs_noticias = mysql_fetch_assoc($rs_noticias);
$totalrow_rs_noticias = mysql_num_rows($rs_noticias);

$query_rs_emails = "SELECT id_email, email_usuario FROM emails";
$rs_emails = mysql_query($query_rs_emails)or die(mysql_error());
$row_rs_emails = mysql_fetch_assoc($rs_emails);
$totalrow_rs_emails = mysql_num_rows($rs_emails);

do {
	$usuarios[$row_rs_administradores['id_administrador']] = $row_rs_administradores['administrador_nombre'];
} while($row_rs_administradores = mysql_fetch_assoc($rs_administradores));

do {
	$noticia_titulo[$row_rs_noticias['id_noticia']] = $row_rs_noticias['noticia_titulo'];
	$noticia_url[$row_rs_noticias['id_noticia']] = $_SERVER['PHP_SELF'].'?noticia='.$row_rs_noticias['id_noticia'];
} while($row_rs_noticias = mysql_fetch_assoc($rs_noticias));

do {
	$direccion_email[$row_rs_emails['id_email']] = $row_rs_emails['email_usuario'].'@revistambp.com';
} while($row_rs_emails = mysql_fetch_assoc($rs_emails));

$operaciones['Cargar Noticia'] = 'cargó la noticia';
$operaciones['Editar Noticia'] = 'editó la noticia';
$operaciones['Subir Foto'] = 'subió una foto de la noticia';
$operaciones['Publicar Noticia'] = 'la noticia';
$operaciones['Elegir Portada'] = 'eligió la portada de ';
$operaciones['Administrar Fotos'] = 'administró las fotos de la noticia';
$operaciones['Crear E-mail'] = 'creó el e-mail';
$operaciones['Armar Portada'] = 'armó una nueva portada';
$operaciones['Cambiar Contrasena'] = 'cambió su contraseña';
$operaciones['Editar HTML'] = 'editó el código HTML de la nota';
$operaciones['Dar Formato'] = 'dio formato a la noticia';
$operaciones['Cargar Diminuta'] = 'cargó la dirección diminuta de la nota';
$operaciones['Identificar IP'] = 'identificó una dirección ip';
$operaciones['Borrar Noticia'] = 'borró una noticia';
$operaciones['Borrar Foto'] = 'borró una foto de la noticia';
$operaciones['Armar Empresas'] = 'armó una nueva portada para la sección empresas';
$operaciones['Cargar Video'] = 'cargó el video';
$operaciones['Cargar Encuesta'] = 'cargó la encuesta';
$operaciones['Armar Encuestas'] = 'armó una nueva portada para la sección encuestas de opinión';
$operaciones['Crear Usuario'] = 'creó el usuario';
$operaciones['Crear Autor'] = 'creó el autor';
$operaciones['Eliminar Formato'] = 'eliminó el formato de la noticia';
$operaciones['Iniciar Sesion'] = 'Inició Sesión en el Panel Administrador';
$operaciones['Cerrar Sesion'] = 'Cerró Sesión en el Panel Administrador';
$operaciones['Ingresar a E-mail'] = 'ingresó a su cuenta de e-mail';
$operaciones['Recortar Foto'] = 'recortó la foto';
$operaciones['Recortar Miniatura'] = 'recortó la miniatura de una foto de';
$operaciones['Guardar Recorte Foto'] = 'guardó el recorte de una foto de';
$operaciones['Guardar Recorte Miniatura'] = 'guardó el recorte de la miniatura de una foto de';
$operaciones['Subir Video a Noticia'] = 'subió un video a la noticia';
$operaciones['Cargar Pagina de Revista'] = 'Cargó una página de la revista';
$operaciones['Modificar Pagina de Revista'] = 'Cargó una versión Flash de una página de la revista';
$operaciones['Eliminar Pagina Revista'] = 'Eliminó una página de la revista';
$operaciones['Cargar Revista'] = 'Creó la revista';

desconectar();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../00-Javascripts/jquery.js"></script>
<script type="text/javascript" src="../00-Javascripts/ajax.js"></script>
<style>
.contenedor_listas {
	margin-top:10px;
}
.contenedor_listas ul li{
	list-style:none;
}

.contenedor_listas ul li a{
	color:#06F;
}

.contenedor_listas ul li a:hover{
	color:#F90;
}

.actividad_1, .actividad_2 {
	padding:15px 0px;
}
.actividad_1 {
	background:#ffcccc;
}

.actividad_2 {
	background:#fde7cc;
}

.contenedor_listas ul li .nombre_usuario{
	font-weight:bold;
	color:#e6260f;
	display:inline-block;
	text-decoration:none;
}
.contenedor_listas ul li .nombre_usuario:hover {
	color:#ff4242;
}

.hora_operacion {
	float:left;
	font-weight:bold;
	color:#fff;
	background:#0065ef;
	padding:5px;
	margin-left:8px;
	margin-right:8px;
	margin-top:10px;
	margin-bottom:5px;
	
}
</style>
</head>

<body>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
  <h1>Registro de Actividad</h1>
  <?php if(($_GET['noticia'])or($_GET['usuario'])) { ?>
  <br />
<a href="<?php echo $_SERVER['PHP_SELF']; ?>">Ver Todo</a>  
<?php } ?>
  <?php if($_GET['noticia']) { ?>
  <br /><br />
<a href="vista-previa.php?noticia=<?php echo $_GET['noticia'];?>">Ver Noticia</a>  
<?php } ?>
  <div class="contenedor_listas">
 <ul>
<?php  	
//armar_registro
$fecha_mostrar = NULL;
$autorizar_fecha = 0;

function diaSemana($ano,$mes,$dia)
{
	// 0->domingo	 | 6->sabado
	$dia= date("w",mktime(0, 0, 0, $mes, $dia, $ano));
		return $dia;
}

function armar_fecha($fecha) {
	$armar_fecha = explode('-', $fecha);
	$dia = $armar_fecha[2];
	$mes = $armar_fecha[1];
	$ano = $armar_fecha[0];
	
	$diaSemana = diaSemana($ano, $mes, $dia);
	
	$dia = ltrim($dia, '0');
	$mes = ltrim($mes, '0');

$dias_nombre[0] = 'Domingo';
$dias_nombre[1] = 'Lunes'; 
$dias_nombre[2] = 'Martes'; 
$dias_nombre[3] = 'Miércoles'; 
$dias_nombre[4] = 'Jueves'; 
$dias_nombre[5] = 'Viernes'; 
$dias_nombre[6] = 'Sábado'; 
	
$meses_nombre[1] = 'Enero'; 
$meses_nombre[2] = 'Febrero'; 
$meses_nombre[3] = 'Marzo'; 
$meses_nombre[4] = 'Abril'; 
$meses_nombre[5] = 'Mayo'; 
$meses_nombre[6] = 'Junio'; 
$meses_nombre[7] = 'Julio'; 
$meses_nombre[8] = 'Agosto'; 
$meses_nombre[9] = 'Septiembre'; 
$meses_nombre[10] = 'Octubre'; 
$meses_nombre[11] = 'Noviembre'; 
$meses_nombre[12] = 'Diciembre'; 

	$nueva_fecha = $dias_nombre[$diaSemana].' '.$dia.' de '.$meses_nombre[$mes].' del '.$ano;
	
	return $nueva_fecha;	
}

$i=1;

$usuario_solicitado = trim($_GET['usuario']);
$noticia_solicitado = trim($_GET['noticia']);

do {
	$fecha_mostrar = $row_rs_registro['fecha_operacion'];
	$fecha_mostrar2 = explode(' ', $fecha_mostrar);
	$mostrar_fecha = $fecha_mostrar2[0];
	
	$hora = explode(':', $fecha_mostrar2[1]);
	
	$hora_mostrar = $hora[0].':'.$hora[1];

	$operacion = $operaciones[$row_rs_registro['operacion']];
	if($row_rs_registro['operacion'] == 'Publicar Noticia') {
		if($row_rs_registro['tipo_operacion'] == 'publicar'){
			$operacion = 'publicó la noticia ';
		} else {
			$operacion = 'quitó la publicación de la noticia ';
		}
	}
	
	if($row_rs_registro['operacion'] == 'Cargar Noticia') {
		if($row_rs_registro['tipo_operacion']) {
			$operacion = $operacion.' <i>('.$row_rs_registro['tipo_operacion'].')</i>';
		}
	}
	
	$mostrar_li_viejo = $mostrar_fecha_li;
	
	if($usuario_solicitado) {
		if($usuario_solicitado == $row_rs_registro['id_administrador']) {
			$mostrar_fecha_li = '<li><h3>'.armar_fecha($mostrar_fecha).'</h3></li>';
		}
	}elseif($noticia_solicitado) {
		if($noticia_solicitado == $row_rs_registro['id_noticia']) {
			$mostrar_fecha_li = '<li><h3>'.armar_fecha($mostrar_fecha).'</h3></li>';
		}
	}else{
		$mostrar_fecha_li = '<li><h3>'.armar_fecha($mostrar_fecha).'</h3></li>';
	}
	
$link_usuario = $_SERVER['PHP_SELF'].'?usuario='.$row_rs_registro['id_administrador'];

$usuario_mostrar = '<a href="'.$link_usuario.'" class="nombre_usuario">'.$usuarios[$row_rs_registro['id_administrador']].'</a>';

	if($row_rs_registro['operacion'] == 'Ingresar a E-mail') {
		$id_email = $row_rs_registro['id_usuario'];
		$usuario_mostrar = '<b>'.$direccion_email[$row_rs_registro['id_email']].'</b>';	
	}
	
	$mostrar = '<div class="hora_operacion">'.$hora_mostrar.'</div><li class="actividad_'.$i.'">'.$usuario_mostrar.' '.$operacion;

	if($row_rs_registro['id_noticia']) {

		$noticia_mostrar = '<a href="'.$noticia_url[$row_rs_registro['id_noticia']].'">'.$noticia_titulo[$row_rs_registro['id_noticia']].'</a>';
		$mostrar = $mostrar.' '.$noticia_mostrar;
	}
		if($row_rs_registro['id_revista']) {

		$noticia_mostrar = $row_rs_registro['id_revista'];
		$mostrar = $mostrar.' '.$noticia_mostrar;
	}
	
	if($row_rs_registro['operacion'] == 'Crear E-mail') {
		$id_email = $row_rs_registro['id_usuario'];
		$mostrar = $mostrar.' <b>'.$direccion_email[$id_email].'</b>';	
	}
	$mostrar = $mostrar.'</li>';
	
	if($mostrar_li_viejo != $mostrar_fecha_li) {
	echo $mostrar_fecha_li;
	}
	
	if($usuario_solicitado) {
		if($usuario_solicitado == $row_rs_registro['id_administrador']) {
			echo $mostrar;
		}
	}elseif($noticia_solicitado) {
		if($noticia_solicitado == $row_rs_registro['id_noticia']) {
			echo $mostrar;
		}
	}else{
		echo $mostrar;
	}
	
	$i++;
	
	if($i==3) {
		$i=1;
	}

} while($row_rs_registro = mysql_fetch_assoc($rs_registro));
?>
 </ul>
 </div>
</div>
 <div class="eliminar_flotante"></div> 
  </div>

  <!-- end .content --></div>
  <!-- end .container --></div>
</body>
</html>
