<?php include('../paginas_include/variables-generales.php');?>
<?php
$nivel_pagina = 0;
include('php/verificar-permisos.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function controlar_formulario() {
	contrasena_actual=document.formulario_mbp.form_contrasena_actual.value;
	contrasena_nueva1=document.formulario_mbp.form_contrasena_nueva1.value;
	contrasena_nueva2=document.formulario_mbp.form_contrasena_nueva2.value;

error=null;
	
	if(!contrasena_actual) {
		error='pepe';
	}
	if(!contrasena_nueva1) {
		error='pepe';
	}
	if(!contrasena_nueva2) {
		error='pepe';
	}
	
	if(contrasena_nueva1 != contrasena_nueva2) {
		alert('Las contraseñas tienen que ser iguales')
		return false;
	}
		if(error==null) {
		return true;
	} else {
		return false;
	}
}
</script>
</head>
<body>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
  <h1>Cambiar Contraseña</h1>
    <?php if($_GET['contrasena']=='incorrecta'){ ?>
  <div class="formulario_error">La contraseña ingresada como "Contraseña Actual", es incorrecta</div>
  <?php } ?>
   <form action="php/cambiar-contrasena-db.php" id="formulario_mbp" name="formulario_mbp" onsubmit="return controlar_formulario()" method="post">
  <p><input type="password" placeholder="Contraseña Actual" id="form_antetitulo" name="form_contrasena_actual"/></p>
  <br/>
  <p><input type="password" placeholder="Contraseña Nueva" id="form_titulo" name="form_contrasena_nueva1"/></p>
    <p><input type="password" placeholder="Repetir Contraseña" id="form_titulo" name="form_contrasena_nueva2"/></p>
    <br />
    <p><center><input type="submit" value="Cambiar Contraseña" id="btn_cargar_noticia" name="btn_cargar_noticia" /></center></p>
  </form>
</div>
 <div class="eliminar_flotante"></div> 
  </div>

  <!-- end .content --></div>
  <!-- end .container --></div>
</body>
</html>
