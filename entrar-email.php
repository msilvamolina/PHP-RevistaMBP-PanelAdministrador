<?php 

include('../paginas_include/variables-generales.php');
$nivel_pagina = 99;
include('php/verificar-permisos.php');

conectar('sitioweb');

$id_email = trim($_GET['email']);

$query_rs_elegir_noticia = "SELECT id_email, email_usuario FROM emails ORDER BY id_email ASC";
$rs_elegir_noticia = mysql_query($query_rs_elegir_noticia)or die(mysql_error());
$row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia);
$totalrow_rs_elegir_noticia = mysql_num_rows($rs_elegir_noticia);

if($id_email) {
$query_rs_noticia = "SELECT id_email, email_usuario, email_contrasena FROM emails WHERE id_email = $id_email";
$rs_noticia = mysql_query($query_rs_noticia)or die(mysql_error());
$row_rs_noticia = mysql_fetch_assoc($rs_noticia);
$totalrow_rs_noticia = mysql_num_rows($rs_noticia);

desconectar();


}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link rel="shortcut icon" href="<?php echo $Servidor_url; ?>favicon.ico">
<script src="<?php echo $Servidor_url; ?>00-Javascripts/autocompletar/jquery-1.3.2.min.js" type="text/javascript"></script>

<link href="css/admin.css" rel="stylesheet" type="text/css" />
<style type="text/css">

	#publicacion_fotos{
		width:620px;
		padding-right:20px;
		padding-top:10px;
		overflow-y:auto;
		border:1px solid #F60;
		-webkit-border-radius:6px;
		-moz-border-radius:6px;
		background:#F93;
	}
	 a {
		 color:#000;
	 }
	#upload{  
		padding:7px;
		margin:0 auto;
        text-align:center;  
		background-color:#F60;
		font-weight:bold;
        color:#000;  
        border:1px solid #000;  
		display:block;  
		text-decoration:none;
		width:80px;
		-webkit-border-radius:4px;
		-moz-border-radius:4px;
    }
		#upload a{
			color:#000;
    }
	
	#gallery{
		list-style:none;
		text-align:center;
		padding:0;
		margin-top:10px;
	}
	#gallery li{	
		display:block;
		float:left;
		width:100px;
		height:100px;
		background:#fff;
		border:1px solid #999;
		text-align:center;
		position:relative;
		padding:10px;
		margin-left:8px;
		margin-bottom:14px;
		-webkit-border-radius:4px;
		-moz-border-radius:4px;
	}
	#gallery li img{
		width:100px;
		height:100px
	}
	#gallery li a{
		position:absolute;
		right:10px;
		top:10px
	}
	#gallery li a img{ width:auto; height:auto}
	
	#crear_publicacion_descripcion {
		width:650px;
		height:400px;
	}
	.noticia_titulo {
		font-size:36px;
		padding:10px;
		width:96%;
		min-height:100px;
	}
	.noticia_bajada {
		font-size:18px;
		min-height:100px;
		padding:10px;
		width:96%;
	}
	.noticia_destacado {
		font-size:18px;
		min-height:100px;
		color:#d3222c;
		padding:10px;
		width:96%;
	}	
	#contenido_principal input[type='text'] {
		margin-top:0;
		margin-left:0;
		width:96%;
	}

	.elegir_noticia {
		margin-top:10px;
		padding:15px;
		background:#6CF;
		border:1px solid #66F;
	}
	.elegir_noticia h3{
		margin-top:0;
	}
	.elegir_noticia select{
		width:100%;

	}
</style>
<script type="text/javascript" src="<?php echo $Servidor_url; ?>00-Javascripts/ajaxupload.js"></script>
	<script type="text/javascript" src="00-appendo/jquery.pack.js"></script>
	<script type="text/javascript" src="00-appendo/jquery.appendo.js"></script>
</head>

<body>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
  <h1>Entrar a un e-mail:</h1>
  <div class="elegir_noticia">
  <h3>E-mail:</h3>
  <form action="<?php echo $_SERVER['PHP_SELF']; ?>" name="form_elegir">
  <select name="email" onchange="document.forms.form_elegir.submit()">
  <option value="0">Elegir un E-mail:</option>
  <?php do { 
	if($id_email == $row_rs_elegir_noticia['id_email']) {
		$seleccionar = 'selected="selected"';
	}else{
		$seleccionar = '';
	}

  ?>

    <option <?php echo $seleccionar; ?> value="<?php echo $row_rs_elegir_noticia['id_email']; ?>"><?php echo $row_rs_elegir_noticia['email_usuario']; ?>@revistambp.com</option>
  <?php } while($row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia)) ?>

  </select>
  </form>  
  </div>
  <?php if($totalrow_rs_noticia) { ?>
  <h3>Acceder al e-mail <i><?php echo $row_rs_noticia['email_usuario'];?>@revistambp.com</i></h3>
  <br/>
  <p>Usuario: <?php echo $row_rs_noticia['email_usuario'];?></p>
  <p>Contraseña: <?php echo $row_rs_noticia['email_contrasena'];?></p>
   <form action="<?php echo $Servidor_url;?>email/php/loguear-usuario.php" id="formulario_mbp"  onsubmit="return controlar_formulario()" name="formulario_mbp" method="post">
  <p><input type="hidden"  autofocus="autofocus" placeholder="Usuario" id="form_email_revista" name="form_email_revista" required="required" value="<?php echo $row_rs_noticia['email_usuario'];?>"/></p>
  <p><input type="hidden"placeholder="Contraseña" id="form_contrasena_revista" name="form_contrasena_revista" required="required" value="<?php echo $row_rs_noticia['email_contrasena'];?>"/></p>
  <p><center><input type="submit"  onclick="controlar_formulario()"value="Ingresar" id="btn_cargar_noticia" name="btn_cargar_noticia" /></center></p>
  </form>
</div>
  <?php } ?>
    <div class="eliminar_flotante"></div>
  </div>

  <!-- end .content --></div>
<p>&nbsp;</p>
  <!-- end .container --></div>
</body>
</html>
