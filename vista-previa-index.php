<?php 
$ubicacion_mobile = '../';
include('../paginas_include/variables-generales.php'); ?>
<?php include('../paginas_include/1-php/detectar-dispositivo.php'); ?>
<?php

//cargar portada

$id_noticia = $_GET['noticia'];
conectar('sitioweb');


$noticias_principales[] = $id_noticia;

$noticias_mosaicos[] = $id_noticia;
$noticias_mosaicos[] = $id_noticia;
$noticias_mosaicos[] = $id_noticia;
$noticias_mosaicos[] = $id_noticia;
$noticias_mosaicos[] = $id_noticia;
$noticias_mosaicos[] = $id_noticia;
//armar array noticias

$query_rs_noticias = "SELECT id_noticia, noticia_antetitulo, noticia_titulo, noticia_titulo_corto, noticia_bajada, noticia_url, foto_portada FROM noticias WHERE id_noticia = $id_noticia";
$rs_noticias = mysql_query($query_rs_noticias)or die(mysql_error());
$row_rs_noticias = mysql_fetch_assoc($rs_noticias);
$totalrow_rs_noticias = mysql_num_rows($rs_noticias);

$query_rs_imagenes = "SELECT id_foto, nombre_foto, recorte_foto_nombre, recorte_foto_miniatura FROM fotos_publicaciones WHERE id_publicacion = $id_noticia";
$rs_imagenes = mysql_query($query_rs_imagenes)or die(mysql_error());
$row_rs_imagenes = mysql_fetch_assoc($rs_imagenes);
$totalrow_rs_imagenes = mysql_num_rows($rs_imagenes);

//funcion para cortar texto
function getSubString($string, $length=NULL) {
    //Si no se especifica la longitud por defecto es 50
    if ($length == NULL)
        $length = 50;
    //Primero eliminamos las etiquetas html y luego cortamos el string
    $stringDisplay = substr(strip_tags($string), 0, $length);
    //Si el texto es mayor que la longitud se agrega puntos suspensivos
    if (strlen(strip_tags($string)) > $length)
        $stringDisplay .= ' (...)';
    return $stringDisplay;
}


do {
	$array_imagenes[$row_rs_imagenes['id_foto']] = $row_rs_imagenes['nombre_foto'];
	$array_imagenes_recorte[$row_rs_imagenes['id_foto']] = $row_rs_imagenes['recorte_foto_nombre'];
	$array_imagenes_recorte_miniatura[$row_rs_imagenes['id_foto']] = $row_rs_imagenes['recorte_foto_miniatura'];
}while($row_rs_imagenes = mysql_fetch_assoc($rs_imagenes));

do {
$foto_portada = $row_rs_noticias['foto_portada'];
	if($foto_portada) {
		$imagen_mostrar = $array_imagenes[$foto_portada];
	} else {
		$imagen_mostrar = 'sin-imagen.jpg';	
	}
if($row_rs_noticias['noticia_titulo_corto']) {
	$titulo_mostrar = $row_rs_noticias['noticia_titulo_corto'];
}else {
	$titulo_mostrar = getSubString($row_rs_noticias['noticia_titulo'], 52);
}
	$array_noticias_antetitulo[$row_rs_noticias['id_noticia']] = $row_rs_noticias['noticia_antetitulo'];
	$array_noticias_titulo[$row_rs_noticias['id_noticia']] = $titulo_mostrar;
	$array_noticias_titulo_largo[$row_rs_noticias['id_noticia']] = $row_rs_noticias['noticia_titulo'];
	$array_noticias_bajada[$row_rs_noticias['id_noticia']] = getSubString($row_rs_noticias['noticia_bajada'], 240);
	$array_noticias_url[$row_rs_noticias['id_noticia']] = $row_rs_noticias['noticia_url'];
	$array_noticias_imagen[$row_rs_noticias['id_noticia']] = $imagen_mostrar;
	$array_noticias_imagen_recorte[$row_rs_noticias['id_noticia']] = $array_imagenes_recorte[$foto_portada];
	$array_noticias_imagen_recorte_miniatura[$row_rs_noticias['id_noticia']] = $array_imagenes_recorte_miniatura[$foto_portada];

} while($row_rs_noticias = mysql_fetch_assoc($rs_noticias));

desconectar();

?>
<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="">
<!--<![endif]-->
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Revista MBP - La revista que leen los que deciden</title>
<link href="<?php echo $Servidor_url;?>boilerplate.css" rel="stylesheet" type="text/css">
  <meta name="description" content="MBP es una revista de Marketing, Branding y Publicidad, producida en San Miguel de Tucumán y destinada a los empresarios, estudiantes y profesionales de Marketing y Ciencias de la Comunicación del NOA."/>
    <meta name="author" content="Martín Silva Molina">
    <link rel="canonical" href="<?php echo $Servidor_url;?>" />
    <meta property="og:url" content="<?php echo $Servidor_url;?>">
    <meta property="og:title" content="Revista MBP - la revista que leen los que deciden">
    <meta property="og:description" content="MBP es una revista de Marketing, Branding y Publicidad, producida en San Miguel de Tucumán y destinada a los empresarios, estudiantes y profesionales de Marketing y Ciencias de la Comunicación del NOA.">
    <meta property="og:image" content="<?php echo $Servidor_url;?>imagenes/estructura/og-MBP.jpg">

	<?php $pagina_en_construccion=1;
	include('../paginas_include/1-php/javascript-general.php'); ?>
        
  <script src="<?php echo $Servidor_url;?>ResponsiveSlider/responsiveslides.min.js"></script>
<script src="<?php echo $Servidor_url;?>respond.min.js"></script>
  <link rel="stylesheet" href="<?php echo $Servidor_url;?>ResponsiveSlider/responsiveslides.css">
    <link rel="stylesheet" href="<?php echo $Servidor_url;?>css/principal.css" type="text/css" media="screen" /> 
    <link rel="stylesheet" href="<?php echo $Servidor_url;?>js/responsive-nav.css">
    <link rel="stylesheet" href="<?php echo $Servidor_url;?>js/responsive-barra.css">
    	<link rel="stylesheet" href="<?php echo $Servidor_url;?>css/mosaic.css" type="text/css" media="screen" />
		<!--[if lt IE 9]>
    <link rel="stylesheet" href="<?php echo $Servidor_url;?>css/cssiexplorer.css" type="text/css" media="screen" /> 
		<![endif]-->
<?php if($dispositivo == 'Computadora') { ?>        
<style>
	.mosaic-overlay {
		display:none;
		z-index:5;
		position:absolute;
		width:100%;
		height:100%;
		background:#111;
	}
</style>
<?php } ?>
<style>
.error404 {
	padding:10px;
	padding-left:15px;
	color:#000;
	width:300px;
	margin-bottom:15px;
	clear:none;	
}
.error404 h1{
	color:#C00;
}

#barra_vista_previa {
	background:#FC0;
	color:#000;
	font-weight:bold;
	width:100$;
	text-align:center;
	padding:10px;
}

body {
	background:#333;
}

.boton_vista {
	display:inline;
	padding:10px;
	background:#F30;
	color:#fff;
}
.boton_vista:hover {
	background:#F60;
	color:#fff;
}
.boton_vista2 {
	display:inline;
	padding:10px;
	background:#606;
	color:#fff;
}
.boton_vista2:hover {
	background:#F60;
	color:#fff;
}

</style>   
	<script type="text/javascript" src="<?php echo $Servidor_url;?>js/mosaic.1.0.1.js"></script>    
    <script src="<?php echo $Servidor_url;?>js/responsive-nav.js"></script>
		<script type="text/javascript">  
			
			jQuery(function($){
				$('.bar3').mosaic({
					animation	:	'slide',		//fade or slide
					anchor_y	:	'top'		//Vertical anchor position
				});
				
		    });
		    
    // You can also use "$(window).load(function() {"
    $(function () {
      // Slideshow 4
      $("#slider4").responsiveSlides({
        auto: true,
        pager: false,
        nav: true,
        speed: 300,
        namespace: "callbacks",
        before: function () {
          $('.events').append("<li>before event fired.</li>");
        },
        after: function () {
          $('.events').append("<li>after event fired.</li>");
        }
      });

    });
  </script>
</head>
<!-- 
To learn more about the conditional comments around the html tags at the top of the file:
paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/

Do the following if you're using your customized build of modernizr (http://www.modernizr.com/):
* insert the link to your js here
* remove the link below to the html5shiv
* add the "no-js" class to the html tags at the top
* you can also remove the link to respond.min.js if you included the MQ Polyfill in your modernizr build 
-->
<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body>
<div id="barra_vista_previa"><a href="editar-noticia.php?noticia=<?php echo $id_noticia; ?>"><div class="boton_vista2">Editar Noticia</div></a><a href="vista-previa.php?noticia=<?php echo $id_noticia; ?>"><div class="boton_vista">Ver Vista previa Noticia</div></a> Estás viendo una vista previa de la portada</div>
	<div class="gridContainer  clearfix">
       <?php include('../paginas_include/2-estructura/encabezado.php'); ?>

          <div id="contenido_pagina" class="fluid">
        <!-- Slideshow 4 -->
    <div class="contenedor_slider">    
    <div class="callbacks_container">
                        <?php if($_GET['error']) { ?>  
     <div class="fluid error404">
     <h1>Error</h1>
     <p>La página que estás buscando no existe</p>
     </div>
     <?php }  ?>
      <ul class="rslides" id="slider4">
    
<?php foreach ($noticias_principales as $clave){  ?>
        <li><a href="<?php echo $array_noticias_url[$clave];?>">
    <p class="slider_antetitulo"><?php echo $array_noticias_antetitulo[$clave];?></p>    
<selection class="slider_imagen">        
<?php if($array_noticias_imagen_recorte[$clave]) { ?>
<img src="<?php echo $Servidor_url; ?>imagenes/noticias/fotos/recortes/<?php echo $array_noticias_imagen_recorte[$clave];?>"  alt="<?php echo $array_noticias_titulo_largo[$clave];?>"  width="637" height="399"/>
<?php } else { ?>
 <img src="<?php echo $Servidor_url; ?>00-Librerias/libreria-imagenes/scripts/recortar-imagen.php?tamano=637x399&imagen=<?php echo $array_noticias_imagen[$clave];?>"  alt="<?php echo $array_noticias_titulo_largo[$clave];?>"  width="637" height="399"/>
<?php } ?>
</selection>
<selection class="slider_imagen_cel">
<?php if($array_noticias_imagen_recorte_miniatura[$clave]) { ?>

 <img src="<?php echo $Servidor_url; ?>imagenes/noticias/fotos/recortes/<?php echo $array_noticias_imagen_recorte_miniatura[$clave];?>"  alt="<?php echo $array_noticias_titulo_largo[$clave];?>"/>
 
<?php } else { ?>
 <img src="<?php echo $Servidor_url; ?>00-Librerias/libreria-imagenes/scripts/recortar-imagen.php?tamano=320x320&imagen=<?php echo $array_noticias_imagen[$clave];?>"  alt="<?php echo $array_noticias_titulo_largo[$clave];?>"  width="320" height="320"/>
<?php } ?>
</selection>
<p class="caption"><?php echo $array_noticias_titulo_largo[$clave];?></p>
      </a></li>
<?php } ?>        
      </ul>
    </div>
    </div>
    <?php include('../paginas_include/2-estructura/publicidad-armar.php'); ?>
    <?php include('../paginas_include/2-estructura/publicidad-home2.php'); ?>

<div id="contenedor_mosaicos">
<?php foreach ($noticias_mosaicos as $clave){  ?>
<div class="mosaic-block3 bar3">
			<a href="<?php echo $array_noticias_url[$clave];?>" class="mosaic-overlay">
				<div class="details">
					<p><br /><?php echo $array_noticias_bajada[$clave];?></p>
				</div>
			</a>
			<div class="mosaic-backdrop">
               <a href="<?php echo $array_noticias_url[$clave];?>">
               <span class="etiqueta2"><?php echo $array_noticias_antetitulo[$clave];?></span>
<?php if($array_noticias_imagen_recorte_miniatura[$clave]) {
			$imagen_miniatura = 'recortes/'.$array_noticias_imagen_recorte_miniatura[$clave];
		}else{
			$imagen_miniatura = $array_noticias_imagen[$clave];
		} ?>               
              <selection class="noticia_imagen_celular"> 
             <img src="<?php echo $Servidor_url; ?>00-Librerias/libreria-imagenes/scripts/recortar-imagen.php?tamano=308x180&imagen=<?php echo $imagen_miniatura;?>"  alt="<?php echo $array_noticias_titulo_largo[$clave];?>" width="308" height="108"/>
             </selection>
             <selection class="noticia_imagen_pc"> 
             <img src="<?php echo $Servidor_url; ?>00-Librerias/libreria-imagenes/scripts/recortar-imagen.php?tamano=225x170&imagen=<?php echo $imagen_miniatura;?>"  alt="<?php echo $array_noticias_titulo_largo[$clave];?>" width="225" height="170"/>
             </selection>
             	<div class="publicacion_titulo3"><h1><?php echo $array_noticias_titulo[$clave];?></h1>
</div>	
              </a>
		    </div>
    </div>
<?php } ?>
  </div>

          </div>
<div class="eliminar_flotante"></div>          
<?php include('../paginas_include/2-estructura/pie.php'); ?>
        </div>
        
	</div>
<br /><br />
    <script>
      var navigation = responsiveNav("#nav", {
        animate: true,        // Boolean: Use CSS3 transitions, true or false
        transition: 400,      // Integer: Speed of the transition, in milliseconds
        label: "",        // String: Label for the navigation toggle
        insert: "before",      // String: Insert the toggle before or after the navigation
        customToggle: "",     // Selector: Specify the ID of a custom toggle
        openPos: "relative",  // String: Position of the opened nav, relative or static
        jsClass: "js",        // String: 'JS enabled' class which is added to <html> el
        init: function(){},   // Function: Init callback
        open: function(){},   // Function: Open callback
        close: function(){}   // Function: Close callback
      });
		</script>
</body>
</html>
