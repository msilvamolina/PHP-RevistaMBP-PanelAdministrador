<?php include('../paginas_include/variables-generales.php');?>
<?php
$nivel_pagina = 5;
include('php/verificar-permisos.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link href="css/admin.css" rel="stylesheet" type="text/css" />
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
 <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
  <script src="js/estadisticas/morris.js"></script>
  <script src="js/estadisticas/examples/lib/prettify.js"></script>
  <script src="js/estadisticas/examples/lib/example.js"></script>
  <link rel="stylesheet" href="js/estadisticas/examples/lib/prettify.css">
  <link rel="stylesheet" href="js/estadisticas/morris.css">
  
<style>
p {
	padding-left:10px;
	padding-top:7px;
}

#graph {
  width: auto;
  height: 250px;
  margin: 20px auto 0 auto;
}
pre {
	display:none;
}

</style>

</head>

<body>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
<form>
<div class="elegir_noticia">
<h3>Elegir periodo de tiempo</h3>
<select >
<option value="hoy">Hoy</option>
<option value="Ayer">Ayer</option>
<option value="semana">Esta Semana</option>
<option value="semana-anterior">Semana Anterior</option>
<option value="este-mes">Este mes</option>
<option value="mes-anterior">Mes anterior</option>
</select>
</div>
<h1>Hoy</h1>
<br />
<h3>Visitas desde Dispositivos</h3>
<div id="barra_dispositivos"></div>
<h3>Visitas desde Navegadores</h3>
<div id="barra_navegadores"></div>
<h3>Notas más leídas</h3>
<div id="barra_notas"></div>
<h3>Revistas más leídas</h3>
<div id="barra_revistas"></div>
<h3>Formatos de Revistas más leídos</h3>
<div id="torta_revistas"></div>
<h3>Autores: Más visitados</h3>
<div id="barra_autores"></div>
<h3>Autores: Cantidad de Notas</h3>
<div id="barra_autores_notas"></div>
<h3>Palabras más Buscadas</h3>
<div id="barra_palabras"></div>
<h3>Frases más Buscadas</h3>
<div id="barra_frases"></div>

<br />
<h1>Redes Sociales</h1>
<br />
<h3>Canales Propios más visitados</h3>
<div id="torta_redes_sociales"></div>
<h3>Notas Compartidas</h3>
<div id="torta_redes_sociales_notas"></div>
<h3>Notas más Compartidas</h3>
<div id="barra_notas_compartidas"></div>

<br />
<h1>Panel Administrador</h1>
<br />
<h3>Usuarios que más suben notas</h3>
<div id="barra_usuarios_notas"></div>
<h3>Usuarios frecuentes</h3>
<div id="barra_usuarios_frecuentes"></div>

 <div class="eliminar_flotante"></div> 
  </div>

  <!-- end .content --></div>
  <!-- end .container --></div>
  <script type="text/javascript">
new Morris.Bar({
  element: 'barra_dispositivos',
  data: [
    {x: 'Cel Android', y: 460},
    {x: 'Tablet Android', y: 550},
    {x: 'Iphone', y: 450},
    {x: 'Ipad', y: 300},
    {x: 'Pc', y: 400},
    {x: 'Linux', y: 500},
    {x: 'Mac', y: 600},
  ],
  xkey: 'x',
  ykeys: ['y'],
  labels: ['Y'],
  barColors: function (row, series, type) {
 if (type === 'bar') {
      var red = Math.ceil(244 * row.y / this.ymax);
	  var blue = Math.ceil(114 * row.y / this.ymax);
      var green = Math.ceil(27 * row.y / this.ymax);

      return 'rgb(' + red + ', ' + blue + ', ' + green +')';
    }
    else {
      return '#000';
    }
  }
});
new Morris.Bar({
  element: 'barra_navegadores',
  data: [
    {x: 'Chrome', y: 460},
    {x: 'Firefox', y: 850},
    {x: 'Explorer', y: 450},
    {x: 'Safari', y: 300},
    {x: 'Otro', y: 100},
  ],
  xkey: 'x',
  ykeys: ['y'],
  labels: ['Y'],
  barColors: function (row, series, type) {
    if (type === 'bar') {
      var red = Math.ceil(57 * row.y / this.ymax);
	  var blue = Math.ceil(99 * row.y / this.ymax);
      var green = Math.ceil(167 * row.y / this.ymax);

      return 'rgb(' + red + ', ' + blue + ', ' + green +')';
    }
    else {
      return '#000';
    }
  }
});
new Morris.Bar({
  element: 'barra_notas',
  data: [
    {x: '¿Cómo llegar a la cabeza del consumidor?', y: 460},
    {x: '¿Cómo llegar a la cabeza del consumidor?', y: 850},
    {x: '¿Cómo llegar a la cabeza del consumidor?', y: 450},
    {x: '¿Cómo llegar a la cabeza del consumidor?', y: 300},
    {x: '¿Cómo llegar a la cabeza del consumidor?', y: 100},
  ],
  xkey: 'x',
  ykeys: ['y'],
  labels: ['Y'],
  barColors: function (row, series, type) {
    if (type === 'bar') {
      var red = Math.ceil(51 * row.y / this.ymax);
	  var blue = Math.ceil(198 * row.y / this.ymax);
      var green = Math.ceil(32 * row.y / this.ymax);

      return 'rgb(' + red + ', ' + blue + ', ' + green +')';
    }
    else {
      return '#000';
    }
  }
});
new Morris.Bar({
  element: 'barra_revistas',
  data: [
    {x: 'Revista MBP 1', y: 460},
    {x: 'Revista MBP 2', y: 850},
    {x: 'Revista MBP 3', y: 450},
    {x: 'Revista MBP 4', y: 300},
    {x: 'Revista MBP 5', y: 100},
    {x: 'Revista MBP 6', y: 100},
    {x: 'Revista MBP 7', y: 800},		
  ],
  xkey: 'x',
  ykeys: ['y'],
  labels: ['Y'],
  barColors: function (row, series, type) {
    if (type === 'bar') {
      var red = Math.ceil(158 * row.y / this.ymax);
	  var blue = Math.ceil(6 * row.y / this.ymax);
      var green = Math.ceil(149 * row.y / this.ymax);

      return 'rgb(' + red + ', ' + blue + ', ' + green +')';
    }
    else {
      return '#000';
    }
  }
});
new Morris.Donut({
  element: 'torta_revistas',
  data: [
    {value: 70, label: 'FlipPage', formatted: '70%' },
    {value: 30, label: 'Móvil', formatted: '30%' },
  ],
  formatter: function (x, data) { return data.formatted; }
});
new Morris.Bar({
  element: 'barra_autores',
  data: [
    {x: 'Ileana Dulout', y: 460},
    {x: 'Fabián Silva Molina', y: 850},
    {x: 'Franco Barriga', y: 450},
    {x: 'Valeria Vega', y: 300},
    {x: 'Martín Silva Molina', y: 100},
    {x: 'Cecilia Luna', y: 100},
    {x: 'Paula Silva Molina', y: 800},		
  ],
  xkey: 'x',
  ykeys: ['y'],
  labels: ['Y'],
  barColors: function (row, series, type) {
    if (type === 'bar') {
      var red = Math.ceil(253 * row.y / this.ymax);
	  var blue = Math.ceil(240 * row.y / this.ymax);
      var green = Math.ceil(27 * row.y / this.ymax);

      return 'rgb(' + red + ', ' + blue + ', ' + green +')';
    }
    else {
      return '#000';
    }
  }
});
new Morris.Bar({
  element: 'barra_autores_notas',
  data: [
    {x: 'Ileana Dulout', y: 20},
    {x: 'Fabián Silva Molina', y: 15},
    {x: 'Franco Barriga', y: 30},
    {x: 'Valeria Vega', y: 6},
    {x: 'Martín Silva Molina', y: 6},
    {x: 'Cecilia Luna', y: 8},
    {x: 'Paula Silva Molina', y: 1},		
  ],
  xkey: 'x',
  ykeys: ['y'],
  labels: ['Y'],
  barColors: function (row, series, type) {
    if (type === 'bar') {
      var red = Math.ceil(253 * row.y / this.ymax);
	  var blue = Math.ceil(240 * row.y / this.ymax);
      var green = Math.ceil(27 * row.y / this.ymax);

      return 'rgb(' + red + ', ' + blue + ', ' + green +')';
    }
    else {
      return '#000';
    }
  }
});
new Morris.Bar({
  element: 'barra_palabras',
  data: [
    {x: 'hola', y: 455},
    {x: 'revista', y: 153},
    {x: 'celulares', y: 330},
    {x: 'comprar', y: 643},
    {x: 'lindo', y: 68},
    {x: 'gorro', y: 82},
    {x: 'pepe', y: 154},		
  ],
  xkey: 'x',
  ykeys: ['y'],
  labels: ['Y'],
  barColors: function (row, series, type) {
    if (type === 'bar') {
      var red = Math.ceil(82 * row.y / this.ymax);
	  var blue = Math.ceil(230 * row.y / this.ymax);
      var green = Math.ceil(200 * row.y / this.ymax);

      return 'rgb(' + red + ', ' + blue + ', ' + green +')';
    }
    else {
      return '#000';
    }
  }
});
new Morris.Bar({
  element: 'barra_frases',
  data: [
    {x: 'como hacer publicidad', y: 455},
    {x: 'como buscar en internet', y: 153},
    {x: 'cocinar fideos', y: 330},
    {x: 'pepe pepe', y: 643},
    {x: 'esta es una frase', y: 68},
    {x: 'esta es otra', y: 82},	
  ],
  xkey: 'x',
  ykeys: ['y'],
  labels: ['Y'],
  barColors: function (row, series, type) {
    if (type === 'bar') {
      var red = Math.ceil(82 * row.y / this.ymax);
	  var blue = Math.ceil(230 * row.y / this.ymax);
      var green = Math.ceil(200 * row.y / this.ymax);

      return 'rgb(' + red + ', ' + blue + ', ' + green +')';
    }
    else {
      return '#000';
    }
  }
});
new Morris.Donut({
  element: 'torta_redes_sociales',
  data: [
    {value: 40, label: 'Facebook', formatted: '40%' },
    {value: 30, label: 'Twitter', formatted: '30%' },
    {value: 20, label: 'Google +', formatted: '20%' },	
    {value: 10, label: 'Youtube', formatted: '10%' },
  ],
  formatter: function (x, data) { return data.formatted; }
});
new Morris.Donut({
  element: 'torta_redes_sociales_notas',
  data: [
    {value: 30, label: 'Facebook', formatted: '30%' },
    {value: 40, label: 'Twitter', formatted: '40%' },
    {value: 15, label: 'Google +', formatted: '15%' },	
    {value: 15, label: 'E-Mail', formatted: '15%' },
  ],
  formatter: function (x, data) { return data.formatted; }
});
new Morris.Bar({
  element: 'barra_notas_compartidas',
  data: [
    {x: '¿Cómo llegar a la cabeza del consumidor?', y: 460},
    {x: '¿Cómo llegar a la cabeza del consumidor?', y: 850},
    {x: '¿Cómo llegar a la cabeza del consumidor?', y: 450},
    {x: '¿Cómo llegar a la cabeza del consumidor?', y: 300},
    {x: '¿Cómo llegar a la cabeza del consumidor?', y: 100},
  ],
  xkey: 'x',
  ykeys: ['y'],
  labels: ['Y'],
  barColors: function (row, series, type) {
    if (type === 'bar') {
      var red = Math.ceil(51 * row.y / this.ymax);
	  var blue = Math.ceil(198 * row.y / this.ymax);
      var green = Math.ceil(32 * row.y / this.ymax);

      return 'rgb(' + red + ', ' + blue + ', ' + green +')';
    }
    else {
      return '#000';
    }
  }
});
new Morris.Bar({
  element: 'barra_usuarios_notas',
  data: [
    {x: 'Ileana Dulout', y: 20},
    {x: 'Fabián Silva Molina', y: 15},
    {x: 'Franco Barriga', y: 30},
    {x: 'Valeria Vega', y: 6},
    {x: 'Martín Silva Molina', y: 6},
    {x: 'Cecilia Luna', y: 8},
    {x: 'Paula Silva Molina', y: 1},		
  ],
  xkey: 'x',
  ykeys: ['y'],
  labels: ['Y'],
  barColors: function (row, series, type) {
    if (type === 'bar') {
      var red = Math.ceil(253 * row.y / this.ymax);
	  var blue = Math.ceil(240 * row.y / this.ymax);
      var green = Math.ceil(27 * row.y / this.ymax);

      return 'rgb(' + red + ', ' + blue + ', ' + green +')';
    }
    else {
      return '#000';
    }
  }
});
new Morris.Bar({
  element: 'barra_usuarios_frecuentes',
  data: [
    {x: 'Ileana Dulout', y: 20},
    {x: 'Fabián Silva Molina', y: 15},
    {x: 'Franco Barriga', y: 30},
    {x: 'Valeria Vega', y: 6},
    {x: 'Martín Silva Molina', y: 6},
    {x: 'Cecilia Luna', y: 8},
    {x: 'Paula Silva Molina', y: 1},		
  ],
  xkey: 'x',
  ykeys: ['y'],
  labels: ['Y'],
  barColors: function (row, series, type) {
    if (type === 'bar') {
      var red = Math.ceil(253 * row.y / this.ymax);
	  var blue = Math.ceil(240 * row.y / this.ymax);
      var green = Math.ceil(27 * row.y / this.ymax);

      return 'rgb(' + red + ', ' + blue + ', ' + green +')';
    }
    else {
      return '#000';
    }
  }
});
</script>
</body>
</html>
