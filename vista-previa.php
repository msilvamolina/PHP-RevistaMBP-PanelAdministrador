<?php $pagina_en_construccion = 0;?>
<?php 
$ubicacion_mobile = '../';
include('../paginas_include/variables-generales.php'); ?>
<?php include('../paginas_include/1-php/detectar-dispositivo.php'); ?>
<?php
$noticia_no_redireccionar = 1;

$nivel_pagina = 0;
include('php/verificar-permisos.php');

$id_noticia = trim($_GET['noticia']);

if($id_noticia) {
	
conectar('sitioweb');
	
$query_rs_noticia = "SELECT id_autor, noticia_antetitulo, noticia_titulo, noticia_bajada, noticia_cuerpo, noticia_cuerpo_con_formato, foto_portada, noticia_url, video_link, noticia_publicada FROM noticias WHERE id_noticia = '$id_noticia'";
$rs_noticia = mysql_query($query_rs_noticia)or die(mysql_error());
$row_rs_noticia = mysql_fetch_assoc($rs_noticia);
$totalrow_rs_noticia = mysql_num_rows($rs_noticia);

$query_rs_diminuta = "SELECT direccion_diminuta FROM direccion_diminuta WHERE id_noticia = '$id_noticia'";
$rs_diminuta = mysql_query($query_rs_diminuta)or die(mysql_error());
$row_rs_diminuta = mysql_fetch_assoc($rs_diminuta);
$totalrow_rs_diminuta = mysql_num_rows($rs_diminuta);

$direccion_diminuta = 'http://mbp.pe/'.$row_rs_diminuta['direccion_diminuta'];

$direccion_diminuta_sin_http ='mbp.pe/'.$row_rs_diminuta['direccion_diminuta'];
desconectar();
//si la noticia no esta cargada en la base de datos, redirecciona a error

if(!$totalrow_rs_noticia) {
	$redireccion = $Servidor_url.'error404';
	header('Location: '.$redireccion);
	exit;
}

//verificamos la direccion ingresada
$link_db = $row_rs_noticia['noticia_url'];
$pagina_url ="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

$direccion_redes_sociales = $link_db;

//Vemos si la noticia esta publicada
$noticia_publicada = $row_rs_noticia['noticia_publicada'];

if($noticia_publicada) {
	$redireccion = $link_db;
	header('Location: '.$redireccion);
	exit;
}

if(!$noticia_no_redireccionar) {
if($link_db != $pagina_url) {
	$redireccion = $link_db;
	header('Location: '.$redireccion);
	exit;
}
}

$noticia_url = $link_db;

//Guardar estadistica

conectar('sitioweb');

$query_rs_verificar_formato = "SELECT formato_antetitulo_fondo, formato_antetitulo_color, formato_titulo_color, formato_autor_color, formato_bajada_color, formato_h2_color, formato_h4_color, formato_h4_fondo, formato_letra_capital_color FROM formato_noticia WHERE id_noticia = $id_noticia";
$rs_verificar_formato = mysql_query($query_rs_verificar_formato)or die(mysql_error());
$row_rs_verificar_formato = mysql_fetch_assoc($rs_verificar_formato);
$totalrow_rs_verificar_formato = mysql_num_rows($rs_verificar_formato);

if(!$totalrow_rs_verificar_formato) {
	$query_rs_verificar_formato = "SELECT formato_antetitulo_fondo, formato_antetitulo_color, formato_titulo_color, formato_autor_color, formato_bajada_color, formato_h2_color, formato_h4_color, formato_h4_fondo, formato_letra_capital_color FROM formato_noticia WHERE id_noticia = 0";
$rs_verificar_formato = mysql_query($query_rs_verificar_formato)or die(mysql_error());
$row_rs_verificar_formato = mysql_fetch_assoc($rs_verificar_formato);
$totalrow_rs_verificar_formato = mysql_num_rows($rs_verificar_formato);
}

//cargar fotos
$query_rs_fotos_noticia = "SELECT nombre_foto, recorte_foto_nombre, recorte_foto_miniatura FROM fotos_publicaciones WHERE id_publicacion = $id_noticia";
$rs_fotos_noticia = mysql_query($query_rs_fotos_noticia)or die(mysql_error());
$row_rs_fotos_noticia = mysql_fetch_assoc($rs_fotos_noticia);
$totalrow_rs_fotos_noticia = mysql_num_rows($rs_fotos_noticia);

//Antetitulo
$formato_antetitulo_fondo = $row_rs_verificar_formato['formato_antetitulo_fondo'];
$formato_antetitulo_color = $row_rs_verificar_formato['formato_antetitulo_color'];

$formato_titulo_color = $row_rs_verificar_formato['formato_titulo_color'];
$formato_autor_color = $row_rs_verificar_formato['formato_autor_color'];

$formato_bajada_color = $row_rs_verificar_formato['formato_bajada_color'];
$formato_h2_color = $row_rs_verificar_formato['formato_h2_color'];

$formato_h4_color = $row_rs_verificar_formato['formato_h4_color'];
$formato_h4_fondo = $row_rs_verificar_formato['formato_h4_fondo'];

$formato_letra_capital_color = $row_rs_verificar_formato['formato_letra_capital_color'];

$query_rs_vinculaciones_autores = "SELECT autores.id_autor, autores.nombre_autor, vinculacion_autor_noticia.id_vinculacion FROM autores, vinculacion_autor_noticia WHERE vinculacion_autor_noticia.id_noticia = $id_noticia AND vinculacion_autor_noticia.id_autor = autores.id_autor";
$rs_vinculaciones_autores = mysql_query($query_rs_vinculaciones_autores)or die(mysql_error());
$row_rs_vinculaciones_autores = mysql_fetch_assoc($rs_vinculaciones_autores);
$totalrow_rs_vinculaciones_autores = mysql_num_rows($rs_vinculaciones_autores);

$noticia_antetitulo = $row_rs_noticia['noticia_antetitulo'];
$noticia_titulo = $row_rs_noticia['noticia_titulo'];
$noticia_bajada = $row_rs_noticia['noticia_bajada'];
$noticia_cuerpo = $row_rs_noticia['noticia_cuerpo'];

$video_link = $row_rs_noticia['video_link'];

$noticia_cuerpo = $row_rs_noticia['noticia_cuerpo'];

if($row_rs_noticia['noticia_cuerpo_con_formato']){
	$noticia_cuerpo = $row_rs_noticia['noticia_cuerpo_con_formato'];
}

$noticia_autores = NULL;
if($totalrow_rs_vinculaciones_autores) {
//armar autores
$i = 1;
$separador_autor = ', ';
do {
	if(!$noticia_autores ) {
		$noticia_autores = 'Por '.$row_rs_vinculaciones_autores['nombre_autor'];
	}else{
		if($i == $totalrow_rs_vinculaciones_autores) {
			$separador_autor = ' y ';
		}
		$noticia_autores = $noticia_autores.$separador_autor.$row_rs_vinculaciones_autores['nombre_autor'];
	}
	$i++;
 } while($row_rs_vinculaciones_autores = mysql_fetch_assoc($rs_vinculaciones_autores));
}

if($row_rs_noticia['noticia_cuerpo_con_formato']) {
$noticia_cuerpo = $row_rs_noticia['noticia_cuerpo_con_formato'];
}
	desconectar();
}

//poner publicidad en noticia
include('../paginas_include/2-estructura/publicidad-armar.php');

$publicidad_derecha[0] ='<img src="'.$Servidor_url.'imagenes/publicidad297x245.jpg" width="300" height="250"/>';


//Armar version para tablet
//Mezclar cuerpo y publicidad para version movil

$publicidad_agregar = '<%PUBLICIDAD%>';
$noticia_separada = str_replace("<div>"," ", $noticia_cuerpo);

$noticia_separada = str_replace("</div>"," ", $noticia_separada);
$noticia_separada = explode('<br>', $noticia_separada);
$cantidad_de_parrafos = 4;

$i=0;
$p=0;
$texto_celulares = NULL;
$agregado = NULL;

$n_parrafo = 0;
foreach ($noticia_separada as $clave => $parrafo){
	//Depurar parrafos
	$noticia_separada[$clave] = trim($noticia_separada[$clave]);
	if($n_parrafo == 0) {
		$div_parrafo = '<div class="parrafo_dispositivo_movil primer_parrafo_movil">';
	} else {
		$div_parrafo = '<div class="parrafo_dispositivo_movil">';	
	}
	$noticia_separada[$clave] = $div_parrafo.$noticia_separada[$clave].'</div>';	

$n_parrafo++;	
}

$separacion_parrafo = '<div class="parrafo_separacion"><br></div>';
foreach ($noticia_separada as $parrafo){ 
	//Agregar_publicidad
	if($i==$cantidad_de_parrafos) {
		//agrego publicidad
		$texto_celulares = $texto_celulares.$separacion_parrafo.$publicidad_agregar.$parrafo;
		$i=0;
	} else {
		if($texto_celulares){
		$texto_celulares = $texto_celulares.$separacion_parrafo.$parrafo;	
		}else{
		$texto_celulares = $parrafo;	
		}
		$i++;
	}
	
	
}

$texto_celulares = trim($texto_celulares.$separacion_parrafo);

//reemplazamos <%PUBLICIDAD%> por la publicidad que corresponda

$texto_celulares_nuevo = explode('<%PUBLICIDAD%>', $texto_celulares);

$nuevo_texto_celular = NULL;

$p = 0;
foreach ($texto_celulares_nuevo as $parrafo) {
		$div_publicidad = '<div class="publicidad_verde_noticia">';
		$div_publicidad_cerrar = '</div>';
		if($publicidad_para_mostrar[$p]) {
		$nuevo_texto_celular = $nuevo_texto_celular.$div_publicidad.$publicidad_para_mostrar[$p].$div_publicidad_cerrar.$parrafo;
		$p++;
		} else {
		$nuevo_texto_celular = $nuevo_texto_celular.$parrafo;
		}
		
}

$texto_celulares = trim($nuevo_texto_celular);


if($totalrow_rs_fotos_noticia) {
	$foto_og = $Servidor_url.'00-Librerias/libreria-imagenes/scripts/recortar-imagen.php?tamano=400x400&imagen='.$row_rs_fotos_noticia['nombre_foto'];
}else{
	$foto_og = $Servidor_url.'imagenes/estructura/og-MBP.jpg';
}

	$foto_og2 = $Servidor_url.'imagenes/estructura/og-MBP.jpg';
	
	$titulo_noticia = $noticia_titulo;

?>
<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="">
<!--<![endif]-->
<head prefix="og: http://ogp.me/ns#">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Revista MBP - <?php echo $noticia_titulo; ?></title>
<link href="<?php echo $Servidor_url;?>boilerplate.css" rel="stylesheet" type="text/css">
   <meta name="description" content="<?php echo $noticia_bajada; ?>"/>
    <meta name="author" content="Martín Silva Molina">
    <link rel="canonical" href="<?php echo $noticia_url;?>" />
    <meta property="og:url" content="<?php echo $noticia_url;?>">
    <meta property="og:title" content="Revista MBP - <?php echo $noticia_titulo; ?>">
    <meta property="og:site_name" content="Revista MBP">
    <meta property="og:description" content="<?php echo $noticia_bajada; ?>">
    
    <link href="<?php echo $foto_og;?>" rel="image_src" />
    <meta property="og:image" content="<?php echo $foto_og;?>">
    <meta property="og:image:type" content="image/jpeg">
	<meta property="og:image:width" content="400">
	<meta property="og:image:height" content="400">
    <script src="<?php echo $Servidor_url; ?>js/ajax.js"></script>

  <script src="<?php echo $Servidor_url;?>ResponsiveSlider/responsiveslides.min.js"></script>
<script src="<?php echo $Servidor_url;?>respond.min.js"></script>
  <link rel="stylesheet" href="<?php echo $Servidor_url;?>ResponsiveSlider/responsiveslides.css">
    <link rel="stylesheet" href="<?php echo $Servidor_url;?>css/principal.css" type="text/css" media="screen" /> 
    <link rel="stylesheet" href="<?php echo $Servidor_url;?>js/responsive-nav.css">
    <link rel="stylesheet" href="<?php echo $Servidor_url;?>js/responsive-barra.css">
    <script src="<?php echo $Servidor_url;?>js/responsive-nav.js"></script>
		<script type="text/javascript">  
		    
    // You can also use "$(window).load(function() {"
    $(function () {
      // Slideshow 4
      $("#slider4").responsiveSlides({
        auto: true,
        pager: false,
        nav: true,
        speed: 300,
        namespace: "callbacks",
        before: function () {
          $('.events').append("<li>before event fired.</li>");
        },
        after: function () {
          $('.events').append("<li>after event fired.</li>");
        }
      });

    });

function ajax_compartir_noticia(){
	//controlar form
	tu_nombre=document.formulario_mbp.tu_nombre.value;
	tu_email=document.formulario_mbp.tu_email.value;
	email_amigo=document.formulario_mbp.email_amigo.value;
	comentario_adicional=document.formulario_mbp.comentario_adicional.value;
	noticia=document.formulario_mbp.noticia.value;

error=null;
	
	if(!tu_nombre) {
		error='pepe';
	}
	if(!tu_email) {
		error='pepe';
	}
	if(!email_amigo) {
		error='pepe';
	}
	if(error==null) {
	//donde se mostrará el resultado de la eliminacion
	divResultado = document.getElementById('contenedor_compartir_noticia');
		//instanciamos el objetoAjax
		ajax=nuevoAjax();
		//uso del medotod GET
		//indicamos el archivo que realizará el proceso de eliminación
		//junto con un valor que representa el id del empleado
		divResultado.innerHTML = '<center><img src="<?php echo $Servidor_url;?>imagenes/cargando.gif" width="32" height="32" /></center>';
		ajax.open("POST", "<?php echo $Servidor_url;?>popup/db/compartir-noticia-db.php",true);
		ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
					divResultado.innerHTML = ajax.responseText;			
			}
		}
			ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			ajax.send('tu_nombre='+tu_nombre+'&tu_email='+tu_email+'&email_amigo='+email_amigo+'&comentario_adicional='+comentario_adicional+'&noticia='+noticia);
		//Refresca la página
	}
}
</script>
<style>
h1 {
	color:<?php echo $formato_titulo_color; ?>;
}
.noticia_antetitulo selection{
	background:<?php echo $formato_antetitulo_fondo; ?>;
	color:<?php echo $formato_antetitulo_color; ?>;
	
}
.articulo_bajada {
	color:<?php echo $formato_bajada_color; ?>;
	text-align:left;
}

.articulo_autor {
	color:<?php echo $formato_autor_color; ?>;
}
h4 {
	margin-top:10px;
	padding:20px;
	margin-right:15px;
	background:<?php echo $formato_h4_fondo; ?>;
	color:<?php echo $formato_h4_color; ?>;
	font-weight:bold;
	text-align:left;
	font-size:24px;
	line-height:25px;
	word-wrap: break-word;
}

h2 {
	color:<?php echo $formato_h2_color; ?>;
	line-height:30px;
	text-align:left;
	width:100%;
}

.compartir_noticia {
	background:#CCC;
	padding:10px 15px;
	margin-top:10px;
	margin-bottom:10px;
	width:290px;
}
.redes_sociales_compartir{
	width:100%;
	text-align:center;
	clear:none;
}

.redes_sociales_compartir ul li{
	display:inline;
}

.redes_sociales_compartir ul li img{
	width:12%;
}


.noticia_antetitulo {
	font-size:18px;
	font-weight:bold;
		margin-top:10px;
		margin-bottom:10px;
}
.noticia_antetitulo selection{
		padding:10px;
		padding-left:20px;
}

.texto_para_celulares div{
		padding-left:10px;
	padding-right:20px;
	color:#000;
text-align:left;
font-size:18px;
font-weight: normal;
line-height:20px;
}

.texto_para_celulares .publicidad_verde_noticia {
	width:100%;
	padding:0;
	padding-top:10px;
	padding-bottom:10px;
	margin-bottom:20px;
	background:#CCC;
	text-align:center;
}

#contenido_pagina article .articulo_bajada{
	text-align:left;
}
.texto_para_celulares .primer_parrafo_movil:first-letter
{
   float:left;
   font-weight:bold;
   font-size:70px;
   line-height:45px;
   padding:5px;
   margin-right: 5px;
   	color:<?php echo $formato_letra_capital_color;?>

}

.articulo_cuerpo:first-letter,.noticia_primer_parrafo:first-letter
{
	color:<?php echo $formato_letra_capital_color;?>
}

.articulo_cuerpo,.noticia_primer_parrafo {
	text-align:left;
}
@media only screen and (min-width: 481px) {
	.compartir_noticia {
		width:450px;
	}
#contenido_pagina article .articulo_bajada{
	text-align:left;
}	
.redes_sociales_compartir{
	text-align:right;
}
.redes_sociales_compartir ul li img{
	width:8%;
}
}
@media only screen and (min-width: 641px) {
	.compartir_noticia {
		width:610px;
	}
.texto_para_celulares .publicidad_verde_noticia {
	float:right;
	width:300px;
	padding:10px;
	margin-bottom:20px;
	background:#CCC;
	text-align:center;
}

.redes_sociales_compartir ul li img{
	width:6%;
}
.texto_para_celulares div{
padding-right:5px;
}
}
@media only screen and (min-width: 961px) {
	.compartir_noticia {
		width:610px;
	}
.texto_para_celulares .parrafo_separacion {
		display:none;
}
	
.texto_para_celulares .publicidad_verde_noticia {
	float:none;
	padding:0;
	width:300px;
	margin-bottom:20px;
	background:none;
}
.texto_para_celulares .parrafo_dispositivo_movil{
	display:none;
}
	h4 {
	width:50%;
	float:left;
}
.redes_sociales_compartir ul li img{
	width:6%;
}
}
@media only screen and (min-width: 1025px) {
	.compartir_noticia {
		width:672px;
	}

.redes_sociales_compartir ul li img{
	width:5.5%;
}
}

.direccion_diminuta {
	margin-bottom:10px;
	margin-top:-5px;
	text-align:left;
	color:#862338;
	font-size:16px;
	font-weight:bold;
}

.direccion_diminuta selection{
	color:#ec1a0c;
}

@media only screen and (min-width: 480px) {

.direccion_diminuta {
	position:absolute;
	margin-bottom:0;
}
}
	@media only screen and (min-width: 641px) {
	.contenido_adentro {
	width:100%;
	padding-left:0;
	clear:none;
	float:left;
	margin-bottom:20px;
	}
	}
	@media only screen and (min-width: 961px) {
	.contenido_adentro {
		width:66.7%;
		padding-left:0;
	}
	}
		@media only screen and (min-width: 1024px) {
	.contenido_adentro {
	width:68.7%;
	padding-left:0;
		}
	}
	.globo_contenedor {
		color:#333;
		margin-left:3px;
		background: #f8fafc;
		border: 1px solid #bdbdbd;
		border-radius: 4px;
		padding: 6px;
		display: inline-block;
}

.video-container {
    position: relative;
    padding-bottom: 56.25%;
    padding-top: 30px; height: 0; overflow: hidden;
}
 
.video-container iframe,
.video-container object,
.video-container embed {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}
#barra_vista_previa {
	background:#FC0;
	color:#000;
	font-weight:bold;
	width:100$;
	text-align:center;
	padding:10px;
}

body {
	background:#333;
}

.boton_vista {
	display:inline;
	padding:10px;
	background:#F30;
	color:#fff;
}
.boton_vista:hover {
	background:#F60;
	color:#fff;
}
.boton_vista2 {
	display:inline;
	padding:10px;
	background:#606;
	color:#fff;
}
.boton_vista2:hover {
	background:#F60;
	color:#fff;
}
</style>  
</head>
<!-- 
To learn more about the conditional comments around the html tags at the top of the file:
paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/

Do the following if you're using your customized build of modernizr (http://www.modernizr.com/):
* insert the link to your js here
* remove the link below to the html5shiv
* add the "no-js" class to the html tags at the top
* you can also remove the link to respond.min.js if you included the MQ Polyfill in your modernizr build 
-->
<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body>
<div id="barra_vista_previa"><a href="editar-noticia.php?noticia=<?php echo $id_noticia; ?>"><div class="boton_vista2">Editar Noticia</div></a><a href="vista-previa-index.php?noticia=<?php echo $id_noticia; ?>"><div class="boton_vista">Ver Vista previa Portada</div></a> Estás viendo una vista previa de la noticia</div>
 
	<div class="gridContainer  clearfix">
            <?php include('../paginas_include/2-estructura/encabezado.php'); ?>
          <div id="contenido_pagina" class="fluid">
          <article class="fluid">
         <?php if($administrador_nivel == 99) { ?>
         <div class="noticia_antetitulo" ><selection><a href="<?php echo $Servidor_url;?>admin/php/publicar-noticia-db.php?opcion=publicar&noticia=<?php echo $id_noticia; ?>">Publicar Noticia</a></selection></div>
         <?php } ?>
         <div class="noticia_antetitulo"><selection><?php echo $noticia_antetitulo; ?></selection></div>
        <h1><?php echo $noticia_titulo; ?></h1>
        <p class="articulo_autor"><?php echo $noticia_autores; ?></p>
        <p class="articulo_bajada"><?php echo $noticia_bajada; ?></p>
<div class="compartir_noticia">
     
 <div class="direccion_diminuta">Dirección Diminuta: <a class="compartir-noticia-email" href="<?php echo $Servidor_url; ?>popup/direccion-diminuta.php"><img src="<?php echo $Servidor_url;?>imagenes/estructura/redes-sociales/icono-pregunta.png"  alt=""/></a><br />
 <selection><?php echo $direccion_diminuta_sin_http;?></selection></div>
<div class="fluid redes_sociales_compartir">
                <ul class="fluidList">
                <li><a href="javascript:void(0);" onclick="window.open('http://www.facebook.com/sharer.php?u=<?php echo $direccion_redes_sociales; ?>','Compartir', 'toolbar=0, status=0, width=650, height=450');" title="Compartir en Facebook!">
                <img src="<?php echo $Servidor_url;?>imagenes/estructura/redes-sociales/icono-facebook.png"  alt=""/></a></li><li><div class="globo_contenedor"><div id="compartidos_facebook">...</div></div></li>
               <li><a rel="nofollow" href="javascript:void(0);" onclick="window.open('https://twitter.com/intent/tweet?url=<?php echo $direccion_diminuta; ?>&via=RevistaMBP','Compartir', 'toolbar=0, status=0, width=650, height=450');"  title="Compartir en Twitter!"class="twittear" target="_blank">
               <img src="<?php echo $Servidor_url;?>imagenes/estructura/redes-sociales/icono-twitter.png"  alt=""/></a></li><li><div class="globo_contenedor"><div id="compartidos_twitter">...</div></div></li>
               <li><a rel="nofollow" href="javascript:void(0);" onclick="window.open('https://plus.google.com/share?url=<?php echo $direccion_redes_sociales; ?>','Compartir', 'toolbar=0, status=0, width=650, height=450');"  class="mas-uno" target="_blank">
               <img src="<?php echo $Servidor_url;?>imagenes/estructura/redes-sociales/icono-google.png"  alt=""/></a></li>
               <li><a rel="nofollow" class="compartir-noticia-email" href="<?php echo $Servidor_url; ?>popup/compartir-noticia.php?noticia=<?php echo $id_noticia;?>" ><img src="<?php echo $Servidor_url;?>imagenes/estructura/redes-sociales/icono-email.png"  alt=""/></a></li><li><div class="globo_contenedor"><div id="compartidos_mail">...</div></div></li>                           
                </ul>
            	</div>
                <div class="eliminar_flotante"></div>
</div>
<?php if($video_link) { ?>
<div class="video-container">
         <iframe src="http://www.youtube.com/embed/<?php echo $video_link;?>" frameborder="0" width="560" height="315"></iframe>
</div>
<?php }elseif($totalrow_rs_fotos_noticia) { ?>
    <div class="contenedor_slider">      
    <div class="callbacks_container">
      <ul class="rslides" id="slider4">
<?php do { ?>  
        <li>      
<selection class="slider_imagen">
<?php if($row_rs_fotos_noticia['recorte_foto_nombre']) { ?>
<img src="<?php echo $Servidor_url; ?>imagenes/noticias/fotos/recortes/<?php echo $row_rs_fotos_noticia['recorte_foto_nombre'];?>"  alt="<?php echo $noticia_titulo;?>"  width="637" height="399"/>
<?php } else { ?>
 <img src="<?php echo $Servidor_url; ?>00-Librerias/libreria-imagenes/scripts/recortar-imagen.php?tamano=637x399&imagen=<?php echo $row_rs_fotos_noticia['nombre_foto'];?>"  alt="<?php echo $noticia_titulo;?>"  width="637" height="399"/>
<?php } ?>
</selection>
<selection class="slider_imagen_cel">
<?php if($row_rs_fotos_noticia['recorte_foto_miniatura']) { ?>

 <img src="<?php echo $Servidor_url; ?>imagenes/noticias/fotos/recortes/<?php echo $row_rs_fotos_noticia['recorte_foto_miniatura'];?>"  alt="<?php echo $noticia_titulo;?>"/>
 
<?php } else { ?>
 <img src="<?php echo $Servidor_url; ?>00-Librerias/libreria-imagenes/scripts/recortar-imagen.php?tamano=320x320&imagen=<?php echo $row_rs_fotos_noticia['nombre_foto'];?>"  alt="<?php echo $noticia_titulo;?>"  width="320" height="320"/>
<?php } ?>
</selection>
        </li>
<?php } while($row_rs_fotos_noticia = mysql_fetch_assoc($rs_fotos_noticia)) ;?>        
      </ul>
    </div>
    </div>
<?php } //verificar fotos?>       
    <div class="articulo_cuerpo">
<?php echo $noticia_cuerpo; ?>
    </div>
 
        </article>
    
    <!-- empieza texto_para_celulares -->
    <div class="texto_para_celulares">        
    <div id="publicidad_derecha">  
    <div class="mosaic_celular eliminar_flotante"></div>
		<?php echo $texto_celulares; ?>
	</div>
    </div>
    <!-- termina texto_para_celulares -->
    
 
    </div>
    <div class="eliminar_flotante"></div>          
<?php include('../paginas_include/2-estructura/pie.php'); ?>
        </div>
        
	</div>
<br /><br />
    <script>
      var navigation = responsiveNav("#nav", {
        animate: true,        // Boolean: Use CSS3 transitions, true or false
        transition: 400,      // Integer: Speed of the transition, in milliseconds
        label: "",        // String: Label for the navigation toggle
        insert: "before",      // String: Insert the toggle before or after the navigation
        customToggle: "",     // Selector: Specify the ID of a custom toggle
        openPos: "relative",  // String: Position of the opened nav, relative or static
        jsClass: "js",        // String: 'JS enabled' class which is added to <html> el
        init: function(){},   // Function: Init callback
        open: function(){},   // Function: Open callback
        close: function(){}   // Function: Close callback
      });

      $(document).ready(function() {

        $('.compartir-noticia-email').magnificPopup({
          type: 'ajax',
          alignTop: true,
          overflowY: 'scroll'// as we know that popup content is tall we set scroll overflow by default to avoid jump
        });

      });
	  
	  $(window).load(function(){
	
    $.get("<?php echo $Servidor_url;?>paginas_include/3-ajax/compartidos-facebook.php", { q: "<?php echo $direccion_diminuta;?>"},
   function(data){
  $('#compartidos_facebook').html(data);
});
    $.get("<?php echo $Servidor_url;?>paginas_include/3-ajax/compartidos-twitter.php", { q: "<?php echo $direccion_diminuta;?>"},
   function(data){
  $('#compartidos_twitter').html(data);
});

    $.get("<?php echo $Servidor_url;?>paginas_include/3-ajax/compartidos-mail.php", { noticia: "<?php echo $id_noticia;?>"},
   function(data){
  $('#compartidos_mail').html(data);
});
});
    </script>      
</body>
</html>
