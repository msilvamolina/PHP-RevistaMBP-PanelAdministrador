<?php 

include('../paginas_include/variables-generales.php');
$nivel_pagina = 5;
include('php/verificar-permisos.php');

conectar('sitioweb');

$id_foto = trim($_GET['foto']);

$query_rs_elegir_noticia = "SELECT noticias.noticia_titulo, fotos_publicaciones.nombre_foto, fotos_publicaciones.id_foto, fotos_publicaciones.id_publicacion FROM noticias, fotos_publicaciones  WHERE noticias.id_noticia = fotos_publicaciones.id_publicacion ORDER BY fotos_publicaciones.id_foto DESC";
$rs_elegir_noticia = mysql_query($query_rs_elegir_noticia)or die(mysql_error());
$row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia);
$totalrow_rs_elegir_noticia = mysql_num_rows($rs_elegir_noticia);

if($id_foto) {
$query_rs_noticia = "SELECT recorte_foto_miniatura, nombre_foto, recorte_foto_x, recorte_foto_y, recorte_foto_w, recorte_foto_h  FROM fotos_publicaciones WHERE id_foto = $id_foto";
$rs_noticia = mysql_query($query_rs_noticia)or die(mysql_error());
$row_rs_noticia = mysql_fetch_assoc($rs_noticia);
$totalrow_rs_noticia = mysql_num_rows($rs_noticia);

$recorte_anterior = $row_rs_noticia['recorte_foto_miniatura'];

desconectar();

$foto_seleccionada = $row_rs_noticia['nombre_foto'];
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link rel="shortcut icon" href="<?php echo $Servidor_url; ?>favicon.ico">
<script src="<?php echo $Servidor_url; ?>00-Javascripts/autocompletar/jquery-1.3.2.min.js" type="text/javascript"></script>

<link href="css/admin.css" rel="stylesheet" type="text/css" />
<style type="text/css">

	#publicacion_fotos{
		width:620px;
		padding-right:20px;
		padding-top:10px;
		overflow-y:auto;
		border:1px solid #F60;
		-webkit-border-radius:6px;
		-moz-border-radius:6px;
		background:#F93;
	}
	 a {
		 color:#000;
	 }
	#upload{  
		padding:7px;
		margin:0 auto;
        text-align:center;  
		background-color:#F60;
		font-weight:bold;
        color:#000;  
        border:1px solid #000;  
		display:block;  
		text-decoration:none;
		width:80px;
		-webkit-border-radius:4px;
		-moz-border-radius:4px;
    }
		#upload a{
			color:#000;
    }
	
	#gallery{
		list-style:none;
		text-align:center;
		padding:0;
		margin-top:10px;
	}
	#gallery li{	
		display:block;
		float:left;
		width:100px;
		height:100px;
		background:#fff;
		border:1px solid #999;
		text-align:center;
		position:relative;
		padding:10px;
		margin-left:8px;
		margin-bottom:14px;
		-webkit-border-radius:4px;
		-moz-border-radius:4px;
	}
	#gallery li img{
		width:100px;
		height:100px
	}
	#gallery li a{
		position:absolute;
		right:10px;
		top:10px
	}
	#gallery li a img{ width:auto; height:auto}
	
	#crear_publicacion_descripcion {
		width:650px;
		height:400px;
	}
	.noticia_titulo {
		font-size:36px;
		padding:10px;
		width:96%;
		min-height:100px;
	}
	.noticia_bajada {
		font-size:18px;
		min-height:100px;
		padding:10px;
		width:96%;
	}
	.noticia_destacado {
		font-size:18px;
		min-height:100px;
		color:#d3222c;
		padding:10px;
		width:96%;
	}	
	#contenido_principal input[type='text'] {
		margin-top:0;
		margin-left:0;
		width:96%;
	}

	.elegir_noticia {
		margin-top:10px;
		padding:15px;
		background:#6CF;
		border:1px solid #66F;
	}
	.elegir_noticia h3{
		margin-top:0;
	}
	.elegir_noticia select{
		width:100%;
	}

/* Apply these styles only when #preview-pane has
   been placed within the Jcrop widget */
.jcrop-holder #preview-pane {
  display: block;
  position: absolute;
  z-index: 2000;
  top: 10px;
  right: -280px;
  padding: 6px;
  border: 1px rgba(0,0,0,.4) solid;
  background-color: white;

  -webkit-border-radius: 6px;
  -moz-border-radius: 6px;
  border-radius: 6px;

  -webkit-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
}

/* The Javascript code will set the aspect ratio of the crop
   area based on the size of the thumbnail preview,
   specified here */
#preview-pane .preview-container {
  width: 320px;
  height: 320px;
  overflow: hidden;
}	
</style>
  <script src="jcrop/js/jquery.min.js"></script>
  <script src="jcrop/js/jquery.Jcrop.js"></script>
  <link rel="stylesheet" href="jcrop/demo_files/main.css" type="text/css" />
  <link rel="stylesheet" href="jcrop/demo_files/demos.css" type="text/css" />
  <link rel="stylesheet" href="jcrop/css/jquery.Jcrop.css" type="text/css" />
  <script type="text/javascript">
jQuery(function($){

    // Create variables (in this scope) to hold the API and image size
    var jcrop_api,
        boundx,
        boundy,

        // Grab some information about the preview pane
        $preview = $('#preview-pane'),
        $pcnt = $('#preview-pane .preview-container'),
        $pimg = $('#preview-pane .preview-container img'),

        xsize = $pcnt.width(),
        ysize = $pcnt.height();
    
    console.log('init',[xsize,ysize]);
    $('#target').Jcrop({
      onChange: updatePreview,
      onSelect: updateCoords,
	  minSize: [320,320],
	  setSelect:   [ 0,0,320,320],
      aspectRatio: 1
    },function(){
      // Use the API to get the real image size
      var bounds = this.getBounds();
      boundx = bounds[0];
      boundy = bounds[1];
      // Store the API in the jcrop_api variable
      jcrop_api = this;

      // Move the preview into the jcrop container for css positioning
      $preview.appendTo(jcrop_api.ui.holder);
    });
 function updateCoords(c)
  {
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#w').val(c.w);
    $('#h').val(c.h);
  };

    function updatePreview(c)
    {
      if (parseInt(c.w) > 0)
      {
        var rx = xsize / c.w;
        var ry = ysize / c.h;

        $pimg.css({
          width: Math.round(rx * boundx) + 'px',
          height: Math.round(ry * boundy) + 'px',
          marginLeft: '-' + Math.round(rx * c.x) + 'px',
          marginTop: '-' + Math.round(ry * c.y) + 'px'
        });
      }
    };

  });

</script>
</head>

<body>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
  <h1>Recortar Miniatura</h1>
  <div class="elegir_noticia">
  <h3>Foto:</h3>
  <form action="<?php echo $_SERVER['PHP_SELF']; ?>" name="form_elegir">
  <select name="foto" onchange="document.forms.form_elegir.submit()">
  <option value="0">Elegir una Foto:</option>
  <?php do { 
	if($id_foto == $row_rs_elegir_noticia['id_foto']) {
		$seleccionar = 'selected="selected"';
	}else{
		$seleccionar = '';
	}
	
  ?>

    <option <?php echo $seleccionar; ?> value="<?php echo $row_rs_elegir_noticia['id_foto']; ?>"><?php echo $row_rs_elegir_noticia['nombre_foto']; ?> - <?php echo $row_rs_elegir_noticia['noticia_titulo'];?></option>
  <?php } while($row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia)) ?>

  </select>
  </form>  
  </div><br />
  <?php if($totalrow_rs_noticia) { ?>

<?php if(($recorte_anterior) && (!$_GET['reemplazar'])) { ?>
<p>Advertencia: Esta Imagen ya fue recortada anteriormente, si continúa reemplazará este recorte:</p>
<br />
  <img src="http://www.revistambp.com/imagenes/noticias/fotos/recortes/<?php echo $recorte_anterior; ?>" />
  <br /><br /><br />
    <p><center><a id="btn_cargar_noticia" href="<?php echo $_SERVER['PHP_SELF']; ?>?foto=<?php echo $id_foto; ?>&reemplazar=1">Nuevo Recorte</a></p>

<?php }else{ ?>  
<div id="preview-pane">
    <div class="preview-container">
      <img src="http://www.revistambp.com/imagenes/noticias/fotos/<?php echo $foto_seleccionada; ?>" class="jcrop-preview" alt="Preview" />
    </div>
   </div>
 <img src="http://www.revistambp.com/imagenes/noticias/fotos/<?php echo $foto_seleccionada; ?>"id="target"  />

   
  <form action="php/recortar-miniatura-db.php" method="post" onsubmit="return checkCoords();">
  			<input type="hidden" name="id_foto" value="<?php echo $id_foto; ?>"/>
			<input type="hidden" id="x" name="x" />
			<input type="hidden" id="y" name="y" />
			<input type="hidden" id="w" name="w" />
			<input type="hidden" id="h" name="h" />
  <p><center><input type="submit" value="Recortar Imagen" id="btn_cargar_noticia" name="btn_cargar_noticia" /></center></p>

		</form>
  <?php } ?>
  <?php } ?>
    <div class="eliminar_flotante"></div>
  </div>

  <!-- end .content --></div>
<p>&nbsp;</p>
  <!-- end .container --></div>
</body>
</html>
