<?php 

include('../paginas_include/variables-generales.php');
$nivel_pagina = 0;
include('php/verificar-permisos.php');

$operacion = trim($_GET['operacion']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link rel="shortcut icon" href="<?php echo $Servidor_url; ?>favicon.ico">
<script src="<?php echo $Servidor_url; ?>00-Javascripts/autocompletar/jquery-1.3.2.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $Servidor_url; ?>00-Javascripts/ajax.js"></script>
<script  type="text/javascript" src="<?php echo $Servidor_url; ?>00-Javascripts/jquery.tipsy.js"></script>
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<?php include_once('../00-Javascripts/nicEditor/nicEdit.php'); ?>
<style type="text/css">

	#publicacion_fotos{
		width:620px;
		padding-right:20px;
		padding-top:10px;
		overflow-y:auto;
		border:1px solid #F60;
		-webkit-border-radius:6px;
		-moz-border-radius:6px;
		background:#F93;
	}
	 a {
		 color:#000;
	 }
	#upload{  
		padding:7px;
		margin:0 auto;
        text-align:center;  
		background-color:#F60;
		font-weight:bold;
        color:#000;  
        border:1px solid #000;  
		display:block;  
		text-decoration:none;
		width:80px;
		-webkit-border-radius:4px;
		-moz-border-radius:4px;
    }
		#upload a{
			color:#000;
    }
	
	#gallery{
		list-style:none;
		text-align:center;
		padding:0;
		margin-top:10px;
	}
	#gallery li{	
		display:block;
		float:left;
		width:100px;
		height:100px;
		background:#fff;
		border:1px solid #999;
		text-align:center;
		position:relative;
		padding:10px;
		margin-left:8px;
		margin-bottom:14px;
		-webkit-border-radius:4px;
		-moz-border-radius:4px;
	}
	#gallery li img{
		width:100px;
		height:100px
	}
	#gallery li a{
		position:absolute;
		right:10px;
		top:10px
	}
	#gallery li a img{ width:auto; height:auto}
	
	#crear_publicacion_descripcion {
		width:650px;
		height:400px;
	}
	.noticia_titulo {
		font-size:36px;
		padding:10px;
		width:96%;
		min-height:100px;
	}
	.noticia_bajada {
		font-size:18px;
		min-height:100px;
		padding:10px;
		width:96%;
	}
	.noticia_destacado {
		font-size:18px;
		min-height:100px;
		color:#d3222c;
		padding:10px;
		width:96%;
	}	
	#contenido_principal input[type='text'] {
		margin-top:0;
		margin-left:0;
		width:96%;
	}
	h3 {
		margin-top:25px;
		margin-bottom:10px;
	}
	.elegir_noticia {
		margin-top:10px;
		padding:15px;
		background:#6CF;
		border:1px solid #66F;
	}
	.elegir_noticia h3{
		margin-top:0;
	}
	.elegir_noticia select{
		width:100%;

	}
.noticia_editada {
	margin-top:20px;
	background:#d5fcd2;
	border:1px solid #6C6;
	padding:15px;
	font-size:18px;
}
.boton_atras a{
	background:#F63;
	color:#fff;
	padding:10px;
	border:1px solid #600;
	text-decoration:none;
	margin-bottom:20px;
}

.boton_atras a:hover{
	background:#F30;
}

.vista_previa a{
	background:#60C;
	color:#fff;
	padding:10px;
	border:1px solid #600;
	text-decoration:none;
	margin-top:20px;
}

.vista_previa a:hover{
	background:#66F;
}
</style>
<script type="text/javascript" src="<?php echo $Servidor_url; ?>00-Javascripts/ajaxupload.js"></script>
</head>

<body>
<?php if($operacion == 'cambiar-contrasena') { ?>
<script LANGUAGE="JavaScript">
var pagina="<?php echo $Servidor_url; ?>admin/php/logout.php"
function redireccionar() {
	location.href=pagina
} 
setTimeout ("redireccionar()", 5000);
</script>
<?php } ?>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
  <h1>¡Operación Exitosa!</h1>
  <div class="noticia_editada">
  <?php 
  switch ($operacion) {
	  case 'editar-noticia':
	  	echo 'La noticia se actualizó correctamente';
		$volver_atras = 'editar-noticia.php';
	  break;
	  case 'editar-html':
	  	echo 'el código HTML se actualizó correctamente';
		$volver_atras = 'editar-HTML.php';
	  break;	  
	  case 'administrar-fotos':
	  	echo 'Las fotos se actualizaron correctamente';
		$volver_atras = 'administrar-fotos.php';		
	  break;
	  case 'cargar-noticia':
	  	echo 'La noticia se cargó correctamente';
		$volver_atras = 'cargar-noticia.php';		
	  break;
	  case 'crear-autor':
	  	echo 'El autor se creó correctamente';
		$volver_atras = 'crear-autor.php';		
	  break;
	  case 'crear-usuario':
	  	echo 'El usuario se creó correctamente';
		$volver_atras = 'crear-usuario.php';		
	  break;
	  case 'crear-email':
	  	echo 'El E-mail se creó correctamente';
		$volver_atras = 'crear-email.php';		
	  break;	  
	  case 'cambiar-contrasena':
	  	echo 'Tu contraseña se cambió correctamente.<br />Tu sesión se cerrará automáticamente en 5 segundos';
	  break;
	  case 'identificar-ip':
	  	echo 'La dirección IP se identificó correctamente';
		$volver_atras = 'identificar-ip.php';		
	  break;	 	  	  
	  case 'armar-portada':
	  	echo 'La portada se guardó correctamente';
		$volver_atras = 'armar-portada.php';		
	  break;
	  case 'armar-empresas':
	  	echo 'La sección empresas se guardó correctamente';
		$volver_atras = 'armar-empresas.php';		
	  break;	  
	  case 'formato-noticia':
	  	echo 'El formato se guardó correctamente';
		$volver_atras = 'formato-noticia.php';		
	  break;		  
	  case 'formato-eliminar':
	  	echo 'El formato se eliminó correctamente';
		$volver_atras = 'formato-noticia.php';		
	  break;	
	  case 'borrar-noticia':
	  	echo 'La noticia se borró correctamente';
		$volver_atras = 'borrar-noticia.php';		
	  break;	
	  case 'cargar-encuesta':
	  	echo 'La encuesta se cargó correctamente';
		$volver_atras = 'cargar-encuesta.php';		
	  break;
	  case 'armar-encuestas':
	  	echo 'La sección encuestas de opinión se guardó correctamente';
		$volver_atras = 'armar-encuestas.php';		
	  break;
	  case 'diminuta':
	  	echo 'La dirección diminuta se creó correctamente';
		$volver_atras = 'cargar-direccion-diminuta.php';		
	  break;	  
	  case 'cargar-video':
	  	echo 'El video se cargó correctamente';
		$volver_atras = 'cargar-video.php';		
	  break;
	  case 'borrar-encuesta':
	  	echo 'La encuesta se borró correctamente';
		$volver_atras = 'borrar-encuesta.php';		
	  break;		   	  	  		  	  	  	  		  
	  case 'editar-encuesta':
	  	echo 'La encuesta se editó correctamente';
		$volver_atras = 'editar-encuesta.php';		
	  break;
	  case 'recortar-foto':
	  	echo 'La foto se recortó correctamente';
		$volver_atras = 'recortar-foto.php';		
	  break;		
	  case 'subir-video-noticia':
	  	echo 'El video se añadió correctamente a la noticia';
		$volver_atras = 'video-noticia.php';		
	  break;
	  case 'cargar-revista':
	  	echo 'La revista se cargó correctamente';
		$volver_atras = 'cargar-revista.php';		
	  break;	 
	  case 'lista-reproduccion':
	  	echo 'La lista de reproducción se cargó correctamente';
		$volver_atras = 'lista-reproduccion.php';		
	  break;	   	  	  	  
 	  case 'armar-actualizacion':
	  	echo 'La Actualización Automática se cargó correctamente';
		$volver_atras = 'actualizacion-automatica.php';		
	  break;	   	
  }
  ?>
  </div>
  <br /><br />
  <?php if($_GET['noticia']) { ?>
  <center><div class="vista_previa"><a target="_blank" href="<?php echo $Servidor_url;?>admin/vista-previa.php?noticia=<?php echo $_GET['noticia']; ?>">Vista Previa</a></div></center>
  <br />  <br />  <br />
  <?php } ?>
  <center><div class="boton_atras"><a href="<?php echo $Servidor_url;?>admin/<?php echo $volver_atras;?>">Volver Atrás</a></div></center>

    <div class="eliminar_flotante"></div>
  </div>

  <!-- end .content --></div>
<p>&nbsp;</p>
  <!-- end .container --></div>
</body>
</html>
