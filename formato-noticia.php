<?php 

include('../paginas_include/variables-generales.php');
$nivel_pagina = 99;
include('php/verificar-permisos.php');

conectar('sitioweb');

$id_noticia = trim($_GET['noticia']);

$query_rs_elegir_noticia = "SELECT id_noticia, noticia_titulo, fecha_carga, fecha_modificacion  FROM noticias ORDER BY id_noticia DESC";
$rs_elegir_noticia = mysql_query($query_rs_elegir_noticia)or die(mysql_error());
$row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia);
$totalrow_rs_elegir_noticia = mysql_num_rows($rs_elegir_noticia);

if($id_noticia) {

$query_rs_verificar_formato = "SELECT id_formato, formato_antetitulo_fondo, formato_antetitulo_color, formato_titulo_color, formato_autor_color, formato_bajada_color, formato_h2_color, formato_h4_color, formato_h4_fondo, formato_letra_capital_color FROM formato_noticia WHERE id_noticia = $id_noticia";
$rs_verificar_formato = mysql_query($query_rs_verificar_formato)or die(mysql_error());
$row_rs_verificar_formato = mysql_fetch_assoc($rs_verificar_formato);
$totalrow_rs_verificar_formato = mysql_num_rows($rs_verificar_formato);

$id_formato = $row_rs_verificar_formato['id_formato'];

if(!$totalrow_rs_verificar_formato) {
	$query_rs_verificar_formato = "SELECT formato_antetitulo_fondo, formato_antetitulo_color, formato_titulo_color, formato_autor_color, formato_bajada_color, formato_h2_color, formato_h4_color, formato_h4_fondo, formato_letra_capital_color FROM formato_noticia WHERE id_noticia = 0";
$rs_verificar_formato = mysql_query($query_rs_verificar_formato)or die(mysql_error());
$row_rs_verificar_formato = mysql_fetch_assoc($rs_verificar_formato);
$totalrow_rs_verificar_formato = mysql_num_rows($rs_verificar_formato);
}
//Antetitulo
$formato_antetitulo_fondo = $row_rs_verificar_formato['formato_antetitulo_fondo'];
$formato_antetitulo_color = $row_rs_verificar_formato['formato_antetitulo_color'];

$formato_titulo_color = $row_rs_verificar_formato['formato_titulo_color'];
$formato_autor_color = $row_rs_verificar_formato['formato_autor_color'];

$formato_bajada_color = $row_rs_verificar_formato['formato_bajada_color'];
$formato_h2_color = $row_rs_verificar_formato['formato_h2_color'];

$formato_h4_color = $row_rs_verificar_formato['formato_h4_color'];
$formato_h4_fondo = $row_rs_verificar_formato['formato_h4_fondo'];

$formato_letra_capital_color = $row_rs_verificar_formato['formato_letra_capital_color'];

}

desconectar();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link rel="shortcut icon" href="<?php echo $Servidor_url; ?>favicon.ico">
<!-- jQuery -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	
	<!-- MiniColors -->
	<script src="js/color-picker/jquery.minicolors.js"></script>
	<link rel="stylesheet" href="js/color-picker/jquery.minicolors.css">
    
<link href="css/admin.css" rel="stylesheet" type="text/css" />
	<style>

		#contenido_principal input[type='text'] {
	height:40px;
	margin:0;
	padding:0;
	padding-left:40px;
	width:300px;
}
.grupo_selector_color {
	padding-left:20px;
	margin-bottom:10px;
}
.eliminar_formato {
	text-align:right;
}

.eliminar_formato a {
	text-decoration:none;
	color:#2d85f2;
}

.eliminar_formato a:hover {
	text-decoration:underline;
	color:#f28e2d;
}
	</style>
<script>
		$(document).ready( function() {
			
            $('.demo').each( function() {
                //
                // Dear reader, it's actually very easy to initialize MiniColors. For example:
                //
                //  $(selector).minicolors();
                //
                // The way I've done it below is just for the demo, so don't get confused 
                // by it. Also, data- attributes aren't supported at this time...they're 
                // only used for this demo.
                //
				$(this).minicolors({
					control: $(this).attr('data-control') || 'hue',
					defaultValue: $(this).attr('data-defaultValue') || '',
					inline: $(this).attr('data-inline') === 'true',
					letterCase: $(this).attr('data-letterCase') || 'lowercase',
					opacity: $(this).attr('data-opacity'),
					position: $(this).attr('data-position') || 'bottom left',
					change: function(hex, opacity) {
						var log;
						try {
							log = hex ? hex : 'transparent';
							if( opacity ) log += ', ' + opacity;
							console.log(log);
						} catch(e) {}
					},
					theme: 'bootstrap'
				});
                
            });
			
		});
	</script>

</head>

<body>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
  <h1>Formato Noticia</h1>
  <div class="elegir_noticia">
  <h3>Noticia:</h3>
  <form action="<?php echo $_SERVER['PHP_SELF']; ?>" name="form_elegir">
  <select name="noticia" onchange="document.forms.form_elegir.submit()">
  <option value="0">Elegir una Noticia:</option>
  <?php do { 
	if($id_noticia == $row_rs_elegir_noticia['id_noticia']) {
		$seleccionar = 'selected="selected"';
	}else{
		$seleccionar = '';
	}
	if($row_rs_elegir_noticia['fecha_modificacion']) {
		$cuantohace = 'modificada '.cuantoHace($row_rs_elegir_noticia['fecha_modificacion']);
	}else{
		$cuantohace = 'cargada '.cuantoHace($row_rs_elegir_noticia['fecha_carga']);
	}
  ?>

    <option <?php echo $seleccionar; ?> value="<?php echo $row_rs_elegir_noticia['id_noticia']; ?>"><?php echo $row_rs_elegir_noticia['noticia_titulo']; ?> - <?php echo $cuantohace;?></option>
  <?php } while($row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia)) ?>

  </select>
  </form>  
  </div>
  <?php if($id_noticia) { ?>
  <?php if($id_formato) { ?>
  <br />
<div class="eliminar_formato"><a href="<?php echo $Servidor_url;?>admin/php/formato-noticia-eliminar.php?formato=<?php echo $id_formato; ?>&noticia=<?php echo $id_noticia; ?>" onclick="return confirm('¿Estás segudo de que querés eliminar el formato?')">Eliminar Formato de la Noticia</a></div>
<?php } ?>
  <form action="php/formato-noticia-db.php" id="formulario_mbp" name="formulario_mbp" onsubmit="return controlar_formulario()" method="post">
    <input type="hidden" name="form_id_noticia" value="<?php echo $id_noticia;?>"/>
  <h3>Antetítulo:</h3>
	<div class="grupo_selector_color">
		<label for="hue-demo">Color de Letra:</label>
		<input type="text" name="formato_antetitulo_color"id="hue-demo" class="demo" data-control="hue" value="<?php echo $formato_antetitulo_color; ?>">
	</div>
	<div class="grupo_selector_color">
		<label for="hue-demo">Color de Fondo:</label>
		<input type="text" name="formato_antetitulo_fondo"id="hue-demo" class="demo" data-control="hue" value="<?php echo $formato_antetitulo_fondo; ?>">
	</div>    
 <h3>Título:</h3>
	<div class="grupo_selector_color">
		<label for="hue-demo">Color de Letra:</label>
		<input type="text" name="formato_titulo_color"id="hue-demo" class="demo" data-control="hue" value="<?php echo $formato_titulo_color; ?>">
	</div>      
 <h3>Autor:</h3>
	<div class="grupo_selector_color">
		<label for="hue-demo">Color de Letra:</label>
		<input type="text" name="formato_autor_color"id="hue-demo" class="demo" data-control="hue" value="<?php echo $formato_autor_color; ?>">
	</div>
 <h3>Bajada:</h3>
	<div class="grupo_selector_color">
		<label for="hue-demo">Color de Letra:</label>
		<input type="text" name="formato_bajada_color"id="hue-demo" class="demo" data-control="hue" value="<?php echo $formato_bajada_color; ?>">
	</div>
 <h3>Subtítulo Header 2:</h3>
	<div class="grupo_selector_color">
		<label for="hue-demo">Color de Letra:</label>
		<input type="text" name="formato_h2_color"id="hue-demo" class="demo" data-control="hue" value="<?php echo $formato_h2_color; ?>">
	</div>
 <h3>Destacado Header 4:</h3>
	<div class="grupo_selector_color">
		<label for="hue-demo">Color de Letra:</label>
		<input type="text" name="formato_h4_color"id="hue-demo" class="demo" data-control="hue" value="<?php echo $formato_h4_color; ?>">
	</div>
	<div class="grupo_selector_color">
		<label for="hue-demo">Color de Fondo:</label>
		<input type="text" name="formato_h4_fondo"id="hue-demo" class="demo" data-control="hue" value="<?php echo $formato_h4_fondo; ?>">
	</div>    
 <h3>Cuerpo Letra Capital:</h3>
	<div class="grupo_selector_color">
		<label for="hue-demo">Color de Letra:</label>
		<input type="text" name="formato_letra_capital_color"id="hue-demo" class="demo" data-control="hue" value="<?php echo $formato_letra_capital_color; ?>">
	</div>
    <br /><br />                          
  <p><center><input type="submit" value="Guardar Formato" id="btn_cargar_noticia" name="btn_cargar_noticia" /></center></p>
  </form>
</div>
  <?php } ?>
    <div class="eliminar_flotante"></div>
  </div>

  <!-- end .content --></div>
<p>&nbsp;</p>
  <!-- end .container --></div>
</body>
</html>
