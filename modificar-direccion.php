<?php 

include('../paginas_include/variables-generales.php');
$nivel_pagina = 99;
include('php/verificar-permisos.php');

conectar('sitioweb');

$id_noticia = trim($_GET['noticia']);

$query_rs_elegir_noticia = "SELECT id_noticia, noticia_titulo, fecha_carga, fecha_modificacion  FROM noticias ORDER BY id_noticia DESC";
$rs_elegir_noticia = mysql_query($query_rs_elegir_noticia)or die(mysql_error());
$row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia);
$totalrow_rs_elegir_noticia = mysql_num_rows($rs_elegir_noticia);

if($id_noticia) {
$query_rs_noticia = "SELECT id_noticia, noticia_titulo FROM noticias WHERE id_noticia = $id_noticia";
$rs_noticia = mysql_query($query_rs_noticia)or die(mysql_error());
$row_rs_noticia = mysql_fetch_assoc($rs_noticia);
$totalrow_rs_noticia = mysql_num_rows($rs_noticia);

desconectar();

$noticia_titulo = $row_rs_noticia['noticia_titulo'];

$noticia_cuerpo = str_replace('.', '.<br>', $noticia_cuerpo);

}

$tipo_archivos[] = 'Noticia';
$tipo_archivos[] = 'Encuesta';
$tipo_archivos[] = 'Video';
$tipo_archivos[] = 'Otro';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link rel="shortcut icon" href="<?php echo $Servidor_url; ?>favicon.ico">
<script src="<?php echo $Servidor_url; ?>00-Javascripts/autocompletar/jquery-1.3.2.min.js" type="text/javascript"></script>

<link href="css/admin.css" rel="stylesheet" type="text/css" />
<style type="text/css">

	#publicacion_fotos{
		width:620px;
		padding-right:20px;
		padding-top:10px;
		overflow-y:auto;
		border:1px solid #F60;
		-webkit-border-radius:6px;
		-moz-border-radius:6px;
		background:#F93;
	}
	 a {
		 color:#000;
	 }
	#upload{  
		padding:7px;
		margin:0 auto;
        text-align:center;  
		background-color:#F60;
		font-weight:bold;
        color:#000;  
        border:1px solid #000;  
		display:block;  
		text-decoration:none;
		width:80px;
		-webkit-border-radius:4px;
		-moz-border-radius:4px;
    }
		#upload a{
			color:#000;
    }
	
	#gallery{
		list-style:none;
		text-align:center;
		padding:0;
		margin-top:10px;
	}
	#gallery li{	
		display:block;
		float:left;
		width:100px;
		height:100px;
		background:#fff;
		border:1px solid #999;
		text-align:center;
		position:relative;
		padding:10px;
		margin-left:8px;
		margin-bottom:14px;
		-webkit-border-radius:4px;
		-moz-border-radius:4px;
	}
	#gallery li img{
		width:100px;
		height:100px
	}
	#gallery li a{
		position:absolute;
		right:10px;
		top:10px
	}
	#gallery li a img{ width:auto; height:auto}
	
	#crear_publicacion_descripcion {
		width:650px;
		height:400px;
	}
	.noticia_titulo {
		font-size:36px;
		padding:10px;
		width:96%;
		min-height:100px;
	}
	.noticia_bajada {
		font-size:18px;
		min-height:100px;
		padding:10px;
		width:96%;
	}
	.noticia_destacado {
		font-size:18px;
		min-height:100px;
		color:#d3222c;
		padding:10px;
		width:96%;
	}	
	#contenido_principal input[type='text'] {
		margin-top:0;
		margin-left:0;
		width:96%;
	}

	.elegir_noticia {
		margin-top:10px;
		padding:15px;
		background:#6CF;
		border:1px solid #66F;
	}
	.elegir_noticia h3{
		margin-top:0;
	}
	.elegir_noticia select{
		width:100%;

	}
</style>
  	  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $Servidor_url;?>00-Javascripts/ajax.js"></script> 

	<script type="text/javascript" src="00-appendo/jquery.pack.js"></script>
	<script type="text/javascript" src="00-appendo/jquery.appendo.js"></script>
<script type="text/javascript">
function verificar_direccion(direccion_ingresada){
	//donde se mostrará el resultado de la eliminacion
	chequear_direccion=document.formulario_mbp.chequear_direccion;	
	divResultado = document.getElementById('verificar_diminuta');
		//instanciamos el objetoAjax
		ajax=nuevoAjax();
		//uso del medotod GET
		//indicamos el archivo que realizará el proceso de eliminación
		//junto con un valor que representa el id del empleado
		divResultado.innerHTML = '<div class="registro_cargando">Comprobando...</div>';
		ajax.open("GET", "<?php echo $Servidor_url;?>paginas_include/3-ajax/ajax-direccion-diminuta.php?direccion="+direccion_ingresada);
		ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText;
				chequear_direccion.value = ajax.responseText;				
			}
		}
		//como hacemos uso del metodo GET
		//colocamos null
		ajax.send(null)
}
</script>    
</head>

<body>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
  <h1>Modificar Dirección</h1>
  <div class="elegir_noticia">
  <h3>Tipo:</h3>
  <form action="<?php echo $_SERVER['PHP_SELF']; ?>" name="form_elegir_tipo">
  <select name="tipo" onchange="document.forms.form_elegir_tipo.submit()">
  <option value="0">Elegir un tipo:</option>
  <?php foreach ($tipo_archivos as $valor) { 
	if($valor == $_GET['tipo']) {
		$seleccionar = 'selected="selected"';
	}else{
		$seleccionar = '';
	}
  ?>

    <option <?php echo $seleccionar; ?> value="<?php echo $valor; ?>"><?php echo $valor; ?></option>
  <?php } ?>

  </select>
  </form>  
  </div>
<?php if($_GET['tipo'] == 'Noticia') { ?> 
  <div class="elegir_noticia">
  <h3>Noticia:</h3>
  <form action="<?php echo $_SERVER['PHP_SELF']; ?>" name="form_elegir">
  <input  type="hidden" name="tipo" value="<?php echo $_GET['tipo']; ?>" />
  <select name="noticia" onchange="document.forms.form_elegir.submit()">
  <option value="0">Elegir una Noticia:</option>
  <?php do { 
	if($id_noticia == $row_rs_elegir_noticia['id_noticia']) {
		$seleccionar = 'selected="selected"';
	}else{
		$seleccionar = '';
	}
	if($row_rs_elegir_noticia['fecha_modificacion']) {
		$cuantohace = 'modificada '.cuantoHace($row_rs_elegir_noticia['fecha_modificacion']);
	}else{
		$cuantohace = 'cargada '.cuantoHace($row_rs_elegir_noticia['fecha_carga']);
	}
  ?>

    <option <?php echo $seleccionar; ?> value="<?php echo $row_rs_elegir_noticia['id_noticia']; ?>"><?php echo $row_rs_elegir_noticia['noticia_titulo']; ?> - <?php echo $cuantohace;?></option>
  <?php } while($row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia)) ?>

  </select>
  </form>  
  </div>
  <?php } ?>
  <?php if($totalrow_rs_noticia) { ?>
  <form action="php/modificar-direccion-db.php" id="formulario_mbp" name="formulario_mbp" onsubmit="return controlar_formulario()" method="post">
    <input type="hidden" name="form_id_noticia" value="<?php echo $id_noticia;?>"/>
<h3>Dirección Absoluta:</h3>  
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>http://www.revistambp.com/<input type="text" name="direccion_diminuta" onkeyup="verificar_direccion(this.value)" placeholder="Dirección" style="color:#999;width:250px" maxlength="20" class="crear_direccion" value="<?php echo $noticia_titulo_corto;?>" required="required"/><input type="hidden" name="chequear_direccion" id="chequear_direccion"  /></td>
    <td><div id="verificar_diminuta"></div></td>
  </tr>
</table>
 <h3>Dirección Diminuta:</h3>  
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>mbp.pe/<input type="text" name="direccion_diminuta" onkeyup="verificar_direccion(this.value)" placeholder="Dirección" style="color:#999;width:250px" maxlength="20" class="crear_direccion" value="<?php echo $noticia_titulo_corto;?>" required="required"/><input type="hidden" name="chequear_direccion" id="chequear_direccion"  /></td>
    <td><div id="verificar_diminuta"></div></td>
  </tr>
</table>
    <h3>Conservar Direcciónes anteriores</h3>
    <p><input type="checkbox" /> Conservar Dirección <i>Absoluta</i> Anterior</p><br />
    <p><input type="checkbox" /> Conservar Dirección <i>Diminuta</i> Anterior</p>
  <p><center><input type="submit" value="Modificar Dirección" id="btn_cargar_noticia" name="btn_cargar_noticia" /></center></p>
  </form>
</div>
  <?php } ?>
    <div class="eliminar_flotante"></div>
  </div>

  <!-- end .content --></div>
<p>&nbsp;</p>
  <!-- end .container --></div>
</body>
</html>
