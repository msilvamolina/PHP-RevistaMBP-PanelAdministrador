<?php include('../paginas_include/variables-generales.php');?>
<?php
$nivel_pagina = 99;
include('php/verificar-permisos.php');

conectar('sitioweb');

$id_noticia = trim($_GET['noticia']);

$opcion = trim($_GET['opcion']);

if($opcion == 'nopublicadas') {

		$query_rs_elegir_noticia = "SELECT id_noticia, noticia_titulo FROM noticias WHERE noticia_publicada IS NULL OR noticia_publicada = 0 ORDER BY id_noticia DESC";
$rs_elegir_noticia = mysql_query($query_rs_elegir_noticia)or die(mysql_error());
$row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia);
$totalrow_rs_elegir_noticia = mysql_num_rows($rs_elegir_noticia);

$titulo = 'Noticias No Publicadas';
$boton_publicar = 'Publicar';
$boton_publicar_class = 'publicar';
$boton_vista_previa = 'Vista Previa';
$boton_vista_previa_link= 'admin/vista-previa.php';
} else {
	$query_rs_elegir_noticia = "SELECT id_noticia, noticia_titulo FROM noticias WHERE noticia_publicada = 1 ORDER BY id_noticia DESC";
$rs_elegir_noticia = mysql_query($query_rs_elegir_noticia)or die(mysql_error());
$row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia);
$totalrow_rs_elegir_noticia = mysql_num_rows($rs_elegir_noticia);

$titulo = 'Noticias Publicadas';
$boton_publicar = 'Quitar Publicación';
$boton_publicar_class = 'nopublicar';
$boton_vista_previa = 'Ver Noticia';
$boton_vista_previa_link= 'noticia.php';
}

	
desconectar();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../00-Javascripts/jquery.js"></script>
<script type="text/javascript" src="../00-Javascripts/ajax.js"></script>
<style>
.publicar_opciones {
	padding-top:20px;
	margin-bottom:40px;
}
.publicar_opciones a{
	margin-right:30px;
	background:#39C;
	padding:10px;
	color:#fff;
	text-decoration:none;
}
.publicar_opciones a:hover{
	background:#63C;
}

.boton_publicar {
	padding:8px;
	text-align:center;
	background:#3C0;
	color:#000;
}
.boton_publicar:hover {
	color:#fff;
	background:#6C3;
}

.vista_previa {
	padding:8px;
	text-align:center;
	background:#69F;
	color:#fff;
}
.vista_previa:hover {
	color:#fff;
	background:#636;
}

.tabla_celda_1 {
	padding:10px;
	background:#CCC;
}
.tabla_celda_2 {
	padding:10px;
	background:#FFC;
}

a {		text-decoration:none;
}

.tabla_publicadas tr td{
	height:40px;
}

.tabla_publicadas tr .nopublicar{
	height:60px;
}

.nopublicar .boton_publicar {
	background:#F60;
	color:#000;
}

.nopublicar .boton_publicar:hover {
	background:#FC0;
	color:#fff;
}
h3 {
	margin-bottom:20px;
}
</style>
</head>

<body>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
  <h1>Publicar Noticia</h1>
  <div class="publicar_opciones">
<a href="<?php echo $_SERVER['PHP_SELF']; ?>?opcion=publicadas">Noticias Publicadas</a>
<a href="<?php echo $_SERVER['PHP_SELF']; ?>?opcion=nopublicadas">Noticias No Publicadas</a>
 </div>
 <h3><?php echo $titulo; ?></h3>
 <table width="640" border="0" class="tabla_publicadas"cellspacing="0" cellpadding="0">
<?php $tabla = 1; do { ?>
  <tr>
    <td width="60%" class="tabla_celda_<?php echo $tabla; ?>"><?php echo $row_rs_elegir_noticia['noticia_titulo']; ?></td>
<td align="right" class="tabla_celda_<?php echo $tabla; ?>"><a  target="_blank" href="<?php echo $Servidor_url; ?><?php echo $boton_vista_previa_link; ?>?noticia=<?php echo $row_rs_elegir_noticia['id_noticia']; ?>"><div class="vista_previa"><?php echo $boton_vista_previa; ?></div></a></td>
    <td align="right" class="tabla_celda_<?php echo $tabla; ?> <?php echo $boton_publicar_class; ?>"><a href="<?php echo $Servidor_url; ?>admin/php/publicar-noticia-db.php?opcion=<?php echo $boton_publicar_class; ?>&noticia=<?php echo $row_rs_elegir_noticia['id_noticia']; ?>"><div class="boton_publicar"><?php echo $boton_publicar; ?></div></a></td>
  </tr>
<?php 
	$tabla++;
	if($tabla == 3) {
		$tabla = 1;
	}
} while ($row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia)); ?>    
</table>
</div>
 <div class="eliminar_flotante"></div> 
  </div>

  <!-- end .content --></div>
  <!-- end .container --></div>
</body>
</html>
