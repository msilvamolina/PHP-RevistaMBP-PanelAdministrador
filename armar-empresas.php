<?php include('../paginas_include/variables-generales.php');?>
<?php
$nivel_pagina = 5;
include('php/verificar-permisos.php');


conectar('sitioweb');

$query_rs_noticias = "SELECT id_noticia, noticia_titulo, fecha_carga FROM noticias WHERE noticia_publicada = 1 ORDER BY id_noticia DESC";
$rs_noticias = mysql_query($query_rs_noticias)or die(mysql_error());
$row_rs_noticias = mysql_fetch_assoc($rs_noticias);
$totalrow_rs_noticias = mysql_num_rows($rs_noticias);

$array_noticias = array();
$array_noticias_fecha = array();

$query_rs_armar_portada = "SELECT noticias_principales FROM armar_empresas ORDER BY id_empresas DESC";
$rs_armar_portada = mysql_query($query_rs_armar_portada)or die(mysql_error());
$row_rs_armar_portada = mysql_fetch_assoc($rs_armar_portada);
$totalrow_rs_armar_portada = mysql_num_rows($rs_armar_portada);

$datos_principales = explode('-', $row_rs_armar_portada['noticias_principales']);
$datos_mosaicos = explode('-', $row_rs_armar_portada['noticias_mosaicos']);

$array_noticias_principales = array();
$array_noticias_mosaicos = array();
$array_noticias_sin_mostrar = NULL;

//Array con todas las noticias
do {
	
	$array_noticias[$row_rs_noticias['id_noticia']] = $row_rs_noticias['noticia_titulo'];
		
} while($row_rs_noticias = mysql_fetch_assoc($rs_noticias));

//Armar array noticias principales
foreach ($datos_principales as $valor) {
	$array_noticias_principales[$valor] = $array_noticias[$valor];
}
//Armar array noticias mosaicos
foreach ($datos_mosaicos as $valor) {
	$array_noticias_mosaicos[$valor] = $array_noticias[$valor];
}
//Armar array noticias sin mostrar

$resultados = array_diff($array_noticias, $array_noticias_mosaicos);
$array_noticias_sin_mostrar = array_diff($resultados, $array_noticias_principales);

desconectar();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="http://code.jquery.com/ui/1.8.21/themes/base/jquery-ui.css" rel="stylesheet">
    <link href="http://code.jquery.com/ui/1.8.21/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <script src="http://code.jquery.com/ui/1.8.21/jquery-ui.min.js"></script>
    <script src="https://raw.github.com/furf/jquery-ui-touch-punch/master/jquery.ui.touch-punch.min.js"></script>
<link rel='stylesheet' href='js/drag/styles.css' type='text/css' media='all' />
<script type="text/javascript">
  // When the document is ready set up our sortable with it's inherant function(s)
        $(function() {
		$( "#test-list" ).sortable({
            placeholder: "ui-state-highlight",
            opacity: 0.6,
        update: function(event, ui) {
		  var order = $('#test-list').sortable('serialize');
  		$("#resultado").load("js/drag/process-sortable.php?"+order);

        }
        });
        $( "#test-list" ).disableSelection(); 

$( "#test-list" ).sortable({
      cancel: ".ui-state-disabled"
    });
	
        });
</script>
<style>
.noticias_principales {
	background:#FC0;
	padding:20px;
	padding-top:1px;
}


.noticias_varias {
	background:#ccc;
	padding:20px;
	margin-top:20px;
	padding-top:1px;	
}

.appendoButtons{
	font-size:26px;
	}
</style>
</head>
<body>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
  <h1>Armar Empresas</h1>
  <h3>Importante:</h3>
  <p>Las noticias que se muestran en esta página, son las noticias que figuran como publicadas</p>
  <br />
<ul id="test-list">
<li id="listItem_sinmostrar" style="background:#8a837a; color:#fff; margin-bottom:15px;"><strong>Noticias Sin Mostrar</strong></li>

<?php foreach ($array_noticias_sin_mostrar as $clave => $valor){ ?>
  <li id="listItem_<?php echo $clave;?>"><img src="js/drag/arrow.png" alt="move" width="16" height="16" class="handle" /><strong><?php echo $valor;?></strong></li>
<?php }  ?>

<li id="listItem_principales" style="background:#e52510; color:#fff; margin-top:15px; margin-bottom:15px;"><strong>Empresas</strong></li>

<?php foreach ($array_noticias_principales as $clave => $valor){ ?>
<?php if($valor) { ?>
  <li id="listItem_<?php echo $clave;?>"><img src="js/drag/arrow.png" alt="move" width="16" height="16" class="handle" /><strong><?php echo $valor;?></strong></li>
<?php }?>
<?php }?>

<li id="listItem_sinmostrar" style="background:#8a837a; color:#fff; margin-top:15px; margin-bottom:15px;"><strong>Descartar Noticias</strong></li>

</ul>

<form action="php/armar-empresas-db.php" id="formulario_mbp" name="formulario_mbp" onsubmit="return controlar_formulario()" method="post">
<div id="resultado"></div>
</form>

 <div class="eliminar_flotante"></div> 
  </div>

  <!-- end .content --></div>
  <!-- end .container --></div>
</body>
</html>
