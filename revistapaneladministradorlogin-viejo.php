<?php include('../paginas_include/variables-generales.php');?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="robots" content="noindex,nofollow">
<title>Panel Administrador</title>
<style>
* {
	margin:0px;
	padding:0px;
}
body {
	font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;

}


a img { 
	border: none;
}

.container {
	width: 950px;
	margin:0 auto;
	margin-top:45px;
}




.fltrt { 
	float: right;
	margin-left: 8px;
}
.fltlft { 
	float: left;
	margin-right: 8px;
}
.clearfloat { 
	clear:both;
	height:0;
	font-size: 1px;
	line-height: 0px;
}

#logoMBP {
	position:absolute;
	margin-top:-22px;
	margin-left:-22px;
	
}

#encabezado_contenido {
	height:114px;
	width:100%;
}

#barravioleta {
	background:#990099;
	width:750px;
	margin-left:-16px;
	padding-left:230px;
	padding-top:5px;
	padding-bottom:5px;
	height:10px;
}

#ul_violeta {
	display:inline;
	font-weight:bold;
	color:#FFF;
	font-size:16px;

}

#ul_violeta li{

	padding:5px;
	display:inline;
}

#ul_violeta li a{
	padding:5px;
	
		color:#fff;
	text-decoration:none;
}
	

#ul_violeta a:hover{
	background:#C36;
}

#contenido {
	background:#FFF;
	padding-left:40px;
	padding-bottom:40px;
	margin-bottom:30px;
}


td {
	height:45px;
}

h1 {
	font-size:48px;
}

#registrase {
	padding:8px;
	background:#990099;
	color:#fff;
	font-weight:bold;
	font-size:14px;
	border:1px solid #900;
	-moz-border-radius:6px;
	-webkit-border-radius:6px;
	cursor:pointer;
}

#registrase:hover {
	background:#c62f64;
	border:1px solid #990099;
}

td p {
	margin-bottom:10px;
}

.buscador_fecha {
	padding-left:300px;
	padding-top:50px;
	
}

small {
	margin-bottom:20px;
}

#informacion_extra_usuario {
	display:inline;
	padding-left:20px;
}

#tabla_registro label{
	color:#000;
	font-weight:bold;
	font-size:16px;
	padding-left:10px;
	
}



#tabla_registro select{
	margin-top:5px;
	padding:4px;
	border:1px solid #c0c0c0;
	background:#fff;
	-webkit-border-radius:6px;
	-moz-border-radius:6px;		
}

#tabla_registro .registro_input_chico{
	width:79px;
}

#tabla_registro .registro_input_grande{
	width:300px;
}

#tabla_registro .registro_input_mediano{
	width:183px;
}


#tabla_registro .registro_fecha_dia{
	font-size:11px;
	color:#000;	
	width:63px;
	height:26px;
	background:#fff;	
}

#tabla_registro .registro_fecha_mes {
	font-size:11px;
	margin-left:13px;
	color:#000;	
	width:100px;
	height:26px;
	background:#fff;	
}

#tabla_registro .registro_fecha_ano{
	font-size:11px;
	margin-left:13px;
	color:#000;	
	width:70px;
	height:26px;
	background:#fff;
}

#tabla_registro .registro_fecha_separacion {

}

.registro_error {
	display: block;
	margin-top:8px;
	color: #DD4B39;
	line-height: 17px;
	font-size:12px;
	
}

.registro_opcion {
	display: block;
	margin-top:8px;
	color: #606060;
	line-height: 17px;
	font-size:12px;
	
}

.registro_condiciones {
	display: block;
	margin-top:8px;
	color: #000;
	line-height: 17px;
	font-size:12px;
	
}

.registro_separacion {
	height:20px;
}

.registro_select_grande {
	width:265px;
	height:26px;
}

.registro_bien {
	width:217px;
	color:#68b21f;
	font-weight:bold;
	font-size:14px;
	padding:4px;
	-webkit-border-radius:4px;
	-moz-border-radius:4px;		
}

.registro_mal {
	width:300px;
	border:1px solid #cf194d;
	color:#cf194d;
	background:#fddbd6;
	padding:6px;	
}

#error_general{
	border:1px solid #cf194d;
	color:#cf194d;
	background:#fddbd6;
	padding:10px;
	margin-bottom:15px;
}

#error_general ul{
	margin-top:3px;
	margin-left:10px;
}

#error_general ul li{
	list-style:none;
	padding:2px;
}
.registro_cargando {
	width:217px;
	color:#696a68;
}

.registro_aclaracion {
	width:300px;
	color:#e52510;
	padding:6px;
}

#tabla_registro td {
	height:60px;
}

#boton_susbribirse {
	font-size:24px;
	padding:16px 20px;
	background:#e52412;
	color:#FFF;
	border:1px solid #900;
	cursor:pointer;
}

#boton_susbribirse:hover{
	background:#f28502;
}

#error_general_contenedor {
	padding-right:40px;
}

#logo_contenedor {
	margin-left:-22px;
	height:27px;
}



.titulo_contenedor {
	margin-bottom:40px;
	margin-left:-40px;
}

.hr {
	color:#f48600;
	background-color:#f48600;
	width:870px;
	height:2px;
}
div.hr hr {
  display: none;
}

#contenido_principal {
	text-align:center;
	padding:20px;
}

#contenido_principal input[type='text'], #contenido_principal input[type='password'] {
	margin-left:30px;
	height:40px;
	margin-top:15px;
	padding:8px;
	font-size:24px;
	width:600px;
	font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;

}

.formulario_bajada {
	margin-left:30px;
	height:100px;
	margin-top:15px;
	padding:8px;
	font-size:24px;
	width:830px;
}

.formulario_noticia {
	margin-left:30px;
	height:400px;
	margin-top:15px;
	padding:8px;
	font-size:24px;
	width:830px;
}


#btn_cargar_noticia {
	margin-top:20px;
	padding:20px;
	width:220px;
	font-size:24px;
	background:#F00;
	color:#fff;
	font-weight:bold;
	border:1px solid #000;
	cursor:pointer;
}

#btn_cargar_noticia:hover {
	background:#F60;
}

.selecionar_autor {
	margin-right:30px;
	margin-top:10px;
	text-align:right;
}
select{
   width: 268px;
   font-size: 16px;
   line-height: 1;
   height: 34px;
}
.error {
	background:#FCC;
	border:1px solid #C00;
	padding:20px;
}
</style>
<script type="text/javascript">
function controlar_formulario() {
	usuario=document.formulario_mbp.form_usuario.value;
	contrasena=document.formulario_mbp.form_contrasena.value;

error=null;
	
	if(!usuario) {
		error='pepe';
	}
	if(!contrasena) {
		error='pepe';
	}
	if(error==null) {
		return true;
	}else{
		return false;
	}
}
</script>
<?php
 $direccion_actual = $_SERVER['REQUEST_URI'];
 $verificar_direccion = explode('/revistapaneladministrador/', $direccion_actual);
 
 $direccion_redireccion = $verificar_direccion[1];
  ?>
</head>

<body>
<div class="container"> 
  <div id="contenido">

<div id="contenido_principal">
<?php if($_GET['error'] == 'error') { ?>
<div class="error">Los datos ingresados son incorrectos, por favor vuelva a intentarlo</div>
<?php } ?>
  <form action="<?php echo $Servidor_url;?>admin/php/loguear-usuario.php" id="formulario_mbp"  onsubmit="return controlar_formulario()" name="formulario_mbp" method="post">
  <p><input type="hidden" name="redirigir" value="<?php echo $direccion_redireccion; ?>"/></p>
  <p><input type="text"  autofocus="autofocus" placeholder="Usuario" id="form_usuario" name="form_usuario"/></p>
  <p><input type="password"placeholder="Contraseña" id="form_contrasena" name="form_contrasena"/></p>
  <p><center><input type="submit"  onclick="controlar_formulario()"value="Ingresar" id="btn_cargar_noticia" name="btn_cargar_noticia" /></center></p>
  </form>
</div>
  
  </div>

  <!-- end .content --></div>
  <!-- end .container --></div>
</body>
</html>
