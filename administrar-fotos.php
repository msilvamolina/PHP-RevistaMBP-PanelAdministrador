<?php 
include('../paginas_include/variables-generales.php');
$nivel_pagina = 5;
include('php/verificar-permisos.php');

conectar('sitioweb');

$id_noticia = trim($_GET['noticia']);

$query_rs_elegir_noticia = "SELECT id_noticia, noticia_titulo, fecha_carga, fecha_modificacion  FROM noticias ORDER BY id_noticia DESC";
$rs_elegir_noticia = mysql_query($query_rs_elegir_noticia)or die(mysql_error());
$row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia);
$totalrow_rs_elegir_noticia = mysql_num_rows($rs_elegir_noticia);

if($id_noticia) {
$query_rs_noticia = "SELECT id_autor, noticia_antetitulo, noticia_titulo, noticia_bajada, noticia_cuerpo, noticia_cuerpo_con_formato, noticia_url FROM noticias WHERE id_noticia = $id_noticia";
$rs_noticia = mysql_query($query_rs_noticia)or die(mysql_error());
$row_rs_noticia = mysql_fetch_assoc($rs_noticia);
$totalrow_rs_noticia = mysql_num_rows($rs_noticia);

}

desconectar();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link rel="shortcut icon" href="<?php echo $Servidor_url; ?>favicon.ico">
<script src="<?php echo $Servidor_url; ?>00-Javascripts/autocompletar/jquery-1.3.2.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $Servidor_url; ?>00-Javascripts/ajax.js"></script>
<script  type="text/javascript" src="<?php echo $Servidor_url; ?>00-Javascripts/jquery.tipsy.js"></script>
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<?php include_once('../00-Javascripts/nicEditor/nicEdit.php'); ?>
<style type="text/css">

	#publicacion_fotos{
		width:620px;
		padding-right:20px;
		padding-top:10px;
		overflow-y:auto;
		border:1px solid #F60;
		-webkit-border-radius:6px;
		-moz-border-radius:6px;
		background:#F93;
	}
	 a {
		 color:#000;
	 }
	#upload{  
		padding:7px;
		margin:0 auto;
        text-align:center;  
		background-color:#F60;
		font-weight:bold;
        color:#000;  
        border:1px solid #000;  
		display:block;  
		text-decoration:none;
		width:80px;
		-webkit-border-radius:4px;
		-moz-border-radius:4px;
    }
		#upload a{
			color:#000;
    }
	
	#gallery{
		list-style:none;
		text-align:center;
		padding:0;
		margin-top:10px;
	}
	#gallery li{	
		display:block;
		float:left;
		width:100px;
		height:100px;
		background:#fff;
		border:1px solid #999;
		text-align:center;
		position:relative;
		padding:10px;
		margin-left:8px;
		margin-bottom:14px;
		-webkit-border-radius:4px;
		-moz-border-radius:4px;
	}
	#gallery li img{
		width:100px;
		height:100px
	}
	#gallery li a{
		position:absolute;
		right:10px;
		top:10px
	}
	#gallery li a img{ width:auto; height:auto}
	
	#crear_publicacion_descripcion {
		width:650px;
		height:400px;
	}
	.noticia_titulo {
		font-size:36px;
		padding:10px;
		width:96%;
		min-height:100px;
	}
	.noticia_bajada {
		font-size:18px;
		min-height:100px;
		padding:10px;
		width:96%;
	}
	.noticia_destacado {
		font-size:18px;
		min-height:100px;
		color:#d3222c;
		padding:10px;
		width:96%;
	}	
	#contenido_principal input[type='text'] {
		margin-top:0;
		margin-left:0;
		width:96%;
	}
	h3 {
		margin-top:25px;
		margin-bottom:10px;
	}
	.elegir_noticia {
		margin-top:10px;
		padding:15px;
		background:#6CF;
		border:1px solid #66F;
	}
	.elegir_noticia h3{
		margin-top:0;
	}
	.elegir_noticia select{
		width:100%;

	}
</style>
<script type="text/javascript" src="<?php echo $Servidor_url; ?>00-Javascripts/ajaxupload.js"></script>
<script type="text/javascript">
function controlar_formulario() {
	antetitulo=document.formulario_mbp.form_antetitulo.value;
	titulo=document.formulario_mbp.form_titulo.value;
	bajada=document.formulario_mbp.form_bajada.value;
	noticia=document.formulario_mbp.crear_publicacion_descripcion.value;

error=null;
	
	if(!antetitulo) {
		error='pepe';
	}
	if(!titulo) {
		error='pepe';
	}
	if(!bajada) {
		error='pepe';
	}
	if(!noticia) {
		error='pepe';
	}
	
	if(error==null) {
		return true;
	} else {
		return false;
	}
}

$(document).ready(function(){
				
		var button = $('#upload'), interval;
		new AjaxUpload(button,{
			action: '<?php echo $Servidor_url; ?>paginas_include/publicacion-fotos.php?publicacion=<?php echo $id_noticia; ?>', 
			name: 'image',
			onSubmit : function(file, ext){
				// cambiar el texto del boton cuando se selecicione la imagen		
				button.text('Subiendo');
				// desabilitar el boton
				this.disable();
				
				interval = window.setInterval(function(){
					var text = button.text();
					if (text.length < 11){
						button.text(text + '.');					
					} else {
						button.text('Subiendo');				
					}
				}, 200);
			},
			onComplete: function(file, response){
				button.text('Subir otra Foto');
							
				window.clearInterval(interval);
							
				// Habilitar boton otra vez
				this.enable();
				
				// Añadiendo las imagenes a mi lista
				
				if($('#gallery li').length == 0){
					$('#gallery').html(response).fadeIn("fast");
					$('#gallery li').eq(0).hide().show("slow");
				}else{
					$('#gallery').prepend(response);
					$('#gallery li').eq(0).hide().show("slow");
				}
			}
		});
		
		// Listar  fotos que hay en mi tabla
		$("#gallery").load("<?php echo $Servidor_url; ?>paginas_include/publicacion-fotos.php?action=listFotos&publicacion=<?php echo $id_noticia; ?>");
		
		// Eliminar
		
		$("#gallery li a").live("click",function(){
			var a = $(this)
			$.get("<?php echo $Servidor_url; ?>paginas_include/publicacion-fotos.php?action=eliminar",{id:a.attr("id")},function(){
				a.parent().fadeOut("slow")
			})
		})
	});
	bkLib.onDomLoaded(function() {
	new nicEditor().panelInstance('crear_publicacion_descripcion');		
})
</script>
</head>

<body>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
  <h1>Administrar Fotos</h1>
  <div class="elegir_noticia">
  <h3>Noticia:</h3>
  <form action="<?php echo $_SERVER['PHP_SELF']; ?>" name="form_elegir">
  <select name="noticia" onchange="document.forms.form_elegir.submit()">
  <option value="0">Elegir una Noticia:</option>
  <?php do { 
	if($id_noticia == $row_rs_elegir_noticia['id_noticia']) {
		$seleccionar = 'selected="selected"';
	}else{
		$seleccionar = '';
	}
	if($row_rs_elegir_noticia['fecha_modificacion']) {
		$cuantohace = 'modificada '.cuantoHace($row_rs_elegir_noticia['fecha_modificacion']);
	}else{
		$cuantohace = 'cargada '.cuantoHace($row_rs_elegir_noticia['fecha_carga']);
	}
  ?>

    <option <?php echo $seleccionar; ?> value="<?php echo $row_rs_elegir_noticia['id_noticia']; ?>"><?php echo $row_rs_elegir_noticia['noticia_titulo']; ?> - <?php echo $cuantohace;?></option>
  <?php } while($row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia)) ?>

  </select>
  </form>  
  </div>
  <?php if($totalrow_rs_noticia) { ?>
  <form action="php/administrar-fotos-db.php" id="formulario_mbp" name="formulario_mbp" method="post">
    <input type="hidden" name="form_id_noticia" value="<?php echo $id_noticia;?>"/>  
  <h3>Fotos:</h3>
      <div id="publicacion_fotos">
        <a href="javascript:void(0)" id="upload">Subir Foto</a>
        <ul id="gallery">
            <!-- Cargar Fotos -->
        </ul>
    </div>
  <p><center><input type="submit" value="Guardar Cambios" id="btn_cargar_noticia" name="btn_cargar_noticia" /></center></p>
  </form>
</div>
  <?php } ?>
    <div class="eliminar_flotante"></div>
  </div>

  <!-- end .content --></div>
<p>&nbsp;</p>
  <!-- end .container --></div>
</body>
</html>
