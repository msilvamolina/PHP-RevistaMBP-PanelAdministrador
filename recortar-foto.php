<?php 

include('../paginas_include/variables-generales.php');
$nivel_pagina = 5;
include('php/verificar-permisos.php');

conectar('sitioweb');

$id_foto = trim($_GET['foto']);

$query_rs_elegir_noticia = "SELECT noticias.noticia_titulo, fotos_publicaciones.nombre_foto, fotos_publicaciones.id_foto, fotos_publicaciones.id_publicacion FROM noticias, fotos_publicaciones  WHERE noticias.id_noticia = fotos_publicaciones.id_publicacion ORDER BY fotos_publicaciones.id_foto DESC";
$rs_elegir_noticia = mysql_query($query_rs_elegir_noticia)or die(mysql_error());
$row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia);
$totalrow_rs_elegir_noticia = mysql_num_rows($rs_elegir_noticia);

if($id_foto) {
$query_rs_noticia = "SELECT recorte_foto_nombre, nombre_foto, recorte_foto_x, recorte_foto_y, recorte_foto_w, recorte_foto_h  FROM fotos_publicaciones WHERE id_foto = $id_foto";
$rs_noticia = mysql_query($query_rs_noticia)or die(mysql_error());
$row_rs_noticia = mysql_fetch_assoc($rs_noticia);
$totalrow_rs_noticia = mysql_num_rows($rs_noticia);

$recorte_anterior = $row_rs_noticia['recorte_foto_nombre'];
desconectar();

$foto_seleccionada = $row_rs_noticia['nombre_foto'];
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link rel="shortcut icon" href="<?php echo $Servidor_url; ?>favicon.ico">
<script src="<?php echo $Servidor_url; ?>00-Javascripts/autocompletar/jquery-1.3.2.min.js" type="text/javascript"></script>

<link href="css/admin.css" rel="stylesheet" type="text/css" />
  <script src="jcrop/js/jquery.min.js"></script>
  <script src="jcrop/js/jquery.Jcrop.js"></script>
  <link rel="stylesheet" href="jcrop/demo_files/main.css" type="text/css" />
  <link rel="stylesheet" href="jcrop/demo_files/demos.css" type="text/css" />
  <link rel="stylesheet" href="jcrop/css/jquery.Jcrop.css" type="text/css" />
  <script type="text/javascript">

  $(function(){

    $('#cropbox').Jcrop({
      aspectRatio: 91 / 57,
	  setSelect:   [ 0,0,637,399 ],
	  minSize: [637,399],
      onSelect: updateCoords
    });

  });

  function updateCoords(c)
  {
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#w').val(c.w);
    $('#h').val(c.h);
  };

  function checkCoords()
  {
    if (parseInt($('#w').val())) return true;
    alert('Seleccioná una región antes de cortar');
    return false;
  };

</script>
</head>

<body>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
  <h1>Recortar Foto</h1>
  <div class="elegir_noticia">
  <h3>Foto:</h3>
  <form action="<?php echo $_SERVER['PHP_SELF']; ?>" name="form_elegir">
  <select name="foto" onchange="document.forms.form_elegir.submit()">
  <option value="0">Elegir una Foto:</option>
  <?php do { 
	if($id_foto == $row_rs_elegir_noticia['id_foto']) {
		$seleccionar = 'selected="selected"';
	}else{
		$seleccionar = '';
	}
	
  ?>

    <option <?php echo $seleccionar; ?> value="<?php echo $row_rs_elegir_noticia['id_foto']; ?>"><?php echo $row_rs_elegir_noticia['nombre_foto']; ?> - <?php echo $row_rs_elegir_noticia['noticia_titulo'];?></option>
  <?php } while($row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia)) ?>

  </select>
  </form>  
  </div><br />
  <?php if($totalrow_rs_noticia) { ?>

<?php if(($recorte_anterior) && (!$_GET['reemplazar'])) { ?>
<p>Advertencia: Esta Imagen ya fue recortada anteriormente, si continúa reemplazará este recorte:</p>
<br />
  <img src="http://www.revistambp.com/imagenes/noticias/fotos/recortes/<?php echo $recorte_anterior; ?>" />
  <br /><br /><br />
    <p><center><a id="btn_cargar_noticia" href="<?php echo $_SERVER['PHP_SELF']; ?>?foto=<?php echo $id_foto; ?>&reemplazar=1">Nuevo Recorte</a></p>

<?php }else{ ?>
  <img src="http://www.revistambp.com/imagenes/noticias/fotos/<?php echo $foto_seleccionada; ?>" id="cropbox"/>
  <form action="php/recortar-foto-db.php" method="post" onsubmit="return checkCoords();">
  			<input type="hidden" name="id_foto" value="<?php echo $id_foto; ?>"/>
			<input type="hidden" id="x" name="x" />
			<input type="hidden" id="y" name="y" />
			<input type="hidden" id="w" name="w" />
			<input type="hidden" id="h" name="h" />
  <p><center><input type="submit" value="Recortar Imagen" id="btn_cargar_noticia" name="btn_cargar_noticia" /></center></p>

		</form>
  <?php } ?>
  <?php } ?>
    <div class="eliminar_flotante"></div>
  </div>

  <!-- end .content --></div>
<p>&nbsp;</p>
  <!-- end .container --></div>
</body>
</html>
