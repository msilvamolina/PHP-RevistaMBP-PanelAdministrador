<?php include('../paginas_include/variables-generales.php');?>
<?php
$nivel_pagina = 5;
include('php/verificar-permisos.php');

conectar('sitioweb');

$query_rs_elegir_noticia = "SELECT id_noticia, noticia_titulo, publicacion_portada FROM noticias ORDER BY id_noticia DESC";
$rs_elegir_noticia = mysql_query($query_rs_elegir_noticia)or die(mysql_error());
$row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia);
$totalrow_rs_elegir_noticia = mysql_num_rows($rs_elegir_noticia);

do {
	$id = $row_rs_elegir_noticia['id_noticia'];
	$nombre = $row_rs_elegir_noticia['noticia_titulo'];
	$publicada = $row_rs_elegir_noticia['publicacion_portada'];

	if($publicada) {
		$noticias_publicadas[$id] = $nombre;
		$noticias_publicadas_antes[$id] = $nombre;
	}else {
		$noticias_sin_publicar[$id] = $nombre;
	}

}while($row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia));

$query_rs_noticias_portada = "SELECT noticias_principales, noticias_mosaicos FROM armar_portada ORDER BY id_portada DESC";
$rs_noticias_portada = mysql_query($query_rs_noticias_portada)or die(mysql_error());
$row_rs_noticias_portada = mysql_fetch_assoc($rs_noticias_portada);
$totalrow_rs_noticias_portada = mysql_num_rows($rs_noticias_portada);

$noticias_portada = $row_rs_noticias_portada['noticias_principales'].'-'.$row_rs_noticias_portada['noticias_mosaicos'];

$portada_noticias_principales = explode('-', $noticias_portada);

$limite_mostrar_sin_publicar = 3;
desconectar();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../00-Javascripts/jquery.js"></script>
<script type="text/javascript" src="../00-Javascripts/ajax.js"></script>
<style>
.publicar_opciones {
	padding-top:20px;
	margin-bottom:40px;
}
.publicar_opciones a{
	margin-right:30px;
	background:#39C;
	padding:10px;
	color:#fff;
	text-decoration:none;
}
.publicar_opciones a:hover{
	background:#63C;
}

.boton_publicar {
	padding:8px;
	text-align:center;
	background:#3C0;
	color:#000;
}
.boton_publicar:hover {
	color:#fff;
	background:#6C3;
}

.vista_previa {
	padding:8px;
	text-align:center;
	background:#69F;
	color:#fff;
}
.vista_previa:hover {
	color:#fff;
	background:#636;
}

.tabla_celda_1 {
	padding:10px;
	background:#CCC;
}
.tabla_celda_2 {
	padding:10px;
	background:#FFC;
}

a {		text-decoration:none;
}

.tabla_publicadas tr td{
	height:40px;
}

.tabla_publicadas tr .nopublicar{
	height:60px;
}

.nopublicar .boton_publicar {
	background:#F60;
	color:#000;
}

.nopublicar .boton_publicar:hover {
	background:#FC0;
	color:#fff;
}
h3 {
	margin-bottom:20px;
}
</style>
</head>

<body>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
  <h1>Publicar Noticias</h1>
<h3>Noticias sin publicar en la página principal</h3>
 <table width="640" border="0" class="tabla_publicadas"cellspacing="0" cellpadding="0">
<?php $tabla = 1; 
	foreach ($noticias_sin_publicar as $clave => $valor) {
		if($i < $limite_mostrar_sin_publicar) {
	 ?>
  <tr>
    <td width="60%" class="tabla_celda_<?php echo $tabla; ?>"><?php echo $valor; ?></td>
<td align="right" class="tabla_celda_<?php echo $tabla; ?>"><a  target="_blank" href="<?php echo $Servidor_url; ?>admin/editar-noticia.php?noticia=<?php echo $clave; ?>"><div class="vista_previa">Editar</div></a></td>
    <td align="right" class="tabla_celda_<?php echo $tabla; ?> <?php echo $boton_publicar_class; ?>"><a href="<?php echo $Servidor_url; ?>admin/php/publicar-noticias-nuevo-db.php?noticia=<?php echo $clave; ?>"><div class="boton_publicar">Publicar</div></a></td>
  </tr>
<?php
	
	$i++;
	$tabla++;
	if($tabla == 3) {
		$tabla = 1;
	}
}
}  ?>    
</table>
<br>
<h3>Noticias Actualmente Publicadas en la Portada</h3>

 <table width="640" border="0" class="tabla_publicadas"cellspacing="0" cellpadding="0">
<?php $tabla = 1; 
	foreach ($portada_noticias_principales as $clave) {
		$noticias_publicadas_antes[$clave] = NULL;
	 ?>
  <tr>
    <td width="80%" class="tabla_celda_<?php echo $tabla; ?>"><?php echo $noticias_publicadas[$clave]; ?></td>
<td align="right" class="tabla_celda_<?php echo $tabla; ?>"><a  target="_blank" href="<?php echo $Servidor_url; ?>admin/editar-noticia.php?noticia=<?php echo $clave; ?>"><div class="vista_previa">Editar</div></a></td>
  </tr>
<?php
	
	$tabla++;
	if($tabla == 3) {
		$tabla = 1;
	}

}  ?>    
</table>

<br>
<h3>Noticias Publicadas que ya no están en la portada</h3>

 <table width="640" border="0" class="tabla_publicadas"cellspacing="0" cellpadding="0">
<?php $tabla = 1; 
	foreach ($noticias_publicadas_antes as $clave => $valor) {
		if($valor) {
	 ?>
  <tr>
    <td width="60%" class="tabla_celda_<?php echo $tabla; ?>"><?php echo $valor; ?></td>
	<td align="right" class="tabla_celda_<?php echo $tabla; ?>"><a  target="_blank" href="<?php echo $Servidor_url; ?>admin/editar-noticia.php?noticia=<?php echo $clave; ?>"><div class="vista_previa">Editar</div></a></td>
    <td align="right" class="tabla_celda_<?php echo $tabla; ?> <?php echo $boton_publicar_class; ?>"><a href="<?php echo $Servidor_url; ?>admin/php/publicar-noticias-nuevo-db.php?noticia=<?php echo $clave; ?>"><div class="boton_publicar">Publicar</div></a></td>
  </tr>
<?php
	
	$tabla++;
	if($tabla == 3) {
		$tabla = 1;
	}
}
}  ?>    
</table>
</div>
 <div class="eliminar_flotante"></div> 
  </div>

  <!-- end .content --></div>
  <!-- end .container --></div>
</body>
</html>
