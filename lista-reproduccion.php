<?php include('../paginas_include/variables-generales.php');?>
<?php
$nivel_pagina = 5;
include('php/verificar-permisos.php');


conectar('sitioweb');

$query_rs_noticias = "SELECT id_video, video_titulo FROM videos ORDER BY id_video DESC";
$rs_noticias = mysql_query($query_rs_noticias)or die(mysql_error());
$row_rs_noticias = mysql_fetch_assoc($rs_noticias);
$totalrow_rs_noticias = mysql_num_rows($rs_noticias);


$array_noticias = array();
$array_noticias_fecha = array();


$id_noticia = trim($_GET['noticia']);

$query_rs_elegir_noticia = "SELECT id_noticia, noticia_titulo, fecha_carga, fecha_modificacion  FROM noticias ORDER BY id_noticia DESC";
$rs_elegir_noticia = mysql_query($query_rs_elegir_noticia)or die(mysql_error());
$row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia);
$totalrow_rs_elegir_noticia = mysql_num_rows($rs_elegir_noticia);

if($id_noticia) { 
$query_rs_noticia_elegida = "SELECT id_noticia, noticia_titulo FROM noticias WHERE id_noticia = $id_noticia";
$rs_noticia_elegida = mysql_query($query_rs_noticia_elegida)or die(mysql_error());
$row_rs_noticia_elegida = mysql_fetch_assoc($rs_noticia_elegida);
$totalrow_rs_noticia_elegida = mysql_num_rows($rs_noticia_elegida);

$query_rs_armar_portada = "SELECT lista_videos FROM videos_listas_reproduccion WHERE id_noticia = $id_noticia";
$rs_armar_portada = mysql_query($query_rs_armar_portada)or die(mysql_error());
$row_rs_armar_portada = mysql_fetch_assoc($rs_armar_portada);
$totalrow_rs_armar_portada = mysql_num_rows($rs_armar_portada);

$datos_principales = explode('-', $row_rs_armar_portada['lista_videos']);

$array_noticias_principales = array();
$array_noticias_mosaicos = array();
$array_noticias_sin_mostrar = NULL;

//Array con todas las noticias
do {
	
	$array_noticias[$row_rs_noticias['id_video']] = $row_rs_noticias['video_titulo'];
		
} while($row_rs_noticias = mysql_fetch_assoc($rs_noticias));

//Armar array noticias principales
foreach ($datos_principales as $valor) {
	$array_noticias_principales[$valor] = $array_noticias[$valor];
}

//Armar array noticias sin mostrar

$resultados = array_diff($array_noticias, $array_noticias_mosaicos);
$array_noticias_sin_mostrar = array_diff($resultados, $array_noticias_principales);

}


desconectar();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link href="css/admin.css" rel="stylesheet" type="text/css" />
 <link href="http://code.jquery.com/ui/1.8.21/themes/base/jquery-ui.css" rel="stylesheet">
    <link href="http://code.jquery.com/ui/1.8.21/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <script src="http://code.jquery.com/ui/1.8.21/jquery-ui.min.js"></script>
    <script src="https://raw.github.com/furf/jquery-ui-touch-punch/master/jquery.ui.touch-punch.min.js"></script>
<link rel='stylesheet' href='js/drag/styles.css' type='text/css' media='all' />
<script type="text/javascript">
  // When the document is ready set up our sortable with it's inherant function(s)
        $(function() {
		$( "#test-list" ).sortable({
            placeholder: "ui-state-highlight",
            opacity: 0.6,
        update: function(event, ui) {
		  var order = $('#test-list').sortable('serialize');
  		$("#resultado").load("js/drag/process-sortable.php?"+order);

        }
        });
        $( "#test-list" ).disableSelection(); 

$( "#test-list" ).sortable({
      cancel: ".ui-state-disabled"
    });
	
        });
</script>
<style>
.noticias_principales {
	background:#FC0;
	padding:20px;
	padding-top:1px;
}


.noticias_varias {
	background:#ccc;
	padding:20px;
	margin-top:20px;
	padding-top:1px;	
}

.appendoButtons{
	font-size:26px;
	}
</style>
</head>
<body>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
  <h1>Lista de Reproducción</h1>
  <br />
  <div class="elegir_noticia">
  <h3>Noticia:</h3>
  <form action="<?php echo $_SERVER['PHP_SELF']; ?>" name="form_elegir">
  <select name="noticia" onchange="document.forms.form_elegir.submit()">
  <option value="0">Elegir una Noticia:</option>
  <?php do { 
	if($id_noticia == $row_rs_elegir_noticia['id_noticia']) {
		$seleccionar = 'selected="selected"';
	}else{
		$seleccionar = '';
	}
	if($row_rs_elegir_noticia['fecha_modificacion']) {
		$cuantohace = 'modificada '.cuantoHace($row_rs_elegir_noticia['fecha_modificacion']);
	}else{
		$cuantohace = 'cargada '.cuantoHace($row_rs_elegir_noticia['fecha_carga']);
	}
  ?>

    <option <?php echo $seleccionar; ?> value="<?php echo $row_rs_elegir_noticia['id_noticia']; ?>"><?php echo $row_rs_elegir_noticia['noticia_titulo']; ?> - <?php echo $cuantohace;?></option>
  <?php } while($row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia)) ?>

  </select>
  </form>  
  </div>
  <?php if($totalrow_rs_noticia_elegida) { ?> 
<?php if(($totalrow_rs_armar_portada)&&(!$_GET['reemplazar'])) { ?> 
    <br />    
<p>Esta noticia ya cuenta con una lista de reproducción</p>
    <br />        <br />        <br />    
<center><a href="lista-reproduccion.php?noticia=<?php echo $id_noticia; ?>&reemplazar=1" id="btn_cargar_noticia">Reemplazar</a></center>
<?php } else { ?>
    <br />    
<ul id="test-list">
<li id="listItem_sinmostrar" style="background:#8a837a; color:#fff; margin-bottom:15px;"><strong>Videos Sin Mostrar</strong></li>

<?php foreach ($array_noticias_sin_mostrar as $clave => $valor){ ?>
  <li id="listItem_<?php echo $clave;?>"><img src="js/drag/arrow.png" alt="move" width="16" height="16" class="handle" /><strong><?php echo $valor;?></strong></li>
<?php }  ?>

<li id="listItem_principales" style="background:#e52510; color:#fff; margin-top:15px; margin-bottom:15px;"><strong>Videos</strong></li>

<?php foreach ($array_noticias_principales as $clave => $valor){ ?>
<?php if($valor) { ?>
  <li id="listItem_<?php echo $clave;?>"><img src="js/drag/arrow.png" alt="move" width="16" height="16" class="handle" /><strong><?php echo $valor;?></strong></li>
<?php }?>
<?php }?>

<li id="listItem_sinmostrar" style="background:#8a837a; color:#fff; margin-top:15px; margin-bottom:15px;"><strong>Descartar Videos</strong></li>

</ul>

<form action="php/lista-reproduccion-db.php" id="formulario_mbp" name="formulario_mbp" onsubmit="return controlar_formulario()" method="post">
<input type="hidden" name="id_noticia" value="<?php echo $id_noticia; ?>"/>

<div id="resultado"></div>
</form>
<?php } } ?>
 <div class="eliminar_flotante"></div> 
  </div>

  <!-- end .content --></div>
  <!-- end .container --></div>
</body>
</html>
