<?php include('../paginas_include/variables-generales.php');?>
<?php
$nivel_pagina = 5;
include('php/verificar-permisos.php');

//Redes sociales
function Saber_Twitter($url) {     
  $json_string = file_get_contents('http://urls.api.twitter.com/1/urls/count.json?url=' . $url);
  $json = json_decode($json_string, true);
  return intval( $json['count'] );
}

function Saber_Facebook($url) {     
  $json_string = file_get_contents('http://graph.facebook.com/?id='.$url);
  $json = json_decode($json_string, true);
  return intval( $json['shares'] );
}

function Saber_Google($url) {
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, "https://clients6.google.com/rpc");
  curl_setopt($curl, CURLOPT_POST, 1);
  curl_setopt($curl, CURLOPT_POSTFIELDS, '[{"method":"pos.plusones.get","id":"p","params":{"nolog":true,"id":"' . $url . '","source":"widget","userId":"@viewer","groupId":"@self"},"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
  $curl_results = curl_exec ($curl);
  curl_close ($curl);
  $json = json_decode($curl_results, true);
  return intval( $json[0]['result']['metadata']['globalCounts']['count'] );
}

conectar('sitioweb');
$query_rs_notas = "SELECT id_noticia, noticia_titulo, noticia_url FROM noticias WHERE noticia_publicada = 1";
$rs_notas = mysql_query($query_rs_notas)or die(mysql_error());
$row_rs_notas = mysql_fetch_assoc($rs_notas);
$totalrow_rs_notas = mysql_num_rows($rs_notas);
desconectar();


//array notas
$array_notas = array();
do {
	$noticia_url = $row_rs_notas['noticia_url'];
	$id_noticia =  $row_rs_notas['id_noticia'];

	$array_notas[$id_noticia] = $row_rs_notas['noticia_titulo'];
		
	$compartidas_facebook[$id_noticia] = Saber_Facebook($noticia_url);
	$compartidas_twitter[$id_noticia] = Saber_Twitter($noticia_url);
	$compartidas_google[$id_noticia] = Saber_Google($noticia_url);

}while($row_rs_notas = mysql_fetch_assoc($rs_notas));

arsort($compartidas_facebook);
arsort($compartidas_twitter);
arsort($compartidas_google);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../00-Javascripts/jquery.js"></script>
<script type="text/javascript" src="../00-Javascripts/ajax.js"></script>
<style>
p strong {
	color:#900;
}
ul li {
	list-style:none;
	list-style-type:none;
	padding-left:5px;
}
tr td {
	padding-left:10px;
}
tr td selection{
	font-size:14px;
	min-width:50px;
	padding:8px;
	background:#F00;
	color:#fff;
}

.celda_1 {
	background:#CCC;
}
.celda_2 {
	background:#999;
}
.estadisticas_opciones {
	background:#900;
	color:#fff;
}
.estadisticas_opciones a {
	color:#fff;
	width:100%;
	padding:12px;
	text-decoration:none;
}
.boton_arriba a{
	background:#900;	
	color:#fff;
	padding:12px;
	text-decoration:none;
}
.boton_arriba a:hover {
	background:#F30;
}
.estadisticas_opciones a:hover {
	background:#F30;
}
.opcion_elegida {
	background:#F30;
}
</style>
</head>

<body>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
<a name="top"></a>
<h1>Redes Sociales</h1>
<br />
<h3>Facebook, notas más compartidas:</h3>
<table width="100%" border="0" class="tabla_estadisticas" cellspacing="0" cellpadding="0">
<?php $i=1; foreach ($compartidas_facebook as $clave => $valor){ ?>
<?php if($compartidas_facebook[$clave]) { ?>
  <tr>
    <td class="celda_<?php echo $i;?>"><?php echo $array_notas[$clave];?></td>
    <td align="right" class="celda_<?php echo $i;?>"><selection><?php echo $valor;?></selection></td>
  </tr>
  <?php } ?>
<?php 
$i++;
if($i==3) {$i=1;}
} ?>  
</table>

<br />
<h3>Twitter, notas más compartidas:</h3>
<table width="100%" border="0" class="tabla_estadisticas" cellspacing="0" cellpadding="0">
<?php $i=1; foreach ($compartidas_twitter as $clave => $valor){ ?>
<?php if($compartidas_twitter[$clave]) { ?>
  <tr>
    <td class="celda_<?php echo $i;?>"><?php echo $array_notas[$clave];?></td>
    <td align="right" class="celda_<?php echo $i;?>"><selection><?php echo $valor;?></selection></td>
  </tr>
  <?php } ?>
<?php 
$i++;
if($i==3) {$i=1;}
} ?>  
</table>

<br />
<h3>Google +, notas más compartidas:</h3>
<table width="100%" border="0" class="tabla_estadisticas" cellspacing="0" cellpadding="0">
<?php $i=1; foreach ($compartidas_google as $clave => $valor){ ?>
<?php if($compartidas_google[$clave]) { ?>
  <tr>
    <td class="celda_<?php echo $i;?>"><?php echo $array_notas[$clave];?></td>
    <td align="right" class="celda_<?php echo $i;?>"><selection><?php echo $valor;?></selection></td>
  </tr>
  <?php } ?>
<?php 
$i++;
if($i==3) {$i=1;}
} ?>  
</table>
<br /><br />
<center><div class="boton_arriba"><a href="#top">Volver para arriba</a></div></center>
</div>
 <div class="eliminar_flotante"></div> 
  </div>

  <!-- end .content --></div>
  <!-- end .container --></div>
</body>
</html>
