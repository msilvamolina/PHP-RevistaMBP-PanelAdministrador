<?php 

include('../paginas_include/variables-generales.php');
$nivel_pagina = 5;
include('php/verificar-permisos.php');

$id_noticia = trim($_GET['noticia']);

//verificamos la direccion ingresada
$link_db = "http://".$_SERVER['HTTP_HOST'].'/admin/editar-noticia.php?noticia='.$id_noticia;
$pagina_url ="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

if($link_db != $pagina_url) {
	$redireccion = $link_db;
	header('Location: '.$redireccion);
	exit;
}

conectar('sitioweb');

$query_rs_elegir_noticia = "SELECT id_noticia, noticia_titulo, fecha_carga, fecha_modificacion  FROM noticias ORDER BY id_noticia DESC";
$rs_elegir_noticia = mysql_query($query_rs_elegir_noticia)or die(mysql_error());
$row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia);
$totalrow_rs_elegir_noticia = mysql_num_rows($rs_elegir_noticia);

if($id_noticia) {
$query_rs_noticia = "SELECT id_revista, noticia_antetitulo, noticia_titulo, noticia_titulo_corto, noticia_bajada, noticia_cuerpo, noticia_cuerpo_con_formato FROM noticias WHERE id_noticia = $id_noticia";
$rs_noticia = mysql_query($query_rs_noticia)or die(mysql_error());
$row_rs_noticia = mysql_fetch_assoc($rs_noticia);
$totalrow_rs_noticia = mysql_num_rows($rs_noticia);

$query_rs_vinculacion_autores = "SELECT id_autor FROM vinculacion_autor_noticia WHERE id_noticia = $id_noticia";
$rs_vinculacion_autores = mysql_query($query_rs_vinculacion_autores)or die(mysql_error());
$row_rs_vinculacion_autores = mysql_fetch_assoc($rs_vinculacion_autores);
$totalrow_rs_vinculacion_autores = mysql_num_rows($rs_vinculacion_autores);

$query_rs_autores = "SELECT autores.id_autor, autores.nombre_autor FROM autores ORDER BY autores.nombre_autor ASC";
$rs_autores = mysql_query($query_rs_autores)or die(mysql_error());
$row_rs_autores = mysql_fetch_assoc($rs_autores);
$totalrow_rs_autores = mysql_num_rows($rs_autores);

desconectar();

$noticia_id_autor = $row_rs_noticia['id_autor'];
$noticia_antetitulo = $row_rs_noticia['noticia_antetitulo'];
$noticia_titulo_corto = $row_rs_noticia['noticia_titulo_corto'];
$noticia_titulo = $row_rs_noticia['noticia_titulo'];
$noticia_bajada = $row_rs_noticia['noticia_bajada'];
$noticia_cuerpo = $row_rs_noticia['noticia_cuerpo'];
$noticia_cuerpo_con_formato = $row_rs_noticia['noticia_cuerpo_con_formato'];
$noticia_url = $row_rs_noticia['noticia_url'];

$noticia_cuerpo = str_replace('.', '.<br>', $noticia_cuerpo);

if($row_rs_noticia['noticia_cuerpo_con_formato']) {
	$noticia_cuerpo = $row_rs_noticia['noticia_cuerpo_con_formato'];
}
	
$array_autores = array();
do {
	$array_autores[$row_rs_autores['id_autor']] = $row_rs_autores['nombre_autor'];
} while ($row_rs_autores = mysql_fetch_assoc($rs_autores));

conectar('magazine');
$query_rs_revistas = "SELECT description, id FROM mag_numbers ORDER BY id ASC";
$rs_revistas = mysql_query($query_rs_revistas)or die(mysql_error());
$row_rs_revistas = mysql_fetch_assoc($rs_revistas);
$totalrow_rs_revistas = mysql_num_rows($rs_revistas);
desconectar();

}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link rel="shortcut icon" href="<?php echo $Servidor_url; ?>favicon.ico">
<script src="<?php echo $Servidor_url; ?>00-Javascripts/autocompletar/jquery-1.3.2.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $Servidor_url; ?>00-Javascripts/ajax.js"></script>
<script  type="text/javascript" src="<?php echo $Servidor_url; ?>00-Javascripts/jquery.tipsy.js"></script>
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<script src="http://js.nicedit.com/nicEdit-latest.js" type="text/javascript"></script>
<style type="text/css">

	#publicacion_fotos{
		width:620px;
		padding-right:20px;
		padding-top:10px;
		overflow-y:auto;
		border:1px solid #F60;
		-webkit-border-radius:6px;
		-moz-border-radius:6px;
		background:#F93;
	}
	 a {
		 color:#000;
	 }
	#upload{  
		padding:7px;
		margin:0 auto;
        text-align:center;  
		background-color:#F60;
		font-weight:bold;
        color:#000;  
        border:1px solid #000;  
		display:block;  
		text-decoration:none;
		width:80px;
		-webkit-border-radius:4px;
		-moz-border-radius:4px;
    }
		#upload a{
			color:#000;
    }
	
	#gallery{
		list-style:none;
		text-align:center;
		padding:0;
		margin-top:10px;
	}
	#gallery li{	
		display:block;
		float:left;
		width:100px;
		height:100px;
		background:#fff;
		border:1px solid #999;
		text-align:center;
		position:relative;
		padding:10px;
		margin-left:8px;
		margin-bottom:14px;
		-webkit-border-radius:4px;
		-moz-border-radius:4px;
	}
	#gallery li img{
		width:100px;
		height:100px
	}
	#gallery li a{
		position:absolute;
		right:10px;
		top:10px
	}
	#gallery li a img{ width:auto; height:auto}
	
	#crear_publicacion_descripcion {
		width:650px;
		height:400px;
	}
	.noticia_titulo {
		font-size:36px;
		padding:10px;
		width:96%;
		min-height:100px;
	}
	.noticia_bajada {
		font-size:18px;
		min-height:100px;
		padding:10px;
		width:96%;
	}
	.noticia_destacado {
		font-size:18px;
		min-height:100px;
		color:#d3222c;
		padding:10px;
		width:96%;
	}	
	#contenido_principal input[type='text'] {
		margin-top:0;
		margin-left:0;
		width:96%;
	}

	.elegir_noticia {
		margin-top:10px;
		padding:15px;
		background:#6CF;
		border:1px solid #66F;
	}
	.elegir_noticia h3{
		margin-top:0;
	}
	.elegir_noticia select{
		width:100%;

	}
</style>
<script type="text/javascript" src="<?php echo $Servidor_url; ?>00-Javascripts/ajaxupload.js"></script>
	<script type="text/javascript" src="00-appendo/jquery.pack.js"></script>
	<script type="text/javascript" src="00-appendo/jquery.appendo.js"></script>
<script type="text/javascript">
jQuery(function() { 
		jQuery('#noticia_autores').appendo({ maxRows: 1000, labelAdd: 'Autor(+)', labelDel: 'Autor(-)'});

});

function controlar_formulario() {
	
	antetitulo=document.formulario_mbp.form_antetitulo.value;
	titulo=document.formulario_mbp.form_titulo.value;
	bajada=document.formulario_mbp.form_bajada.value;
	noticia=document.formulario_mbp.crear_publicacion_descripcion.value;

error=null;
	
	if(!antetitulo) {
		error='pepe';
	}
	if(!titulo) {
		error='pepe';
	}
	if(!bajada) {
		error='pepe';
	}
	if(!noticia) {
		error='pepe';
	}
	
	if(error==null) {
		return true;
	} else {
		return false;
	}
}


	bkLib.onDomLoaded(function() {
	new nicEditor({fullPanel : true}).panelInstance('crear_publicacion_descripcion');

	
})
</script>
</head>

<body>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
  <h1>Editar Noticia</h1>
  <div class="elegir_noticia">
  <h3>Noticia:</h3>
  <form action="<?php echo $_SERVER['PHP_SELF']; ?>" name="form_elegir">
  <select name="noticia" onchange="document.forms.form_elegir.submit()">
  <option value="0">Elegir una Noticia:</option>
  <?php do { 
	if($id_noticia == $row_rs_elegir_noticia['id_noticia']) {
		$seleccionar = 'selected="selected"';
	}else{
		$seleccionar = '';
	}
	if($row_rs_elegir_noticia['fecha_modificacion']) {
		$cuantohace = 'modificada '.cuantoHace($row_rs_elegir_noticia['fecha_modificacion']);
	}else{
		$cuantohace = 'cargada '.cuantoHace($row_rs_elegir_noticia['fecha_carga']);
	}
  ?>

    <option <?php echo $seleccionar; ?> value="<?php echo $row_rs_elegir_noticia['id_noticia']; ?>"><?php echo $row_rs_elegir_noticia['noticia_titulo']; ?> - <?php echo $cuantohace;?></option>
  <?php } while($row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia)) ?>

  </select>
  </form>  
  </div>
  <?php if($totalrow_rs_noticia) { ?>
  <form action="php/editar-noticia-db.php" id="formulario_mbp" name="formulario_mbp" onsubmit="return controlar_formulario()" method="post">
    <input type="hidden" name="form_id_noticia" value="<?php echo $id_noticia;?>"/>
  <h3>Antetítulo:</h3>
  <div id="form_antetitulo">
  <input type="text" name="form_antetitulo" class="noticia_antetitulo" value="<?php echo $noticia_antetitulo;?>"/>
  </div>
  <h3>Título:</h3>
  <div id="form_titulo">
  <textarea name="form_titulo" class="noticia_titulo"><?php echo $noticia_titulo;?></textarea>
  </div>
   <h3>Título Corto:</h3>
  <div id="form_antetitulo">
  <input type="text" name="form_titulo_corto" maxlength="52" class="noticia_antetitulo" value="<?php echo $noticia_titulo_corto;?>"/>
  </div> 
  <h3>Autores:</h3>  
<?php
$noticias_mosaicos_minimo = 5;
$i = 1;
$id_autor = 0;

$vinculacion_autores_actual = NULL;
?>         
   	<TABLE id="noticia_autores" class="tabla_noticia">
	<TBODY>
<?php do {
	$id_autor = $row_rs_vinculacion_autores['id_autor'];
	
	if(!$vinculacion_autores_actual) {
		$vinculacion_autores_actual = $id_autor;
	}else{
		$vinculacion_autores_actual = $vinculacion_autores_actual.'-'.$id_autor;		
	}
	 ?>    
  	<TR>
		<TD> 
  <select name="noticia_autores[]" id="noticia_autores" >
  <option selected="selected" value="0">Sin autor</option>
<?php foreach ($array_autores as $clave => $valor){
	if($id_autor == $clave) {
	$seleccionar = 'selected="selected"';
	}else{
	$seleccionar = NULL;	
	}	
	 ?>  
 <option <?php echo $seleccionar; ?> value="<?php echo $clave; ?>"><?php echo $valor; ?></option>
 <?php }; ?>
  </select>
  		</TD> 
	</TR>   
<?php } while($row_rs_vinculacion_autores = mysql_fetch_assoc($rs_vinculacion_autores)); ?>
	</TBODY></TABLE> 
 <input type="hidden" name="vinculacion_autores_actual" value="<?php echo $vinculacion_autores_actual;?>"/>   
 <h3>Revista a la que Pertenece:</h3>  
  <p><div class="selecionar_autor">
  <select id="select_autor" name="select_revista">
    <option value="0">Sin revista</option>
  <?php
  $seleccionar = NULL;
  $id_revista = $row_rs_noticia['id_revista'];
   do{ 
   $id_revista_nueva = $row_rs_revistas['id'];
  	if($id_revista == $id_revista_nueva) {
	$seleccionar = 'selected="selected"';
	}else{
	$seleccionar = NULL;	
	}	
  ?>
    <option <?php echo $seleccionar; ?>  value="<?php echo $row_rs_revistas['id']; ?>"><?php echo $row_rs_revistas['description']; ?></option>
  <?php } while ($row_rs_revistas = mysql_fetch_assoc($rs_revistas)); ?>
  </select></div></p>     
  <h3>Bajada:</h3>  
  <div id="form_bajada">
  <textarea name="form_bajada" class="noticia_bajada"><?php echo $noticia_bajada;?></textarea>
  </div>
  <h3>Cuerpo:</h3>         
          <div id="crear_publicacion_div_descripcion">
    	    	<textarea id="crear_publicacion_descripcion"  name="crear_publicacion_descripcion">
                <?php echo $noticia_cuerpo; ?>
                </textarea>
        	</div> 
  <p><center><input type="submit" value="Guardar Cambios" id="btn_cargar_noticia" name="btn_cargar_noticia" /></center></p>
  </form>
</div>
  <?php } ?>
    <div class="eliminar_flotante"></div>
  </div>

  <!-- end .content --></div>
<p>&nbsp;</p>
  <!-- end .container --></div>
</body>
</html>
