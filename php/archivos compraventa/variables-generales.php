<?php
/*
 * PHP que contiene las variables que m�s utilizamos en PHP. Como, por ejemplo: Amigable.
 * Creado el Mi�rcoles 30/05
*/ 

/* 
 * Devuelve el nombre del archivo filtrado de la siguiente manera: 
 * Los espacios sustituidos por '-' 
 * Los acentos se quitan de las vocales 
 * 'n' en vez de '�' 
 * Cualquier otro caracter especial diferente de ('_', '-', '.') se elimina 
*/ 

//locahost
$Servidor_url = 'https://www.compraventaok.com.ar/';

if (!function_exists("amigable")) {
function amigable($nom_archivo) 
{ 
    $arr_busca = array(' ','�','�','�','�','�','�','�', 
    '�','�', '�','�','�','�','�','�','�','�','�','�', 
    '�','�','�','�','�', '�','�','�','�','�','�','�', 
    '�','�','�','�','�','�','�','�','�','�','�'); 
    $arr_susti = array('-','a','a','a','a','a','A','A', 
    'A','A','e','e','e','E','E','E','i','i','i','I','I', 
    'I','o','o','o','o','o','O','O','O','O','u','u','u','u', 
    'U','U','U','U','c','C','N','n'); 
    $nom_archivo = str_replace($arr_busca, $arr_susti, $nom_archivo);
	$nom_archivo = trim(preg_replace('/[^a-zA-Z0-9\s\-]/', '', $nom_archivo));
    return $nom_archivo;
}
}
if (!function_exists("amigable_acentos")) {
function amigable_acentos($nom_archivo) 
{ 
    $arr_busca = array('�','�','�','�','�','�','�', 
    '�','�', '�','�','�','�','�','�','�','�','�','�', 
    '�','�','�','�','�', '�','�','�','�','�','�','�', 
    '�','�','�','�','�','�','�','�','�','�','�'); 
    $arr_susti = array('a','a','a','a','a','A','A', 
    'A','A','e','e','e','E','E','E','i','i','i','I','I', 
    'I','o','o','o','o','o','O','O','O','O','u','u','u','u', 
    'U','U','U','U','c','C','N','n'); 
    $nom_archivo = str_replace($arr_busca, $arr_susti, $nom_archivo);
    return $nom_archivo;
}
}
if (!function_exists("getRealIP")) {
function getRealIP()
{
 
   if(@$_SERVER['HTTP_X_FORWARDED_FOR'] != '' )
   {
      $client_ip = 
         ( !empty($_SERVER['REMOTE_ADDR']) ) ? 
            $_SERVER['REMOTE_ADDR'] 
            : 
            ( ( !empty($_ENV['REMOTE_ADDR']) ) ? 
               $_ENV['REMOTE_ADDR'] 
               : 
               "unknown" );
 
      // los proxys van a�adiendo al final de esta cabecera
      // las direcciones ip que van "ocultando". Para localizar la ip real
      // del usuario se comienza a mirar por el principio hasta encontrar 
      // una direcci�n ip que no sea del rango privado. En caso de no 
      // encontrarse ninguna se toma como valor el REMOTE_ADDR
 
      $entries = split('[, ]', $_SERVER['HTTP_X_FORWARDED_FOR']);
 
      reset($entries);
      while (list(, $entry) = each($entries)) 
      {
         $entry = trim($entry);
         if ( preg_match("/^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/", $entry, $ip_list) )
         {
            // http://www.faqs.org/rfcs/rfc1918.html
            $private_ip = array(
                  '/^0\./', 
                  '/^127\.0\.0\.1/', 
                  '/^192\.168\..*/', 
                  '/^172\.((1[6-9])|(2[0-9])|(3[0-1]))\..*/', 
                  '/^10\..*/');
 
            $found_ip = preg_replace($private_ip, $client_ip, $ip_list[1]);
 
            if ($client_ip != $found_ip)
            {
               $client_ip = $found_ip;
               break;
            }
         }
      }
   }
   else
   {
      $client_ip = 
         ( !empty($_SERVER['REMOTE_ADDR']) ) ? 
            $_SERVER['REMOTE_ADDR'] 
            : 
            ( ( !empty($_ENV['REMOTE_ADDR']) ) ? 
               $_ENV['REMOTE_ADDR'] 
               : 
               "unknown" );
   }
 
   return $client_ip;
 
}

//Ip del visitante de la p�gina
$ip_visitante = getRealIP();
}

//Conectar con las bases de datos

if (!function_exists("conectar")) {

function conectar($db)
{
	$db = "admicvok_".$db;
	mysql_connect("localhost", "admicvok_martin", "birlibirloqueGB335");
	mysql_select_db($db);
}

function desconectar()
{
	mysql_close();
}

}

if (!function_exists("cuantoHace")) {
function cuantoHace($PublishDate){
/*Calcula el tiempo que hace desde $PublishDate hasta la fecha actual, con arreglo a un
formato personalizado: Muestra segundos, minutos, horas, o dias transcurridos.*/
$PublishDate = strtotime($PublishDate);
$actual=time();
$dif=$actual-$PublishDate;
$segundos = $dif;
$segundos > 0 ? $segundos_text = $segundos." segundos " : $segundos_text = "";
$minutos = floor($dif/(60));
$minutos > 1 ? $minutos_text=$minutos." minutos " : $minutos_text = "";
$minutos == 1 ? $minutos_text = $minutos." minuto " : $minutos_text = $minutos_text;
$horas = floor($dif/(60*60));
$horas > 1 ? $horas_text = $horas." horas ":$horas_text = "";
$horas == 1 ? $horas_text = $horas." hora " : $horas_text = $horas_text;
$dias = floor($dif/(60*60*24));
$dias > 1 ? $dias_text = $dias." dias " : $dias_text = "";
$dias == 1 ? $dias_text = $dias." dia " : $dias_text = $dias_text;
/*El texto a mostrar para saber el tiempo transcurrido entre la fecha dada y el momento actual�..*/
$time_dif_text="";
$segundos > 0 ? $time_dif_text=$segundos_text : $time_dif_text = $time_dif_text;
$minutos > 0 ? $time_dif_text = $minutos_text : $time_dif_text = $time_dif_text;
$horas > 0 ? $time_dif_text = $horas_text : $time_dif_text = $time_dif_text;
$dias > 0 ? $time_dif_text = $dias_text : $time_dif_text = $time_dif_text;
$dias > 5 ? $time_dif_text = " el ".date("d M Y",$PublishDate) : $time_dif_text = " hace ".$time_dif_text;

if($time_dif_text == ' hace ') {
	$time_dif_text = ' hace instantes';
}
return $time_dif_text;
}
}

$img_cargando = '<center style="margin-top:5px"><img src="'.$Servidor_url.'imagenes/cargando.gif" /></center>';

?>