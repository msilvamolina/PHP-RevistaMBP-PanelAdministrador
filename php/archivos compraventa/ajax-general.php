<?php include('../paginas_include/4-php/variables-generales.php'); ?>
<?php include('../paginas_include/4-php/variables-sesion.php'); ?>
<?php header('Content-type: text/javascript'); ?>
function leeCookie(nombre)
{
	var cookies=document.cookie; if(!cookies) return false;
	var comienzo=cookies.indexOf(nombre);
	if(comienzo==-1) return false;
	comienzo=comienzo+nombre.length+1;
	cantidad=cookies.indexOf("; ", comienzo)-comienzo; if(cantidad<=0) cantidad=cookies.length;
	return cookies.substr(comienzo, cantidad);
}

function desaparecer(objeto, caida){
      $(objeto).animate({
       opacity: 0.0,
       marginTop: caida,
       }, 500 ).hide("slow");
 }
       
function aparecer(objeto, caida){
            $(objeto)
                .animate({
                    opacity: '0.0',
                    marginTop: "-"+caida,
                }, 10 ).show()
                .animate({
                    opacity: '1.0',
                    marginTop: "0",
                }, 1000 );
}

function cerrar_sugerencias(){
desaparecer('#ventana_sugerencias','5%');
}


function logout(){
		//instanciamos el objetoAjax
		ajax=nuevoAjax();
		//uso del medotod GET
		//indicamos el archivo que realizará el proceso de eliminación
		//junto con un valor que representa el id del empleado
		ajax.open("GET", "<?php echo $Servidor_url; ?>paginas_include/4-php/logout.php");
		ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				window.location='<?php echo $logoutUrl; ?>';
			}
		}
		//como hacemos uso del metodo GET
		//colocamos null
		ajax.send(null);
}
function ajax_mandar_login(){
	//donde se mostrará el resultado de la eliminacion
		//donde se mostrará el resultado de la eliminacion
	usuario=document.form_login.login_usuario.value;
	contrasena=document.form_login.login_contrasena.value;	
	recordar=document.form_login.login_recordar.checked;		

	if((usuario != '') && (contrasena != '')) {
	divResultado = document.getElementById('login_error');
		//instanciamos el objetoAjax
		ajax=nuevoAjax();
		//uso del medotod GET
		//indicamos el archivo que realizará el proceso de eliminación
		//junto con un valor que representa el id del empleado
		divResultado.innerHTML = '<center style="margin:5px"><img src="<?php echo $Servidor_url;?>imagenes/cargando.gif" width="26" height="26" /></center>';
		ajax.open("POST", "<?php echo $Servidor_url; ?>paginas_include/3-ajax/ajax-login-proceso.php",true);
		ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				if(ajax.responseText == 'log-ok') {
					location.reload(true);					
				}else{
					divResultado.innerHTML = ajax.responseText;
					aparecer('#login_error');
				}
				
			}
		}
			ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			ajax.send('usuario='+usuario+'&contrasena='+contrasena+'&recordar='+recordar);
		//Refresca la página
	}
}

function mandar_busqueda(){
	buscar_campo=document.form_buscar.buscar_campo.value;
				
	if(buscar_campo != '') {
		//instanciamos el objetoAjax
		ajax=nuevoAjax();
		//uso del medotod GET
		//indicamos el archivo que realizará el proceso de eliminación
		//junto con un valor que representa el id del empleado
		ajax.open("POST", "<?php echo $Servidor_url; ?>paginas_include/3-ajax/ajax-mandar-busqueda.php",true);
		ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				window.location="<?php echo $Servidor_url; ?>buscar?"+ajax.responseText;
			}
		}
			ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			ajax.send('buscar_campo='+buscar_campo);
		//Refresca la página
	}
}


function subir(){
if( document.documentElement.scrollTop < 10 ){
window.scrollTo(0,0);
}else{
window.scrollBy(0,-10);
setTimeout('subir()',10);
}
}

function iSubmitEnter(oEvento, form){ 
     var iAscii; 

     if (oEvento.keyCode) {
         iAscii = oEvento.keyCode; 
	 } else if (oEvento.which) {
         iAscii = oEvento.which; 
	 } else {
         return false; 
	 } 
	 if (iAscii == 13) {
		 if(form == 'login') {
			 ajax_mandar_login();
		 }else if(form == 'buscar') {
			 mandar_busqueda();
		 }else if(form == 'login_grande') {
			 ajax_mandar_login_grande(); 
		 }else if(form == 'login_chico') {
			 ajax_enviar_login(); 
		 }
	 }
}


function ajax_armar_publicidad(){
	//donde se mostrará el resultado de la eliminacion
	divResultado = document.getElementById('barraderecha');

		//instanciamos el objetoAjax
		ajax=nuevoAjax();
		//uso del medotod GET
		//indicamos el archivo que realizará el proceso de eliminación
		//junto con un valor que representa el id del empleado
		
		divResultado.innerHTML = '<center style="margin-top:20px"><img src="<?php echo $Servidor_url;?>imagenes/cargando.gif" width="32" height="32" /></center>';
		ajax.open("GET", "<?php echo $Servidor_url;?>paginas_include/1-estructura/barra-derecha.php?q=<?php echo $palabras_usuario_ordenadas;?>");
		ajax.onreadystatechange=function() {
			if (ajax.readyState==4) {
				//mostrar resultados en esta capa
				divResultado.innerHTML = ajax.responseText;
			}
		}
		//como hacemos uso del metodo GET
		//colocamos null
		ajax.send(null)
}

function cambiar_botones_login(){
	//donde se mostrará el resultado de la eliminacion
	cookie = leeCookie('usuario_logueado');
	if(cookie == 'logueado') {
	divResultado = document.getElementById('botones_barra_top');
		//instanciamos el objetoAjax
		ajax=nuevoAjax();
		//uso del medotod GET
		//indicamos el archivo que realizará el proceso de eliminación
		//junto con un valor que representa el id del empleado	
		divResultado.innerHTML = '<a href="<?php echo $Servidor_url; ?>micuenta" class="logueado boton_separacion "><span>Mi cuenta</span></a><a class="logueado" onclick="logout()"><span>Cerrar Sesión</span></a>';
	}
}