<?php
//Este Script verifica si el usuario está logueado, si no es así, entonces redirecciona a la página de login para que se loguee
if(!$login_sesion) {
	$url_pagina = $_SERVER['REQUEST_URI'];
	$pagina_login = $Servidor_url.'login'.$url_pagina;
	header("Location: ".$pagina_login);
	exit;
}
?>