<?php
session_start();

if(!$_SESSION) {
	
	if($_GET['pedirlogin']){
		$direccion_actual = $_SERVER['REQUEST_URI'];
		$redireccion = $Servidor_url.'revistapaneladministrador'.$direccion_actual;
	}else{
		$redireccion = $Servidor_url.'error404';
	}
		header('Location: '.$redireccion);
		exit;
} else {
	$id_administrador = $_SESSION['id_administrador'];
	$administrador_usuario = $_SESSION['administrador_usuario'];
	$administrador_nombre = $_SESSION['administrador_nombre'];
	$administrador_apellido = $_SESSION['administrador_apellido'];
	$administrador_sexo = $_SESSION['administrador_sexo'];		
	$administrador_nivel = $_SESSION['administrador_nivel'];
	
	if($administrador_nivel < $nivel_pagina) {
		$redireccion = $Servidor_url.'admin/errorpermisos.php';
		header('Location: '.$redireccion);
		exit;
	}
}
?>