<?php
$pagina_redireccion = trim($_GET['redirigir']);

include('../../paginas_include/variables-generales.php');

//Si se loguea correctamente
$id_administrador = trim($_GET['id_administrador']);
$administrador_usuario = trim($_GET['administrador_usuario']);
$administrador_nombre = trim($_GET['administrador_nombre']);
$administrador_apellido = trim($_GET['administrador_apellido']);
$administrador_sexo = trim($_GET['administrador_sexo']);
$administrador_nivel = trim($_GET['administrador_nivel']);

if($id_administrador) {
	session_start();
	$_SESSION['id_administrador'] = $id_administrador;
	$_SESSION['administrador_usuario'] = $administrador_usuario;
	$_SESSION['administrador_nombre'] = $administrador_nombre;
	$_SESSION['administrador_apellido'] = $administrador_apellido;	
	$_SESSION['administrador_sexo'] = $administrador_sexo;
	$_SESSION['administrador_nivel'] = $administrador_nivel;

	if($pagina_redireccion) {
		$redireccion = $Servidor_url.$pagina_redireccion;
	}else {
		$redireccion = $Servidor_url.'admin/index.php';
	}

}
//redireccionar
header('Location: '.$redireccion);
?>
