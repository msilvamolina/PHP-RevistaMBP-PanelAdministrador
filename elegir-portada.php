<?php 

include('../paginas_include/variables-generales.php');
$nivel_pagina = 0;
include('php/verificar-permisos.php');

conectar('sitioweb');

$id_noticia = trim($_GET['noticia']);

$query_rs_elegir_noticia = "SELECT id_noticia, noticia_titulo, fecha_carga, fecha_modificacion  FROM noticias ORDER BY id_noticia DESC";
$rs_elegir_noticia = mysql_query($query_rs_elegir_noticia)or die(mysql_error());
$row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia);
$totalrow_rs_elegir_noticia = mysql_num_rows($rs_elegir_noticia);

$ubicacion = $Servidor_url.'imagenes/noticias/fotos/';
$sin_imagen = 'sin-imagen.jpg';

if($id_noticia) {
$query_rs_noticia = "SELECT foto_portada FROM noticias WHERE id_noticia = $id_noticia";
$rs_noticia = mysql_query($query_rs_noticia)or die(mysql_error());
$row_rs_noticia = mysql_fetch_assoc($rs_noticia);
$totalrow_rs_noticia = mysql_num_rows($rs_noticia);

$id_portada = $row_rs_noticia['foto_portada'];

if($id_portada) {
	$query_rs_portada = "SELECT nombre_foto FROM fotos_publicaciones WHERE id_foto = $id_portada";
$rs_portada = mysql_query($query_rs_portada)or die(mysql_error());
$row_rs_portada = mysql_fetch_assoc($rs_portada);
$totalrow_rs_portada = mysql_num_rows($rs_portada);

$foto_portada = $ubicacion.$row_rs_portada['nombre_foto'];
}
$query_rs_imagenes = "SELECT id_foto, nombre_foto FROM fotos_publicaciones WHERE id_publicacion = $id_noticia";
$rs_imagenes = mysql_query($query_rs_imagenes)or die(mysql_error());
$row_rs_imagenes = mysql_fetch_assoc($rs_imagenes);
$totalrow_rs_imagenes = mysql_num_rows($rs_imagenes);

}

if(!$foto_portada) {
	$foto_portada = $ubicacion.$sin_imagen;
}

desconectar();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link rel="shortcut icon" href="<?php echo $Servidor_url; ?>favicon.ico">
<script src="<?php echo $Servidor_url; ?>00-Javascripts/autocompletar/jquery-1.3.2.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $Servidor_url; ?>00-Javascripts/ajax.js"></script>
<script  type="text/javascript" src="<?php echo $Servidor_url; ?>00-Javascripts/jquery.tipsy.js"></script>
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<?php include_once('../00-Javascripts/nicEditor/nicEdit.php'); ?>
<style type="text/css">

	#publicacion_fotos{
		width:620px;
		padding-right:20px;
		padding-top:10px;
		overflow-y:auto;
		border:1px solid #F60;
		-webkit-border-radius:6px;
		-moz-border-radius:6px;
		background:#F93;
	}
	 a {
		 color:#000;
	 }
	#upload{  
		padding:7px;
		margin:0 auto;
        text-align:center;  
		background-color:#F60;
		font-weight:bold;
        color:#000;  
        border:1px solid #000;  
		display:block;  
		text-decoration:none;
		width:80px;
		-webkit-border-radius:4px;
		-moz-border-radius:4px;
    }
		#upload a{
			color:#000;
    }
	
	#gallery{
		list-style:none;
		text-align:center;
		padding:0;
		margin-top:10px;
	}
	#gallery li{	
		display:block;
		float:left;
		width:100px;
		height:100px;
		background:#fff;
		border:1px solid #999;
		text-align:center;
		position:relative;
		padding:10px;
		margin-left:8px;
		margin-bottom:14px;
		-webkit-border-radius:4px;
		-moz-border-radius:4px;
	}
	#gallery li img{
		width:100px;
		height:100px
	}
	#gallery li a{
		position:absolute;
		right:10px;
		top:10px
	}
	#gallery li a img{ width:auto; height:auto}
	
	#crear_publicacion_descripcion {
		width:650px;
		height:400px;
	}
	.noticia_titulo {
		font-size:36px;
		padding:10px;
		width:96%;
		min-height:100px;
	}
	.noticia_bajada {
		font-size:18px;
		min-height:100px;
		padding:10px;
		width:96%;
	}
	.noticia_destacado {
		font-size:18px;
		min-height:100px;
		color:#d3222c;
		padding:10px;
		width:96%;
	}	
	#contenido_principal input[type='text'] {
		margin-top:0;
		margin-left:0;
		width:96%;
	}
	h3 {
		margin-top:25px;
		margin-bottom:10px;
	}
	.elegir_noticia {
		margin-top:10px;
		padding:15px;
		background:#6CF;
		border:1px solid #66F;
	}
	.elegir_noticia h3{
		margin-top:0;
	}
	.elegir_noticia select{
		width:100%;

	}
	.portada_actual {
		text-align:center;
		padding:20px;
		background:#CCC;
		border:1px solid #666
	}
	.portada_actual img{
		border:2px solid #000;
		width:300px;
	}
	.imagenes_para_elegir {
		text-align:center;
		padding:20px;
		background:#CCC;
		border:1px solid #666;
		padding-left:35px;
	}	
	.imagenes_para_elegir img{
		width:200px;
		
	}
	.seleccion_imagen {
		padding-top:10px;
		margin-bottom:15px;
		margin-right:15px;
		background:#666;
		width:280px;
		float:left;
	}
		.seleccion_imagen radio{
			font-size:48px;
			padding:30px;
		}
	.seleccion_imagen:hover {
		background:#636;
	}
	.seleccion_imagen p{
		padding:5px;
	}
	.noticia_editada {
	margin-top:20px;
	background:#d5fcd2;
	border:1px solid #6C6;
	padding:15px;
	font-size:18px;
}

.error_fotos {
	background:#fdddef;
	margin-top:10px;
	padding:10px;
}

.error_fotos a, .agregar_mas_fotos a{
	background:#F00;
	color:#FFF;
	padding:10px;
	text-decoration:none;
}

.error_fotos a:hover, .agregar_mas_fotos a:hover{
	background:#ff8700;
}
</style>
<script type="text/javascript" src="<?php echo $Servidor_url; ?>00-Javascripts/ajaxupload.js"></script>
<script type="text/javascript">


function elegir_imagen_portada(numero) {
	document.formulario_mbp.nueva_portada[numero].checked = true;
}
</script>
</head>

<body>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
  <h1>Elegir Portada</h1>
  <div class="elegir_noticia">
  <h3>Noticia:</h3>
  <form action="<?php echo $_SERVER['PHP_SELF']; ?>" name="form_elegir">
  <select name="noticia" onchange="document.forms.form_elegir.submit()">
  <option value="0">Elegir una Noticia:</option>
  <?php do { 
	if($id_noticia == $row_rs_elegir_noticia['id_noticia']) {
		$seleccionar = 'selected="selected"';
	}else{
		$seleccionar = '';
	}
	if($row_rs_elegir_noticia['fecha_modificacion']) {
		$cuantohace = 'modificada '.cuantoHace($row_rs_elegir_noticia['fecha_modificacion']);
	}else{
		$cuantohace = 'cargada '.cuantoHace($row_rs_elegir_noticia['fecha_carga']);
	}
  ?>

    <option <?php echo $seleccionar; ?> value="<?php echo $row_rs_elegir_noticia['id_noticia']; ?>"><?php echo $row_rs_elegir_noticia['noticia_titulo']; ?> - <?php echo $cuantohace;?></option>
  <?php } while($row_rs_elegir_noticia = mysql_fetch_assoc($rs_elegir_noticia)) ?>

  </select>
  </form>  
  </div>
  <?php if($totalrow_rs_imagenes) { ?>
  <form action="php/elegir-portada-db.php" id="formulario_mbp" name="formulario_mbp" onsubmit="return controlar_formulario()" method="post">
    <input type="hidden" name="form_id_noticia" value="<?php echo $id_noticia;?>"/>
    <?php if($_GET['portada'] == 'actualizada') { ?>   
    <div class="noticia_editada">
    La portada se actualizó correctamente
  </div>
  <?php } ?>
    <div class="imagenes_noticia">
	    <h3>Portada Actual:</h3>
    	<div class="portada_actual">
      <img src="<?php echo $foto_portada; ?>"  alt=""/>     
       </div>
     <br />
       <div class="agregar_mas_fotos">
  <p><a href="<?php echo $Servidor_url;?>admin/administrar-fotos.php?noticia=<?php echo $_GET['noticia']; ?>">Agregar más fotos</a></p>
  </div>
       	 <h3>Elegir Portada:</h3>    
	       <div class="imagenes_para_elegir">
           <center>
           <?php
		   $i = 0;
		    do {
			   $mostrar_foto = $ubicacion.$row_rs_imagenes['nombre_foto'];
			   
			   $id_foto = $row_rs_imagenes['id_foto'];
			   
			   $seleccionar_imagen = NULL;
			   
			   if($id_portada == $id_foto) {
				   $seleccionar_imagen = 'checked="checked"';
			   }
			    ?>
           <a href="javascript:void()" onclick="elegir_imagen_portada(<?php echo $i; ?>)">
           	<div class="seleccion_imagen">
		      <p><img src="<?php echo $mostrar_foto; ?>"  alt=""/></p>
              <p><input type="radio" <?php echo $seleccionar_imagen;?> name="nueva_portada" value="<?php echo $id_foto; ?>"/></p>
              </div>
          	</a>
           <?php $i++;} while($row_rs_imagenes = mysql_fetch_assoc($rs_imagenes)) ; ?>    
              </center>  
              <div class="eliminar_flotante"></div>                  		    	
    	   </div>
      </div>
  <p><center><input type="submit" value="Guardar Cambios" id="btn_cargar_noticia" name="btn_cargar_noticia" /></center></p>
  </form>
</div>
  <?php } else { ?>
  <?php if($_GET['noticia']) {  ?>
  <div class="error_fotos">
  <p>Esta noticia no contiene fotos</p>
  <br />
  <p><a href="<?php echo $Servidor_url;?>admin/administrar-fotos.php?noticia=<?php echo $_GET['noticia']; ?>">Agregar Fotos a esta noticia</a></p>
  <br />
  </div>
  <?php }} ?>
    <div class="eliminar_flotante"></div>
  </div>

  <!-- end .content --></div>
<p>&nbsp;</p>
  <!-- end .container --></div>
</body>
</html>
