<?php include('../paginas_include/variables-generales.php');?>
<?php
$nivel_pagina = 5;
include('php/verificar-permisos.php');

$dia_de_hoy = date("d");
$mes_de_hoy = date("m");
$ano_de_hoy = date("Y");

//Datos de Mes
$dia1 = trim($_GET['dia']);
$mes1 = trim($_GET['mes']);
$ano1 = trim($_GET['ano']);

if(!$mes1) {
	$dia1 = $dia_de_hoy;
	$mes1 = $mes_de_hoy;
	$ano1 = $ano_de_hoy;
}

$mes = $ano1.'-'.$mes1;
function diaSemana($ano,$mes,$dia)
{
	// 0->domingo	 | 6->sabado
	$dia= date("w",mktime(0, 0, 0, $mes, $dia, $ano));
		return $dia;
}

$dias_nombre[0] = 'Domingo';
$dias_nombre[1] = 'Lunes'; 
$dias_nombre[2] = 'Martes'; 
$dias_nombre[3] = 'Miércoles'; 
$dias_nombre[4] = 'Jueves'; 
$dias_nombre[5] = 'Viernes'; 
$dias_nombre[6] = 'Sábado'; 
$diaSemana = diaSemana($ano1, $mes1, $dia1);

if($dia1 == 0) {
$dia_nombre_mostrar = NULL;
$fecha1 = $mes.'-01 00:00:00';
$fecha2 = $mes.'-31 23:59:59';
} else {
$dia_nombre_mostrar = $dias_nombre[$diaSemana].' '.$dia1.' de ';
$fecha1 = $mes.'-'.$dia1.' 00:00:00';
$fecha2 = $mes.'-'.$dia1.' 23:59:59';
}

$meses_nombre[1] = 'Enero'; 
$meses_nombre[2] = 'Febrero'; 
$meses_nombre[3] = 'Marzo'; 
$meses_nombre[4] = 'Abril'; 
$meses_nombre[5] = 'Mayo'; 
$meses_nombre[6] = 'Junio'; 
$meses_nombre[7] = 'Julio'; 
$meses_nombre[8] = 'Agosto'; 
$meses_nombre[9] = 'Septiembre'; 
$meses_nombre[10] = 'Octubre'; 
$meses_nombre[11] = 'Noviembre'; 
$meses_nombre[12] = 'Diciembre'; 

$mes1 = ltrim($mes1, '0');

$mes_nombre_mostrar = $meses_nombre[$mes1];
$ano_nombre = $ano1;

conectar('estadisticas');
$query_rs_estadisticas = "SELECT * FROM sitio WHERE fecha_visita between '$fecha1' AND '$fecha2' ORDER BY id_estadistica DESC";
$rs_estadisticas = mysql_query($query_rs_estadisticas)or die(mysql_error());
$row_rs_estadisticas = mysql_fetch_assoc($rs_estadisticas);
$totalrow_rs_estadisticas = mysql_num_rows($rs_estadisticas);

desconectar();

conectar('sitioweb');
$query_rs_notas = "SELECT id_noticia, noticia_titulo, noticia_url FROM noticias WHERE noticia_publicada = 1";
$rs_notas = mysql_query($query_rs_notas)or die(mysql_error());
$row_rs_notas = mysql_fetch_assoc($rs_notas);
$totalrow_rs_notas = mysql_num_rows($rs_notas);
desconectar();

$visita_ip = array();
$visita_noticia = array();

do {
	$visita_ip[] = $row_rs_estadisticas['ip_visitante'];
	if($row_rs_estadisticas['id_noticia']) {
		$visita_noticia[] = $row_rs_estadisticas['id_noticia'];
	}
	if($row_rs_estadisticas['id_revista']) {
		$visita_revista[] = $row_rs_estadisticas['id_revista'];
		$revista_tipo[] = $row_rs_estadisticas['pagina_visitada'];
	}
	$visita_navegador[] = $row_rs_estadisticas['navegador'];
	$visita_dispositivo[] = $row_rs_estadisticas['dispositivo'];
	$visita_sistema_operativo[] = $row_rs_estadisticas['sistema_operativo'];
	$visita_pagina_visitada[] = $row_rs_estadisticas['pagina_visitada'];

} while($row_rs_estadisticas = mysql_fetch_assoc($rs_estadisticas));

@$ip_lista = array_unique($visita_ip);
@$noticia_lista = array_count_values($visita_noticia);

@$revistas_lista = array_count_values($visita_revista);

@$visita_navegador_lista = array_count_values($visita_navegador);
@$visita_dispositivo_lista = array_count_values($visita_dispositivo);
@$visita_sistema_operativo_lista = array_count_values($visita_sistema_operativo);
@$visita_pagina_visitada_lista = array_count_values($visita_pagina_visitada);

@$revistas_formatos = array_count_values($revista_tipo);

@arsort($noticia_lista);
@arsort($revistas_lista);
@arsort($visita_navegador_lista);
@arsort($visita_dispositivo_lista);
@arsort($visita_sistema_operativo_lista);
@arsort($visita_pagina_visitada_lista);

@$cantidad_ip = count($ip_lista);

//array notas
$array_notas = array();
do {
	$noticia_url = $row_rs_notas['noticia_url'];
	$id_noticia =  $row_rs_notas['id_noticia'];

	$array_notas[$id_noticia] = $row_rs_notas['noticia_titulo'];

}while($row_rs_notas = mysql_fetch_assoc($rs_notas));

//opciones menu
//opcion dia anterior
		$ano1_anterior = $ano1;		
		$mes1_anterior = $mes1;
	if($mes1 == 1) {
		$ano1_anterior = $ano1 - 1;
		$mes1_anterior = 12;
	}
	$opcion_dia_anterior = $_SERVER['PHP_SELF'].'?ano='.$ano1_anterior.'&mes='.$mes1_anterior.'&dia='.($dia1 - 1);	
if($dia1 <= 1) {
	$opcion_dia_anterior = $_SERVER['PHP_SELF'].'?ano='.$ano1_anterior.'&mes='.($mes1_anterior - 1).'&dia=31&accion=dia-anterior';
}

$opcion_todo_mes = $_SERVER['PHP_SELF'].'?ano='.$ano1.'&mes='.$mes1.'&dia=0';
$opcion_hoy = $_SERVER['PHP_SELF'].'?ano='.$ano_de_hoy.'&mes='.$mes_de_hoy.'&dia='.$dia_de_hoy;

	$ano1_posterior = $ano1;
	$mes1_posterior = $mes1;
	if($mes1 == 12) {
		$ano1_posterior = $ano1 + 1;
		$mes1_posterior = 1;
	}
	$opcion_dia_siguiente = $_SERVER['PHP_SELF'].'?ano='.$ano1_posterior.'&mes='.$mes1_posterior.'&dia='.($dia1 + 1);
if($dia1 >= 31) {
	$opcion_dia_siguiente = $_SERVER['PHP_SELF'].'?ano='.$ano1_posterior.'&mes='.($mes1_posterior + 1).'&dia=1&accion=dia-siguiente';
}
	
//opcion selecionada

$opcion_elegida = NULL;
if(!$dia1) {
	$opcion_elegida_todo_mes = 'class="opcion_elegida"';
}
if(($dia1==$dia_de_hoy)&&($mes1==$mes_de_hoy)&&($ano1==$ano_de_hoy)) {
	$opcion_elegida_hoy = 'class="opcion_elegida"';
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../00-Javascripts/jquery.js"></script>
<script type="text/javascript" src="../00-Javascripts/ajax.js"></script>
<style>
p strong {
	color:#900;
}
ul li {
	list-style:none;
	list-style-type:none;
	padding-left:5px;
}
tr td {
	padding-left:10px;
}
tr td selection{
	font-size:14px;
	min-width:50px;
	padding:8px;
	background:#F00;
	color:#fff;
}

.celda_1 {
	background:#CCC;
}
.celda_2 {
	background:#999;
}
.estadisticas_opciones {
	background:#900;
	color:#fff;
}
.estadisticas_opciones a {
	color:#fff;
	width:100%;
	padding:12px;
	text-decoration:none;
}
.boton_arriba a{
	background:#900;	
	color:#fff;
	padding:12px;
	text-decoration:none;
}
.boton_arriba a:hover {
	background:#F30;
}
.estadisticas_opciones a:hover {
	background:#F30;
}
.opcion_elegida {
	background:#F30;
}
</style>
</head>

<body>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
<a name="top"></a>
<div class="elegir_noticia">
<h3>Año 2013</h3>
<h3>Elegir mes</h3>
  <form action="<?php echo $_SERVER['PHP_SELF']; ?>" name="form_elegir">
  <input type="hidden" value="2013" name="ano"/>
  <select name="mes" onchange="document.forms.form_elegir.submit()">
  <?php foreach ($meses_nombre as $clave => $valor){ 
	if($clave == $mes1) {
		$seleccionar = 'selected="selected"';
	}else{
		$seleccionar = '';
	}
  ?>
  <option <?php echo $seleccionar;?> value="<?php echo $clave; ?>"><selection><?php echo $valor;?></selection></option>
<?php } ?>
</select>
<br /><br />
<h3>Elegir día</h3>
  <select name="dia" onchange="document.forms.form_elegir.submit()">
  <?php $i = 0; do { 
	if($i == $dia1) {
		$seleccionar = 'selected="selected"';
	}else{
		$seleccionar = '';
	}
	if($i==0) { 
	$nombre_mostrar = 'Todo el mes';
	} else {
	$nombre_mostrar = $i;
	}
	
  ?>
  <option <?php echo $seleccionar;?> value="<?php echo $i; ?>"><selection><?php echo $nombre_mostrar;?></selection></option>
<?php $i++;}while($i <= 31); ?>
</select>
</form>
</div>
<br />
<table width="100%" border="0" cellspacing="0" class="estadisticas_opciones"cellpadding="0">
  <tr>
    <td align="center"><a href="<?php echo $opcion_dia_anterior; ?>">Día anterior</a></td>
    <td align="center"><a href="<?php echo $opcion_todo_mes; ?>" <?php echo $opcion_elegida_todo_mes; ?>>Todo el mes</a></td>
    <td align="center"><a href="<?php echo $opcion_hoy; ?>" <?php echo $opcion_elegida_hoy; ?>>Hoy</a></td>
    <td align="center"><a href="<?php echo $opcion_dia_siguiente; ?>">Día siguiente</a></td>
  </tr>
</table>
<br />
  <h1><?php echo $dia_nombre_mostrar.$mes_nombre_mostrar.' '.$ano_nombre;?></h1><br />
 <?php if(!$totalrow_rs_estadisticas) { ?>
<p>No se encontraron registros en la base de datos</p>
 <?php } else { ?>
 <table width="100%" border="0" class="tabla_estadisticas" cellspacing="0" cellpadding="0">
  <tr>
    <td class="celda_1">Cantidad de Visitas</td>
    <td align="right" class="celda_1"><selection><?php echo $totalrow_rs_estadisticas; ?></selection></td>
  </tr>
    <tr>
    <td class="celda_2">Cantidad de Visitas desde IPs diferentes</td>
    <td align="right" class="celda_2"><selection><?php echo $cantidad_ip;?></selection></td>
  </tr>
</table>
<br />
<h3>Notas más leídas</h3>
<table width="100%" border="0" class="tabla_estadisticas" cellspacing="0" cellpadding="0">
<?php $i=1; foreach ($noticia_lista as $clave => $valor){ ?>
  <tr>
    <td class="celda_<?php echo $i;?>"><?php echo $array_notas[$clave];?></td>
    <td align="right" class="celda_<?php echo $i;?>"><selection><?php echo $valor;?></selection></td>
  </tr>
<?php 
$i++;
if($i==3) {$i=1;}
} ?>  
</table>
<br />
<h3>Revistas más leídas</h3>
<table width="100%" border="0" class="tabla_estadisticas" cellspacing="0" cellpadding="0">
<?php $i=1; foreach ($revistas_lista as $clave => $valor){ ?>
  <tr>
    <td class="celda_<?php echo $i;?>">MBP Nº<?php echo $clave;?></td>
    <td align="right" class="celda_<?php echo $i;?>"><selection><?php echo $valor;?></selection></td>
  </tr>
<?php 
$i++;
if($i==3) {$i=1;}
} ?>  
</table>
<br />
<h3>Formatos de Revistas más leídas</h3>
<table width="100%" border="0" class="tabla_estadisticas" cellspacing="0" cellpadding="0">
  <tr>
    <td class="celda_1">Versión Flip Page</td>
    <td align="right" class="celda_1"><selection><?php echo $revistas_formatos['revista-flippage']; ?></selection></td>
  </tr>
    <tr>
    <td class="celda_2">Versión Adaptativa</td>
    <td align="right" class="celda_2"><selection><?php echo $revistas_formatos['revista-adaptativa']; ?></selection></td>
  </tr>
</table>
<br />
<h3>Páginas más visitadas</h3>
<table width="100%" border="0" class="tabla_estadisticas" cellspacing="0" cellpadding="0">
<?php $i=1; foreach ($visita_pagina_visitada_lista as $clave => $valor){ ?>
  <tr>
    <td class="celda_<?php echo $i;?>"><?php echo $clave;?></td>
    <td align="right" class="celda_<?php echo $i;?>"><selection><?php echo $valor;?></selection></td>
  </tr>
<?php 
$i++;
if($i==3) {$i=1;}
} ?>  
</table>
<br />
<h3>Navegadores más usados</h3>
<table width="100%" border="0" class="tabla_estadisticas" cellspacing="0" cellpadding="0">
<?php $i=1; foreach ($visita_navegador_lista as $clave => $valor){ ?>
  <tr>
    <td class="celda_<?php echo $i;?>"><?php echo $clave;?></td>
    <td align="right" class="celda_<?php echo $i;?>"><selection><?php echo $valor;?></selection></td>
  </tr>
<?php 
$i++;
if($i==3) {$i=1;}
} ?>  
</table>
<br />
<h3>Dispositivos más usados</h3>
<table width="100%" border="0" class="tabla_estadisticas" cellspacing="0" cellpadding="0">
<?php $i=1; foreach ($visita_dispositivo_lista as $clave => $valor){ ?>
  <tr>
    <td class="celda_<?php echo $i;?>"><?php echo $clave;?></td>
    <td align="right" class="celda_<?php echo $i;?>"><selection><?php echo $valor;?></selection></td>
  </tr>
<?php 
$i++;
if($i==3) {$i=1;}
} ?>  
</table>

<br />
<h3>Sistemas Operativos más usados</h3>
<table width="100%" border="0" class="tabla_estadisticas" cellspacing="0" cellpadding="0">
<?php $i=1; foreach ($visita_sistema_operativo_lista as $clave => $valor){ ?>
  <tr>
    <td class="celda_<?php echo $i;?>"><?php echo $clave;?></td>
    <td align="right" class="celda_<?php echo $i;?>"><selection><?php echo $valor;?></selection></td>
  </tr>
<?php 
$i++;
if($i==3) {$i=1;}
} ?>  
</table>
<br /><br />
<center><div class="boton_arriba"><a href="#top">Volver para arriba</a></div></center>
<?php } ?>
</div>
 <div class="eliminar_flotante"></div> 
  </div>

  <!-- end .content --></div>
  <!-- end .container --></div>
</body>
</html>
