<?php $panel_administrador = 'panel_administrador'; ?>
<?php include('../paginas_include/variables-generales.php'); ?>
<?php include('../paginas_include/1-php/detectar-dispositivo.php'); ?>
<?php

?>
<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="">
<!--<![endif]-->
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Panel Administrador</title>
<link href="<?php echo $Servidor_url;?>boilerplate.css" rel="stylesheet" type="text/css">
  <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" href="../css/principal-admin.css" type="text/css" media="screen" />
    <link href="css/admin-fluido.css" rel="stylesheet" type="text/css" />    
    <link rel="stylesheet"  href="../js/jquery.mobile-1.3.2/demos/css/themes/default/jquery.mobile-1.3.2.min.css">
    	<link rel="stylesheet" href="../js/jquery.mobile-1.3.2/demos/_assets/css/jqm-demos.css">
        	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<script src="../js/jquery.mobile-1.3.2/demos/js/jquery.js"></script>
	<script src="../js/jquery.mobile-1.3.2/demos/_assets/js/index.js"></script>
	<script src="../js/jquery.mobile-1.3.2/demos/js/jquery.mobile-1.3.2.min.js"></script>
    <style>
	.columna_pie {
		margin-top:10px;
	width:100%;
	clear:none;
}

#contenido_principal {
	padding-right:20px;
	padding-left:20px;
	padding-bottom:20px;
	width:87.5%;
	clear:none;

}
h1 {
		font-size:36px;
		color:#000;
}

header {
	margin-top:40px;
}

#barra_opciones  {
	display:none;
}

.revistas_contenedor H1{
	color:#fff;
	font-weight:bold;
}

#contenido_principal input[type='text'],#contenido_principal input[type='password'],#contenido_principal input[type='email'], #contenido_principal textarea {
	height:40px;
	padding:8px;
	font-size:24px;
}

#contenido_principal textarea {
	height:100px;
}

#contenido_principal .formulario_noticia {
	height:400px;
}
@media only screen and (min-width: 481px) {
	h1 {
		font-size:48px;
}
		.columna_pie {
	margin-left:30px;
	width:63%;
	clear:none;
}
#contenido_principal {
	padding-right:20px;
	padding-left:20px;
	padding-bottom:20px;
	width:92%;
	clear:none;
}

}
@media only screen and (min-width: 641px) {
#contenido_principal {
	padding-right:20px;
	padding-left:20px;
	padding-bottom:20px;
	width:94%;
	clear:none;
}
}
@media only screen and (min-width: 961px) {
	#barra_opciones {
		display:block;
	width:250px;
	
	clear:none;
}

#barra_opciones nav ul {
	margin:0;
	padding:0;
}

#barra_opciones nav ul li{
	font-weight:bold;
	font-size:18px;
	padding:10px;
	background:#88018c;
	list-style:none;
	color:#fff;
}

#barra_opciones nav ul a{
	text-decoration:none;
}

#barra_opciones nav ul li:hover{
	background:#F90;
}
		.columna_pie {
	margin-left:30px;
	width:70%;
	clear:none;
}
#contenido_principal {
	padding-right:20px;
	padding-left:20px;
	padding-bottom:20px;
	width:69.7%;
	clear:none;
}
}
@media only screen and (min-width: 1025px) {

#contenido_principal {
	padding-right:20px;
	padding-left:20px;
	padding-bottom:20px;
	width:71.65%;
	clear:none;
}
}
@media only screen and (min-width: 1281px) {

#contenido_principal {
	padding-right:20px;
	padding-left:20px;
	padding-bottom:20px;
	width:77.34%;
	clear:none;
}
}

	</style>    

</head>
<!-- 
To learn more about the conditional comments around the html tags at the top of the file:
paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/

Do the following if you're using your customized build of modernizr (http://www.modernizr.com/):
* insert the link to your js here
* remove the link below to the html5shiv
* add the "no-js" class to the html tags at the top
* you can also remove the link to respond.min.js if you included the MQ Polyfill in your modernizr build 
-->
<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<body>
<div data-role="page" id="demo-page" data-theme="d" data-url="demo-page">
    <div data-role="header" data-theme="c">
        <h1>Panel Administrador - Revista MBP</h1>
        <a href="#left-panel" data-icon="bars" data-iconpos="notext" data-shadow="false" data-iconshadow="false">Menu</a>
    </div><!-- /header -->
    <div data-role="content">
	<div class="gridContainer  clearfix">
            <?php include('../paginas_include/2-estructura/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal" class="fluid">
  <h1>Cargar Noticia</h1>
  <form action="php/cargar-noticia-db.php" id="formulario_mbp" name="formulario_mbp" onsubmit="return controlar_formulario()" method="post">
  <p><input type="text" placeholder="Antetítulo" id="form_antetitulo" name="form_antetitulo"/></p>
  <p><input type="text" placeholder="Título" id="form_titulo" name="form_titulo"/></p>
  <p><div class="selecionar_autor">
  <select id="select_autor" name="select_autor">
    <option value="0">Sin autor (Elegir Autor)</option>
  <?php do{ ?>
    <option value="<?php echo $row_rs_autores['id_autor']; ?>"><?php echo $row_rs_autores['nombre_autor']; ?></option>
  <?php } while ($row_rs_autores = mysql_fetch_assoc($rs_autores)); ?>
  </select></div></p>
  <p><textarea placeholder="Bajada" class="formulario_bajada"  id="form_bajada" name="form_bajada"></textarea></p>
  <p><textarea placeholder="Noticia" class="formulario_noticia"  id="form_noticia" name="form_noticia"></textarea></p>
    <p><textarea placeholder="Destacado 1 (Opcional)" class="formulario_bajada form_destacado"  id="form_destacado1" name="form_destacado1"></textarea></p>
  <p><textarea placeholder="Destacado 2 (Opcional)" class="formulario_bajada form_destacado"  id="form_destacado2" name="form_destacado2"></textarea></p>
  <p><textarea placeholder="Destacado 3 (Opcional)" class="formulario_bajada form_destacado"  id="form_destacado3" name="form_destacado3"></textarea></p>
  <p><textarea placeholder="Destacado 4 (Opcional)" class="formulario_bajada form_destacado"  id="form_destacado4" name="form_destacado4"></textarea></p>
  <p><center><input type="submit" value="Cargar Noticia" id="btn_cargar_noticia" name="btn_cargar_noticia" /></center></p>
  </form>
</div>

                              <div class="eliminar_flotante"></div>
<?php include('../paginas_include/2-estructura/pie-admin.php'); ?>
        </div>
	</div>

<br /><br />
    </div><!-- /content -->
    <div data-role="panel" id="left-panel" data-theme="c">
        <ul data-role="listview" data-theme="d">
            <li data-icon="delete"><a href="#" data-rel="close">Cerrar</a></li>
            <li data-role="list-divider">Menu</li>
            <li data-icon="back"><a href="http://localhost/revistambp/admin/nuevodiseno.php2" >Demo intro</a></li>
        </ul>
        
    </div><!-- /panel -->
</div>
</body>
</html>
