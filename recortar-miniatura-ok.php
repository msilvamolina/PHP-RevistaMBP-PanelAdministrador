<?php 

include('../paginas_include/variables-generales.php');
$nivel_pagina = 5;
include('php/verificar-permisos.php');

conectar('sitioweb');

$id_foto = trim($_GET['foto']);

if($id_foto) {
$query_rs_noticia = "SELECT nombre_foto, recorte_miniatura_x, recorte_miniatura_y, recorte_miniatura_w, recorte_miniatura_h  FROM fotos_publicaciones WHERE id_foto = $id_foto";
$rs_noticia = mysql_query($query_rs_noticia)or die(mysql_error());
$row_rs_noticia = mysql_fetch_assoc($rs_noticia);
$totalrow_rs_noticia = mysql_num_rows($rs_noticia);

desconectar();

$foto_seleccionada = $row_rs_noticia['nombre_foto'];

$recorte_foto_x = $row_rs_noticia['recorte_miniatura_x'];
$recorte_foto_y = $row_rs_noticia['recorte_miniatura_y'];
$recorte_foto_w = $row_rs_noticia['recorte_miniatura_w'];
$recorte_foto_h = $row_rs_noticia['recorte_miniatura_h'];

$recorte_foto = $recorte_foto_x.'x'.$recorte_foto_y.'x'.$recorte_foto_w.'x'.$recorte_foto_h;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link rel="shortcut icon" href="<?php echo $Servidor_url; ?>favicon.ico">
<script src="<?php echo $Servidor_url; ?>00-Javascripts/autocompletar/jquery-1.3.2.min.js" type="text/javascript"></script>

<link href="css/admin.css" rel="stylesheet" type="text/css" />
<style type="text/css">

	#publicacion_fotos{
		width:620px;
		padding-right:20px;
		padding-top:10px;
		overflow-y:auto;
		border:1px solid #F60;
		-webkit-border-radius:6px;
		-moz-border-radius:6px;
		background:#F93;
	}
	 a {
		 color:#000;
	 }
	#upload{  
		padding:7px;
		margin:0 auto;
        text-align:center;  
		background-color:#F60;
		font-weight:bold;
        color:#000;  
        border:1px solid #000;  
		display:block;  
		text-decoration:none;
		width:80px;
		-webkit-border-radius:4px;
		-moz-border-radius:4px;
    }
		#upload a{
			color:#000;
    }
	
	#gallery{
		list-style:none;
		text-align:center;
		padding:0;
		margin-top:10px;
	}
	#gallery li{	
		display:block;
		float:left;
		width:100px;
		height:100px;
		background:#fff;
		border:1px solid #999;
		text-align:center;
		position:relative;
		padding:10px;
		margin-left:8px;
		margin-bottom:14px;
		-webkit-border-radius:4px;
		-moz-border-radius:4px;
	}
	#gallery li img{
		width:100px;
		height:100px
	}
	#gallery li a{
		position:absolute;
		right:10px;
		top:10px
	}
	#gallery li a img{ width:auto; height:auto}
	
	#crear_publicacion_descripcion {
		width:650px;
		height:400px;
	}
	.noticia_titulo {
		font-size:36px;
		padding:10px;
		width:96%;
		min-height:100px;
	}
	.noticia_bajada {
		font-size:18px;
		min-height:100px;
		padding:10px;
		width:96%;
	}
	.noticia_destacado {
		font-size:18px;
		min-height:100px;
		color:#d3222c;
		padding:10px;
		width:96%;
	}	
	#contenido_principal input[type='text'] {
		margin-top:0;
		margin-left:0;
		width:96%;
	}

	.elegir_noticia {
		margin-top:10px;
		padding:15px;
		background:#6CF;
		border:1px solid #66F;
	}
	.elegir_noticia h3{
		margin-top:0;
	}
	.elegir_noticia select{
		width:100%;

	}
	
	a {
		text-decoration:none;
	}
	#contenido_principal img {
		border:1px solid #000;
	}
</style>
  <script src="jcrop/js/jquery.min.js"></script>
  <script src="jcrop/js/jquery.Jcrop.js"></script>
  <link rel="stylesheet" href="jcrop/demo_files/main.css" type="text/css" />
  <link rel="stylesheet" href="jcrop/demo_files/demos.css" type="text/css" />
  <link rel="stylesheet" href="jcrop/css/jquery.Jcrop.css" type="text/css" />

</head>

<body>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
  <h1>Recorte Completado</h1>
  <br />
  <?php if($totalrow_rs_noticia) { ?>

<img src="http://www.revistambp.com/00-Librerias/libreria-imagenes/scripts/recortar-imagen-coordenadas.php?tamano=<?php echo $recorte_foto; ?>&amp;imagen=<?php echo $foto_seleccionada; ?>" width="320" height="320">
<br /><br /><br />
  <p><center><a id="btn_cargar_noticia" href="recortar-miniatura-guardar.php?foto=<?php echo $id_foto;?>">Guardar Recorte</a></center></p>
<br /><br /><br />
  <p><center><a id="btn_cargar_noticia" href="recortar-miniatura.php?foto=<?php echo $id_foto;?>">Volver a Recortar</a></center></p>
  <?php } ?>
    <div class="eliminar_flotante"></div>
  </div>

  <!-- end .content --></div>
<p>&nbsp;</p>
  <!-- end .container --></div>
</body>
</html>
