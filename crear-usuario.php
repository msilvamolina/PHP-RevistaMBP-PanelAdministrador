<?php include('../paginas_include/variables-generales.php');?>
<?php
$nivel_pagina = 99;
include('php/verificar-permisos.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function controlar_formulario() {
	usuario=document.formulario_mbp.form_usuario.value;
	contrasena=document.formulario_mbp.form_contrasena.value;
	nombre=document.formulario_mbp.form_nombre.value;
	apellido=document.formulario_mbp.form_apellido.value;
	email=document.formulario_mbp.form_email.value;

error=null;

	if(!usuario) {
		error='pepe';
	}
	if(!contrasena) {
		error='pepe';
	}
	if(!nombre) {
		error='pepe';
	}
	if(!apellido) {
		error='pepe';
	}		
	if(!email) {
		error='pepe';
	}
	
	if(error==null) {
		return true;
	} else {
		return false;
	}
}
</script>
</head>
<body>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
  <h1>Crear Usuario</h1>
  <?php if($_GET['datos']=='yaestan'){ ?>
  <div class="formulario_error">Error: El usuario o e-mail ingresados, ya pertenecen a un usuario registrado</div>
  <?php } ?>
   <form action="php/crear-usuario-db.php" id="formulario_mbp" name="formulario_mbp" onsubmit="return controlar_formulario()" method="post">
  <p><input type="text" placeholder="Usuario" id="form_antetitulo" name="form_usuario"/></p>
  <p><input type="text" placeholder="Contrasena" id="form_antetitulo" name="form_contrasena"/></p>
  <br />
  <p><input type="text" placeholder="Nombre" id="form_antetitulo" name="form_nombre"/></p>
  <p><input type="text" placeholder="Apellido" id="form_antetitulo" name="form_apellido"/></p>
  <br />
  <p><input type="email" placeholder="Email" id="form_titulo" name="form_email"/></p>
  <br />
   <p><div class="selecionar_sexo">Sexo:
  <select id="form_sexo" name="form_sexo">
    <option value="Hombre">Hombre</option>
    <option value="Mujer">Mujer</option>
  </select></div></p>
  <br />
     <p><div class="selecionar_sexo">Nivel de Usuario:
  <select id="form_nivel" name="form_nivel">
    <option value="0">(0) Básico</option>
    <option value="1">(1) Intermedio</option>
    <option value="5">(5) Avanzado</option>    
  </select></div></p>
    <br />  <br />
    <p><center><input type="submit" value="Crear Usuario" id="btn_cargar_noticia" name="btn_cargar_noticia" /></center></p>
  </form>
</div>
 <div class="eliminar_flotante"></div> 
  </div>

  <!-- end .content --></div>
  <!-- end .container --></div>
</body>
</html>
