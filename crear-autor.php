<?php include('../paginas_include/variables-generales.php');?>
<?php
$nivel_pagina = 5;
include('php/verificar-permisos.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Panel Administrador</title>
<link href="css/admin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function controlar_formulario() {
	nombre=document.formulario_mbp.form_nombre.value;
	email=document.formulario_mbp.form_email.value;

error=null;
	
	if(!nombre) {
		error='pepe';
	}
	if(!email) {
		error='pepe';
	}
	
	if(error==null) {
		return true;
	} else {
		return false;
	}
}
</script>
</head>
<body>
<div class="container">
<?php include('includes/encabezado-admin.php'); ?>
<?php include('includes/barra-opciones.php'); ?>
<div id="contenido_principal">
  <h1>Crear Autor</h1>
   <form action="php/crear-autor-db.php" id="formulario_mbp" name="formulario_mbp" onsubmit="return controlar_formulario()" method="post">
  <p><input type="text" placeholder="Nombre" id="form_antetitulo" name="form_nombre"/></p>
  <p><input type="email" placeholder="Email" id="form_titulo" name="form_email"/></p>
    <p><center><input type="submit" value="Crear Autor" id="btn_cargar_noticia" name="btn_cargar_noticia" /></center></p>
  </form>
</div>
 <div class="eliminar_flotante"></div> 
  </div>

  <!-- end .content --></div>
  <!-- end .container --></div>
</body>
</html>
